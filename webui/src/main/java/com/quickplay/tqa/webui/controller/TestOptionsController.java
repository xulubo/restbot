package com.quickplay.tqa.webui.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.quickplay.tqa.webui.service.TestOptions;

@RequestMapping("/api")
@SessionAttributes(value={"context", "progress"})
@Controller
@Scope("session")
public class TestOptionsController extends BaseApiController {

	@RequestMapping(value="/options", method=RequestMethod.GET)
	@ResponseBody
	public Object getOptions() {
		return tm.getOptions();
	}
	
	@RequestMapping(value="/options", method={RequestMethod.POST, RequestMethod.PUT})
	@ResponseBody
	public Object saveOptions(
			@RequestBody TestOptions options) {
		tm.saveOptions(options);
		return tm.getOptions();
	}
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value="/reload", method={RequestMethod.POST})
	public void reloadTestPlans() {
		testService.reload();
	}
}
