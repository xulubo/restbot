package com.quickplay.tqa.webui.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.gson.Gson;
import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.core.Expression;
import com.quickplay.restbot.caze.core.ExpressionSet;
import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.persist.reader.xml.BaseEntity;

@RequestMapping({"/api/plan/{planName:[^:]*}{casePath}"})
@SessionAttributes(value={"context", "progress"})
@Controller
@Scope("session")
public class CaseApiController extends BaseApiController {
	Logger logger = LoggerFactory.getLogger(CaseApiController.class);

	/**
	 * Read all cases of first test suite
	 * @param planName
	 * @return
	 */
	@RequestMapping(value="", method=RequestMethod.GET)
	@ResponseBody
	public Object getCaseDetail(
			@PathVariable String planName,
			@PathVariable String casePath) {
		
		logger.debug("getCases planName={} casePath={}", planName, casePath);
		Plan plan = repo.get(planName);
		
		//make it an absolute path
		if (!casePath.startsWith(":")) {
			casePath = ":" + casePath;
		}
		return plan.find(casePath).getEntity();
	}
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value="", method=RequestMethod.DELETE)
	@ResponseBody
	public void deleteCase (
			@PathVariable String planName,
			@PathVariable String casePath) {
		
		logger.trace("Enter getProgress");
		
		if (casePath.equals(":")) {
			repo.delete(planName);
		} else {
			repo.get(planName).find(casePath).remove();
		}
	}

	@RequestMapping(value="", method={RequestMethod.POST, RequestMethod.PUT})
	@ResponseBody
	public Object saveCase (
			@PathVariable String planName,
			@PathVariable String casePath,
			@RequestBody BaseEntity caseEntity) {
		
		logger.trace("Enter getProgress");
		logger.debug("{}", new Gson().toJson(caseEntity));
		
		Plan plan = repo.get(planName);
		plan.find(casePath).addChild(caseEntity);
		repo.save(plan);
		
		return caseEntity;
	}

	@RequestMapping(value="/children", method={RequestMethod.POST, RequestMethod.PUT})
	@ResponseBody
	public Object addChildCase (
			@PathVariable String planName,
			@PathVariable String casePath,
			@RequestBody BaseEntity caseEntity) {
		
		logger.trace("Enter addChildCase");
		logger.debug("{}", new Gson().toJson(caseEntity));
		
		Plan plan = repo.get(planName);
		plan.find(casePath).addChild(caseEntity);
		repo.save(plan);
		
		return caseEntity;
	}
	
	/*
	 * To clone a case in the same suite
	 */
	@RequestMapping(value="/clone", method=RequestMethod.POST)
	@ResponseBody
	public Object cloneCase (
			@PathVariable String planName,
			@PathVariable String casePath) {
		
		logger.debug("cloning case {}/{}", planName, casePath);
		
		Plan plan = repo.get(planName);
		Case caze = plan.find(casePath);
		int newId = caze.getParent().newChildId();

		BaseEntity clone;
		try {
			clone = (BaseEntity) BeanUtils.cloneBean(caze.getEntity());
			clone.setId(String.valueOf(newId));
			caze.getParent().addChild(clone);
			repo.save(plan);

			logger.debug("clone success");
			return clone;
		} catch (IllegalAccessException | InstantiationException
				| InvocationTargetException | NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	

	@RequestMapping(value="/queryParams", params="action=reset", method={RequestMethod.POST})
	@ResponseBody
	public Object resetQueryParam (
			@PathVariable String planName,
			@PathVariable String casePath,
			@RequestBody List<Expression> e) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("{}", new Gson().toJson(e));
		}
		
		Plan plan = repo.get(planName);
		ExpressionSet set = plan.find(casePath).modifier().getQueryParams();
		set.clear();
		set.addAll(e);
		repo.save(plan);
		
		return e;
	}
	
	@RequestMapping(value="/queryParams", method={RequestMethod.POST})
	@ResponseBody
	public Object addQueryParam (
			@PathVariable String planName,
			@PathVariable String casePath,
			@RequestBody Expression e) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("{}", new Gson().toJson(e));
		}
		
		Plan plan = repo.get(planName);
		plan.find(casePath).modifier().getQueryParams().save(e);
		repo.save(plan);
		
		return e;
	}
	
	@RequestMapping(value="/queryParams/{paramId}", method={RequestMethod.PUT})
	@ResponseBody
	public Object saveQueryParam (
			@PathVariable String planName,
			@PathVariable String casePath,
			@PathVariable Integer paramId,
			@RequestBody Expression e) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("{}", new Gson().toJson(e));
		}
		
		Plan plan = repo.get(planName);
		plan.find(casePath).modifier().getQueryParams().save(e);
		repo.save(plan);
		
		return e;
	}
	
	@RequestMapping(value="/properties", method={RequestMethod.POST})
	@ResponseBody
	public Object addProperties (
			@PathVariable String planName,
			@PathVariable String casePath,
			@RequestBody Expression e) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("{}", new Gson().toJson(e));
		}
		
		Plan plan = repo.get(planName);
		plan.find(casePath).getProperties().save(e);
		repo.save(plan);
		
		return e;
	}
	
	
	@RequestMapping(value="/queryParams/{paramId}", method={RequestMethod.DELETE})
	@ResponseBody
	public Object deleteQueryParam (
			@PathVariable String planName,
			@PathVariable String casePath,
			@PathVariable Integer paramId) {

		Plan plan = repo.get(planName);
		Expression e = plan.find(casePath).getQueryParams().removeById(paramId);
		repo.save(plan);
		return e;
	}	
	
	@RequestMapping(value="/properties/{propertyId}", method={RequestMethod.PUT})
	@ResponseBody
	public Object saveProperties (
			@PathVariable String planName,
			@PathVariable String casePath,
			@PathVariable Integer propertyId,
			@RequestBody Expression e) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("{}", new Gson().toJson(e));
		}
		
		Plan plan = repo.get(planName);
		plan.find(casePath).getProperties().save(e);
		repo.save(plan);
		
		return e;
	}

	@RequestMapping(value="/properties/{propertyId}", method={RequestMethod.DELETE})
	@ResponseBody
	public Object deleteProperty (
			@PathVariable String planName,
			@PathVariable String casePath,
			@PathVariable Integer propertyId) {

		Plan plan = repo.get(planName);
		Expression e = plan.find(casePath).getProperties().removeById(propertyId);
		repo.save(plan);
		return e;
	}
}
