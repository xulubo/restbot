package com.quickplay.tqa.webui.model;

import com.quickplay.restbot.caze.core.Plan;


public class TestPlanInfo {
	
	private String id;
	private String name;

	public TestPlanInfo() {
		
	}
	
	public TestPlanInfo(Plan plan) {
		this.id = plan.getId();
		this.name = plan.getName();
	}
	
	public TestPlanInfo(String id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
}
