package com.quickplay.tqa.webui.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.quickplay.tqa.webui.service.BrowserHttpClientFactory;

@RequestMapping("/detour")
@Controller
@Scope("session")
public class DetourRequestController {
	private static final Logger logger = LoggerFactory.getLogger(DetourRequestController.class);
	
	@Resource
	BrowserHttpClientFactory httpClientFactory;
	
	@RequestMapping("/request")
	@ResponseBody
	public Object getPendingRequest(
			@RequestParam Integer timeout) {
		return httpClientFactory.getRequest(timeout);
	}
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping("/response")
	public void reportResponse(
			@RequestParam Integer requestId,
			@RequestBody Map<String, Object> response) {
		String body = (String) response.get("body");
		Integer statusCode = (Integer) response.get("statusCode");
		logger.debug("response received, statusCode={}, body={}", statusCode, body);

		ResponseEntity<String> entity = new ResponseEntity<String>(body, null, HttpStatus.valueOf(statusCode));
		httpClientFactory.setResponse(requestId, entity);
	}
}
