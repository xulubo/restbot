package com.quickplay.tqa.webui.model;

import java.io.File;

import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.persist.reader.PlanDiscover;
import com.quickplay.restbot.persist.reader.json.JsonPlanReader;
import com.quickplay.restbot.persist.reader.json.JsonPlanWriter;

public class PlanLoader {
	private String id;
	private Plan plan;
	private File planDir;
	private File planFile;

	public PlanLoader(File planDir) {
		this.id = planDir.getName();
		this.planDir = planDir;
		this.planFile = new File(planDir, "plan.json");	
	}

	public String getId() {
		return this.id;
	}
	
	public void create() {
		if (planDir.exists()) {
			throw new RuntimeException("Plan " + this.id + "exists already");
		}
		
		if (!planDir.mkdirs()) {
			throw new RuntimeException("Failed to create directory " + planDir.getAbsolutePath());
		}
		
		this.plan = new Plan();
		this.id = planDir.getName();
		//id is not allowed to be modified, it could be better to use an UUID instead of directory name,
		//but currently, cause the plan is saved in directory, UUID is kind of hard to find a plan
		plan.setId(this.id);	
		//use the dir name as plan name by default, it can be changed later
		plan.setName(this.id);
		save();
	}
	
	//reload plan from disk
	public void load() {
		if (planFile.exists()) {
			this.plan = new JsonPlanReader().read(planFile);
		} else {
			this.plan = new PlanDiscover().read(new File(planDir, "src"));
			//put in plan.xml
			if (this.plan != null) {
//				save();
			}
		}
		
		if (this.plan == null) {
			throw new RuntimeException("PlanNotFoundException " + this.planFile.getAbsolutePath());
		}
		
		if (this.plan.getProperty("libDir") == null) {
			this.plan.setProperty("libDir", planDir.getAbsolutePath());
		}
		
		if (this.plan.getId() == null) {
			this.plan.setId(planDir.getName());
		}		

	}
	
	public void save() {
		JsonPlanWriter writer = new JsonPlanWriter();
		writer.write(plan, planFile);		
	}

	public String getName() {
		return getPlan().getName();
	}
	
	public Plan getPlan() {
		return this.plan;
	}
}