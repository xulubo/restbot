package com.quickplay.tqa.webui.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.persist.reader.PlanReaderFactory;
import com.quickplay.restbot.persist.reader.json.JsonPlanWriter;
import com.quickplay.tqa.webui.service.ConfigurationService;

@Controller
public class FileUploadController {

	@Resource
	ConfigurationService configService;

    @RequestMapping(value="/upload", method=RequestMethod.GET)
    public @ResponseBody String provideUploadInfo() {
        return "You can upload a file by posting to this same URL.";
    }

    @RequestMapping(value="/upload", method=RequestMethod.POST)
    public @ResponseBody String handleFileUpload(
            @RequestParam("file") MultipartFile file,
            @RequestParam(required=true) String planName){
    	String name = file.getOriginalFilename();
        if (!file.isEmpty()) {
            try {
        		File dir = new File(configService.getWorkDir(), planName);
            	if (FilenameUtils.isExtension(name, "xlsx")) {
            		savePlan(file, dir);
            	} else if (FilenameUtils.isExtension(name, "jar")) {
            		saveJar(file, dir);
            	}

                return "You successfully uploaded " + name + "!";
            } catch (Exception e) {
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + name + " because the file was empty.";
        }
    }

    private void savePlan(MultipartFile file, File dir) throws IOException {
    	File src = new File(dir, "src");
    	if (!src.exists()) {
    		src.mkdir();
    	}
    	
		File planFile = new File(src, file.getOriginalFilename());
		save(file, planFile);
		Plan plan = PlanReaderFactory.INSTANCE.read(planFile);
		JsonPlanWriter writer = new JsonPlanWriter();
		writer.write(plan, new File(dir, "plan.json"));
    }
    
    private void saveJar(MultipartFile file, File dir) throws IOException {
		File lib = new File(dir, "lib");
		if (!lib.exists()) {
			lib.mkdirs();
		}
		
		File libFile = new File(lib, file.getOriginalFilename());
		save(file, libFile);
    }
    
    private void save(MultipartFile file, File out) throws IOException {
        byte[] bytes = file.getBytes();
        BufferedOutputStream stream =
                new BufferedOutputStream(new FileOutputStream(out));
        stream.write(bytes);
        stream.close();
    }
}