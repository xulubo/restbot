package com.quickplay.tqa.webui.model;

import com.quickplay.restbot.caze.core.Case;


public class TestSuiteInfo {

	private String name;
	private int totalCases=0;
	private int runs=0;
	private int errors=0;
	private int failures=0;
	
	
	public TestSuiteInfo(Case suite) {
		this.name = suite.getName();
		this.totalCases = suite.getChildren().size();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTotalCases() {
		return totalCases;
	}

	public void setTotalCases(int totalCases) {
		this.totalCases = totalCases;
	}

	public int getRuns() {
		return runs;
	}

	public void setRuns(int runs) {
		this.runs = runs;
	}

	public int getErrors() {
		return errors;
	}

	public void setErrors(int errors) {
		this.errors = errors;
	}

	public int getFailures() {
		return failures;
	}

	public void setFailures(int failures) {
		this.failures = failures;
	}
	

}
