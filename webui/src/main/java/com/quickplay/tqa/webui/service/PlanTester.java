package com.quickplay.tqa.webui.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.quickplay.restbot.caze.core.CaseReport;
import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.net.HttpClientFactory;
import com.quickplay.tqa.webui.model.TestRun;

/**
 * Plan tester is for executing a single plan, and generate plan report
 * tester keeps history of all test runs
 * @author robertx
 *
 */
public class PlanTester {
	private static final Logger logger = LoggerFactory.getLogger(PlanTester.class);
	
	private Map<Integer, TestRun> runs = Maps.newConcurrentMap();
	private int lastRunId = 0;
	private Plan plan;

	public PlanTester(Plan plan) {
		this.plan = plan;
	}
	
	public Plan getTestPlan() {
		return this.plan;
	}
	
	public int runTest(Collection<String> casePaths, HttpClientFactory factory) {
		TestRun run = new TestRun(plan, ++lastRunId, factory);
		
		run.start(casePaths);
		runs.put(run.getId(), run);
		
		return run.getId();
	}
	
	public TestRun getTestRun(int runId) {
		if (runId >= runs.size()) {
			logger.error("runId {} is out of range", runId);
			return null;
		}
		return runs.get(runId);
	}

	public TestRun getTestRun(Integer runId) {
		TestRun run;
		if (runId == null) {
			run = runs.get(runs.size()-1);
		} else {
			run = runs.get(runId);
		}
		
		return run;
	}

	public List<CaseReport> getCaseReport(Integer runId) {
		TestRun run = getTestRun(runId);
		if (run == null) {
			logger.debug("testrun ({}) is not found", runId);
			return null;
		}

		return run.createReport();
	}
	
	/**
	 * Get report of specified test case
	 * 
	 * @param suiteName
	 * @param caseId
	 * @param runId  the index of the testrun, null to get the last one
	 * @return
	 */
	public CaseReport getCaseReport(String casePath, Integer runId) {
		TestRun run = getTestRun(runId);
		if (run == null) {
			logger.debug("testrun ({}) is not found", runId);
			return null;
		}
		
		return run.getCase(casePath).createReport();
	}
	

	public CaseReport getLastCaseReport(String casePath) {
		return getCaseReport(casePath, null);
	}

}
