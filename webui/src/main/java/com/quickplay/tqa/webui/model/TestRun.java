package com.quickplay.tqa.webui.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.core.CaseAllowFilter;
import com.quickplay.restbot.caze.core.CaseReport;
import com.quickplay.restbot.caze.core.CaseStatus;
import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.exceptions.CaseNotFoundException;
import com.quickplay.restbot.net.HttpClientFactory;
import com.quickplay.restbot.runner.CaseEvent;
import com.quickplay.restbot.runner.PlanExecutor;
import com.quickplay.restbot.runner.PlanExecutor.EventListener;

/**
 * A single execution of a test to a plan, it is responsible for executing the test and collecting report
 * @author robertx
 *
 */
public class TestRun implements CaseAllowFilter {
	private static final Logger logger = LoggerFactory.getLogger(TestRun.class);
	
	static enum Progress {
		BEGIN, STARTED, DONE
	};
	
	private final Integer id;
	private final PlanExecutor executor;
	private final BlockingQueue<CaseEvent> events =  new LinkedBlockingQueue<CaseEvent>();
	private final Map<String, TestSuiteProgress> caseProgressMap = Maps.newConcurrentMap();
	private Progress progress = Progress.BEGIN;
	private Collection<String> casePaths;
	private Plan plan;
	
	public TestRun(Plan plan, int id, HttpClientFactory factory) {
		this.id = id;
		this.plan = plan;
		this.executor = new PlanExecutor(plan);
		if (factory != null) {
			this.executor.getContext().setHttpClientFactory(factory);
		}
	}
	
	public int getId() {
		return id;
	}
	

	/*
	 * Get executed test case for reporting
	 */
	public Case getCase(String casePath) throws CaseNotFoundException {
		checkProgress();

		if (casePaths != null && !casePaths.contains(casePath)) {
			logger.debug("Case {} was not included in this run", casePath);
			return null;
		}
		
		logger.debug("case has executed");		

		return this.plan.find(casePath);
	}
	
	/*
	 * execute the entire plan
	 */
	public void start() {
		run(null);
	}

	/*
	 * execute specified suite
	 */
	public void start(String casePath) {
		run(Arrays.asList(casePath));
	}

	public void start(Collection<String> casePaths) {
		run(casePaths);
	}
	
	private void checkProgress() {
		if (progress == Progress.BEGIN) {
			throw new RuntimeException("TestNotStartedException");
		}

		logger.debug("check if case is still running");
		synchronized(id) {
			try {
				if (progress == Progress.STARTED) {
					id.wait(5000);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (progress != Progress.DONE) {
			throw new RuntimeException("WaitReportTimeoutException");
		}

	}
		
	private void run(Collection<String> casePaths) {
		
		this.casePaths = casePaths;
		
		progress = Progress.STARTED;
		EventListener l = new EventListener() {

			@Override
			public void onEvent(CaseEvent e) {
				logger.debug("adding event: {}", e.getStatus());
				events.add(e);
			}

		};
		executor.setEventListener(l);
		executor.setCaseFilter(this);

		new Thread(new Runnable(){

			@Override
			public void run() {
				synchronized(id) {
					executor.start();
					events.add(new CaseEvent("$system", CaseStatus.finished));
					progress = Progress.DONE;
					
					logger.debug("test is finished");
					id.notify();
				}
			}
			
		}).start();

	}
	
	private void handleEvent(CaseEvent e) {
		if (e == null) {
			logger.error("null event");
			return;
		}
		
		TestSuiteProgress p = caseProgressMap.get(e.getCasePath());
		if (p == null) {
			p = new TestSuiteProgress(e.getCasePath());
			caseProgressMap.put(e.getCasePath(), p);
		}
		
		switch(e.getStatus()){
		case starting:
			p.setRuns(p.getRuns()+1);
			break;
		case passed:
			p.setSuccesses(p.getSuccesses()+1);
			break;
		case failed:
			p.setFailures(p.getFailures()+1);
			break;
		case error:
			p.setErrors(p.getErrors()+1);
			break;
		default:
			break;
		}
	}
	
	public List<CaseEvent> getNewEvents(long timeout) {
		List<CaseEvent> newEvents = new LinkedList<CaseEvent>();
		try {
			if (events.isEmpty() && timeout > 0) {
				CaseEvent e;
				e = events.poll(timeout, TimeUnit.SECONDS);
				newEvents.add(e);
			}

			while(!events.isEmpty()) {
				CaseEvent e = events.poll();
				logger.trace("case {} {}", e.getCasePath(), e.getStatus());
				newEvents.add(e);
			}
		} catch (InterruptedException e1) {
			//ignore
		} 

		return newEvents;
	}
	
	public Collection<TestSuiteProgress> getSuiteProgress(long timeout) {
		
		List<CaseEvent> events = getNewEvents(timeout);
		
		for(CaseEvent e : events) {
			handleEvent(e);
		}
		
		return caseProgressMap.values();
	}

	@Override
	public boolean isCaseAllowed(String casePath) {
		if (casePaths == null) {
			return true;
		}
		
		return casePaths.contains(casePath);
	}

	public List<CaseReport> createReport() {
		checkProgress();
		List<CaseReport> reports = Lists.newLinkedList();
		if (casePaths == null) {
			reports.add(plan.createReport());
		} else {
			for(String p : casePaths) {
				reports.add(plan.find(p).createReport());
			}
		}
		
		return reports;
	}

}
