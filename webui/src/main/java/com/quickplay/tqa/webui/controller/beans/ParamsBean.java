package com.quickplay.tqa.webui.controller.beans;

import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class ParamsBean {
	@JsonPropertyOrder({"baseUrl"})
	public Map<String, String> params;
	public Map<String, Set<String>> history;
}
