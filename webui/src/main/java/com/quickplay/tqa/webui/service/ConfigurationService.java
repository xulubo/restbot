package com.quickplay.tqa.webui.service;

import java.io.File;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ConfigurationService {
	private static final Logger logger = LoggerFactory.getLogger(ConfigurationService.class);
	
	private static File workDir = null;
	
	
	public File getWorkDir() {
		return workDir;
	}
	
	static public void setWorkDir(String dir) {
		File file = new File(dir);
		if (!file.exists()) {
			logger.debug("creating work dir {}", dir);
			file.mkdirs();
		}
		
		if (!file.isDirectory()) {
			throw new RuntimeException("Resource directory not readable");
		}
		workDir = file;
	}
	
	@PostConstruct
	public void init() throws Exception {
		logger.debug("init");
	}
}
