package com.quickplay.tqa.webui.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.quickplay.restbot.caze.core.CaseReport;
import com.quickplay.tqa.webui.service.BrowserHttpClientFactory;
import com.quickplay.tqa.webui.service.PlanTester;
import com.quickplay.tqa.webui.service.TestOptions;

@Service
@Scope("session")
public class TestManager {

	//need to find a solution for the Cross-Origin issue first
	//currently a workaround is to add a Chrome extension https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi/related?hl=en-US
	//which enables to send cross-origin request
	@Resource
	BrowserHttpClientFactory httpClientFactory;

	@Resource
	PlanRepository repo;
	
	private Map<String, PlanTester> testers = Maps.newConcurrentMap();
	private TestOptions testOptions = new TestOptions();

	
	public int runTest(String planName) {
		return runTest(planName);
	}
	
	public int runTest(String planName, String parentCasePath, Collection<String> caseIds) {
		PlanTester tester = testers.get(planName);
		if (tester == null) {
			tester = new PlanTester(repo.get(planName));
			testers.put(planName, tester);
		}
		
		List<String> ls;
		
		if (caseIds != null) {
			ls = Lists.newArrayListWithCapacity(caseIds.size());
			for(String i : caseIds) {
				ls.add(parentCasePath + ":" + i);
			}
		} else {
			ls = Arrays.asList(parentCasePath);
		}
		
		return tester.runTest(ls, testOptions.getBrowserAsProxy()? httpClientFactory : null);
	}
	
	public int runTest(String planName, String parentCasePath) {
		return runTest(planName, parentCasePath, null);
	}
	
	public CaseReport getCaseReport(String planName, String casePath, Integer runId) {
		return getPlanTester(planName).getCaseReport(casePath, runId);
	}
	
	public List<CaseReport> getCaseReport(String planName, Integer runId) {
		return getPlanTester(planName).getCaseReport(runId);
	}
	
	public TestRun getTestRun(String planName, int runId) {
		return this.testers.get(planName).getTestRun(runId);
	}

	public void clear() {
		testers.clear();
	}	
	
	public TestOptions getOptions() {
		return this.testOptions;
	}
	
	public void saveOptions(TestOptions options) {
		BeanUtils.copyProperties(options, testOptions);
	}
	
	public PlanTester getPlanTester(String planName) {
		return this.testers.get(planName);
	}
}
