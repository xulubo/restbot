package com.quickplay.tqa.webui.model;

import java.util.List;

import com.quickplay.restbot.caze.core.Plan;


public interface PlanRepository {

	//created plan will be returned on success
	Plan create(String planName);
	
	//saved plan will be returned on success, null or lese
	Plan save(Plan plan);

	//load plan from disk
	Plan load(String planName);

	//get plan from cache
	Plan get(String planName);

	boolean delete(String planName);

	List<TestPlanInfo> getPlanInfos();

}
