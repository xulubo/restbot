package com.quickplay.tqa.webui.service;

public class TestOptions {

	private Boolean browserAsProxy = false;

	public Boolean getBrowserAsProxy() {
		return browserAsProxy;
	}

	public void setBrowserAsProxy(Boolean browserAsProxy) {
		this.browserAsProxy = browserAsProxy;
	}
	
	
}
