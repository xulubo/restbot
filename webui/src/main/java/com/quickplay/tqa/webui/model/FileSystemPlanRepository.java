package com.quickplay.tqa.webui.model;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.tqa.webui.service.ConfigurationService;

@Service
@Scope("session")
public class FileSystemPlanRepository implements PlanRepository {
	
	private static final Logger logger = LoggerFactory.getLogger(FileSystemPlanRepository.class);
	
	@Resource
	ConfigurationService confService;

	private final String DEFAULT_PLAN_NAME = "QuickTest";
	private final Map<String, PlanLoader> planLoaderMap = Maps.newConcurrentMap();
	
	@PostConstruct
	public void load() {
		planLoaderMap.clear();
		
		logger.debug("loading plan resources");
		for(File dir : confService.getWorkDir().listFiles()) {
			if (dir.isDirectory()) {
				PlanLoader loader = new PlanLoader(dir);
				loader.load();
				
				if (planLoaderMap.containsKey(loader.getId())) {
					throw new RuntimeException("duplicated plan " + loader.getId());
				}
				
				planLoaderMap.put(loader.getId(), loader);
			}
		}
		
		if (!planLoaderMap.containsKey(DEFAULT_PLAN_NAME)) {
			create(DEFAULT_PLAN_NAME);
		}
	}
	

	//TODO: should load from disk
	public Plan load(String planName) {
		PlanLoader loader = planLoaderMap.get(planName);
		if (loader == null) {
			throw new RuntimeException("Plan " + planName + " doesn't exist");
		}
		
		loader.load();
		return loader.getPlan();
	}
	
	public Plan get(String planName) {
		if (StringUtils.isBlank(planName) || planName.equals("null")) {
			planName = DEFAULT_PLAN_NAME;
		}
		
		PlanLoader h = planLoaderMap.get(planName);
		if (h == null) {
			throw new RuntimeException("Plan " + planName + " doesn't exist");
		}
		
		return h.getPlan();
	}
	

	@Override
	public Plan create(String planName) {
		
		if (planLoaderMap.containsKey(planName)) {
			throw new RuntimeException("Plan " + planName + "exists already");
		}
				
		File planDir = new File(confService.getWorkDir(), planName);
		PlanLoader loader = new PlanLoader(planDir);
		loader.create();

		planLoaderMap.put(planName, loader);

		return loader.getPlan();
	}
	
	public Plan save(Plan plan) {
		PlanLoader loader = planLoaderMap.get(plan.getId());
		
		//update
		if (loader == null) {
			throw new RuntimeException("Plan " + plan.getName() + " doesn't exist");
		}
		
		
		loader.save();
		return loader.getPlan();
	}

	public boolean delete(String planName) {
		logger.debug("deleting plan {}", planName);
		
		Plan plan = get(planName);
		if (plan == null) {
			throw new RuntimeException("can't find plan " + planName);
		}
		
		File planFolder = new File(confService.getWorkDir(), planName);
		
		logger.debug("deleting plan folder {}", planFolder.getAbsoluteFile());
		if (planFolder.exists()) {
			FileUtils.deleteQuietly(planFolder);
		}
		
		if (planFolder.exists()) {
			logger.error("failed to remove folder {}", planFolder.getAbsolutePath());
			return false;
		} 
	
		planLoaderMap.remove(planName);
		logger.debug("plan {} removed successfully", planName);
		
		return true;
	}

	//every time refresh page, tests will be reloaded from disk
	public List<TestPlanInfo> getPlanInfos() {
		
		List<TestPlanInfo> infos = Lists.newArrayList();
		for(String id : planLoaderMap.keySet()) {
			infos.add(new TestPlanInfo(id, planLoaderMap.get(id).getName()));
		}
		
		return infos;
	}
	
}
