package com.quickplay.tqa.webui.controller;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.tqa.webui.model.TestPlanInfo;
import com.quickplay.tqa.webui.service.ConfigurationService;
import com.quickplay.tqa.webui.service.TestService;

@RequestMapping({"/api/plan", "/api/p"})
@SessionAttributes(value={"context", "progress"})
@Controller
@Scope("session")
public class PlanApiController extends BaseApiController {
	Logger logger = LoggerFactory.getLogger(PlanApiController.class);
	Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	
	@Resource
	ConfigurationService confService;

	@Resource
	TestService testService;
	
	@RequestMapping(value="", method=RequestMethod.GET)
	@ResponseBody
	public Object getPlanInfos() {
		return repo.getPlanInfos();
	}
	
	/**
	 * Create a new test plan
	 * @param planName
	 */
	@RequestMapping(value="", method=RequestMethod.POST)
	@ResponseBody
	public Object createPlan(
			@RequestBody TestPlanInfo p) {
		
		Plan plan = repo.create(p.getName());
		
		return new TestPlanInfo(plan);
	}
	
	@RequestMapping(value="/{planId:[^:]*}", method=RequestMethod.GET)
	@ResponseBody
	public Object getCaseDetail(
			@PathVariable String planId) {
		
		logger.debug("get root cases planName={}", planId);
		Plan plan = repo.get(planId);
		return plan.getEntity();
	}
	
	/**
	 * Create a new test plan
	 * @param planName
	 */
	@RequestMapping(value="/{planId:[^:]*}", method=RequestMethod.DELETE)
	public ResponseEntity<String> deletePlan(
			@PathVariable String planId) {
		boolean result = repo.delete(planId);
		
		return result ? new ResponseEntity<String>(HttpStatus.OK) 
				: new ResponseEntity<String>("failed to create plan", HttpStatus.OK);
	}
	
}
