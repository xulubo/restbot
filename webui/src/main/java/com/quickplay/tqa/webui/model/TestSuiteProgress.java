package com.quickplay.tqa.webui.model;

public class TestSuiteProgress {

	private String name;
	private int successes = 0;
	private int errors = 0;
	private int failures = 0;
	private int runs = 0;
	
	public TestSuiteProgress(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public int getSuccesses() {
		return successes;
	}
	public void setSuccesses(int successes) {
		this.successes = successes;
	}
	public int getErrors() {
		return errors;
	}
	public void setErrors(int errors) {
		this.errors = errors;
	}
	public int getFailures() {
		return failures;
	}
	public void setFailures(int failures) {
		this.failures = failures;
	}

	public int getRuns() {
		return runs;
	}
	
	public void setRuns(int runs) {
		this.runs = runs;
	}
}
