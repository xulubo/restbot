package com.quickplay.tqa.webui.service;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.quickplay.tqa.webui.model.FileSystemPlanRepository;
import com.quickplay.tqa.webui.model.TestManager;

@Service
@Scope("session")
public class TestService {
	private static final Logger logger = LoggerFactory.getLogger(TestService.class);
	
	@Resource
	ConfigurationService confService;
	
	@Resource
	private FileSystemPlanRepository planRepo;
	
	@Resource
	private TestManager testManager;
	
	public TestService() {
		logger.debug("TestService is created for session");
	}
	
	public void reload() {
		testManager.clear();
		planRepo.load();
	}
	
}
