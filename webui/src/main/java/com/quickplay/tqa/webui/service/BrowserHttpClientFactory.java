package com.quickplay.tqa.webui.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.quickplay.restbot.caze.core.TestContext;
import com.quickplay.restbot.net.HttpClient;
import com.quickplay.restbot.net.HttpClientFactory;
import com.quickplay.restbot.utils.BeforeRequestStartListener;

/**
 * Consider the scenario, the test service is deployed on a public domain, and the SUT(service under test) is located 
 * in a local domain in which the tester accesses the restbot service.  In test case, RestBot is not able to access 
 * the SUT on the remote side, so this detour way is designed, the steps are 
 * 1. test request is launched in browser,
 * 2. browser starts a URL request listener simultaneously
 * 3. Restbot received test command, make the test case url and return to request listener in browser, and wait result
 * 4. browser finish the request, and post back to restbot
 * 5. restbot receive the response, do validation, then send test result back to browser
 * 
 * @author robertx
 *
 */

@Component
public class BrowserHttpClientFactory implements HttpClientFactory {
	private static final Logger logger = LoggerFactory.getLogger(BrowserHttpClientFactory.class);


	LinkedBlockingQueue<Request> pendingRequests = new LinkedBlockingQueue<>();
	ConcurrentHashMap<Integer, ResponseListener> responses = new ConcurrentHashMap<>();

	public void setResponse(int requestId, ResponseEntity<String> response) {
		ResponseListener l = responses.get(requestId);
		if (l != null) {
			try {
				l.put(response);
			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	public Request getRequest(int timeout) {
		try {
			return pendingRequests.poll(timeout, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			logger.debug(e.getMessage(), e);
			return null;
		}
	}
	
	@Override
	public HttpClient getHttpClient(TestContext ctx) {
		return new MyHttpClient(ctx);
	}
	
	class MyHttpClient implements HttpClient {
		BeforeRequestStartListener beforeRequestStartListener;
		Map<String, String> headers = Maps.newHashMap();

		public MyHttpClient(TestContext ctx) {
/*			
			for(String key : ctx.getParamMap().keySet()) {
				if (key.startsWith("header-")) {
					String value = ctx.getParam(key);
					addRequestHeader(key.substring(7), value);
				}
			}
*/		}
		
		@Override
		public void setBeforeRequestStartListener(BeforeRequestStartListener l) {
			this.beforeRequestStartListener = l;
		}

		@Override
		public ResponseEntity<String> get(String url) {
			return doRequest(HttpMethod.GET.name(), url, null);
		}

		@Override
		public ResponseEntity<String> delete(String url) {
			return doRequest(HttpMethod.DELETE.name(), url, null);
		}

		@Override
		public ResponseEntity<String> put(String url, String requestBody) {
			return doRequest(HttpMethod.PUT.name(), url, requestBody);
		}

		@Override
		public ResponseEntity<String> post(String url, String requestBody) {
			return doRequest(HttpMethod.POST.name(), url, requestBody);
		}

		@Override
		public ResponseEntity<String> head(String url) {
			return doRequest(HttpMethod.HEAD.name(), url, null);
		}

		private ResponseEntity<String> doRequest(String method, String url, String requestBody) {
			if (beforeRequestStartListener != null) {
				beforeRequestStartListener.onRequest(this);
			}

			Request q = new Request(method, url, requestBody, headers);
			ResponseListener l = new ResponseListener();
			responses.put(q.id, l);
			pendingRequests.add(q);
			try {
				return l.poll(10, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
				return null;
			}
		}

		@Override
		public void addRequestHeader(String name, String value) {
			headers.put(name, value);			
		}

	}

	@SuppressWarnings("serial")
	static class ResponseListener extends LinkedBlockingQueue<ResponseEntity<String>>{

	}

	static class Request {
		static AtomicInteger requestId = new AtomicInteger(0);
		public String method;
		public String url;
		public String data;
		public Map<String, String> headers;
		public Integer id = requestId.incrementAndGet();

		public Request(String m, String url, String data, Map<String, String> headers) {
			this.method = m;
			this.url = url;
			this.data = data;
			this.headers = headers;
		}
	}


}
