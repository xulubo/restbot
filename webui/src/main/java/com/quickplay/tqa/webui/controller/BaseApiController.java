package com.quickplay.tqa.webui.controller;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quickplay.tqa.webui.model.PlanRepository;
import com.quickplay.tqa.webui.model.TestManager;
import com.quickplay.tqa.webui.service.ConfigurationService;
import com.quickplay.tqa.webui.service.TestService;

@Controller
@Scope("session")
public class BaseApiController {
	Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	
	@Resource
	ConfigurationService confService;

	@Resource
	TestService testService;
	
	@Resource
	PlanRepository repo;

	@Resource
	TestManager tm;
}
