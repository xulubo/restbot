package com.quickplay.tqa.webui.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.quickplay.tqa.webui.model.TestRun;

@RequestMapping({"/api/plan/{planName:[^:]*}{casePath}"})
@SessionAttributes(value={"context", "progress"})
@Controller
@Scope("session")
public class CaseExecutionApiController extends BaseApiController {
	Logger logger = LoggerFactory.getLogger(CaseExecutionApiController.class);

	/**
	 * Start execution of a test case or suite in the current casePath
	 * @param planName
	 * @param casePath the parent case path
	 * @param caseIds case ID list
	 * @return
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value="/exe", method=RequestMethod.POST)
	@ResponseBody
	public Object executeSingleCase(
			@PathVariable String planName,
			@PathVariable String casePath) {
		logger.debug("exe planName={}, casePath={}", planName, casePath);
		int runId = tm.runTest(planName, casePath);
		return tm.getCaseReport(planName, runId);
	}

	/**
	 * Start execution of a test case or suite in the current casePath
	 * @param planName
	 * @param casePath the parent case path
	 * @param caseIds case ID list
	 * @return
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value="/children/exe", method=RequestMethod.POST)
	@ResponseBody
	public Object executeChildCases(
			@PathVariable String planName,
			@PathVariable String casePath,
			@RequestBody List<String> caseIds) {

		if (!casePath.startsWith(":")) {
			casePath = ":" + casePath;
		}
		
		int runId = tm.runTest(planName, casePath, caseIds);
		return tm.getCaseReport(planName, runId);
	}
	
	/**
	 * Query test progress
	 * @param planName
	 * @param timeout
	 * @param runId
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/{runId}/event", method=RequestMethod.GET)
	@ResponseBody
	public Object getCaseEvent (
			@PathVariable String planName,
			@RequestParam(required=false) long timeout,
			@RequestParam(required=true) Integer runId) {
		
		logger.trace("Enter getCaseEvent");
		
		TestRun run = tm.getTestRun(planName, runId);
		if (run != null) {
			return run.getNewEvents(timeout);
		}
		
		return null;
	}

	

	@RequestMapping(value="/{runId}/report", method=RequestMethod.GET)
	@ResponseBody
	public Object getReport (
			@PathVariable String planName,
			@PathVariable String casePath,
			@RequestParam(required=false) Integer runId) {
		
		logger.trace("Enter getProgress");
		
		return tm.getCaseReport(planName, casePath, runId);
	}
}
