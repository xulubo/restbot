<script type="text/template" id="caseListViewTemplate">
<div>
	<button id="test" class="btn btn-primary">
	<span class="glyphicon glyphicon-send"></span> Test</button>
</div>
<div class="table">
	<table class="listing">
		<thead>
<\% 
if (models && models.length > 0) {
	var tmp = _.template($("#caseListHeadTemplate").html());
	print(tmp(heads));
}
%>
		</thead>
		<tbody>
<\% 
if (models && models.length > 0) {
	var tmp = _.template($("#caseListRowTemplate").html());
	_.each(models, function(m) {
		print(tmp({h:heads, v:m}));
	});
}
%>

		</tbody>
	</table>
</div>

<div>
	<button id="deleteSuite" class="btn btn-default">Delete this suite</button>
	<button id="addNewCase" class="btn btn-default">Add new case</button>
</div>
</script>


<script type="text/template" id="caseListHeadTemplate">
			<tr id='heads'/>
				<th><input type='checkbox' id='selectAll' checked/></th>
				<th class='operation'></th>
				<th>ID</th>
				<th class='separater'><div data-name='meta' class='collapse'></div></th>

				<\% _.each(propertyHeads, function(val){ %>
					<th class='property'><\%=val%></th>
				<\% }) %>

				<th class='separater'><div data-name='param' class='collapse'></div></th>

				<\% _.each(queryParamHeads, function(val){ %>
					<th class='param'><\%=val%></th>
				<\% }) %>

				<th class='separater'><div data-name='response' class='collapse'></div></th>
				<\% _.each(validationRuleHeads, function(val){ %>
					<th class='response'><\%=val%></th>
				<\% }) %>

			</tr>
</script>
<script type="text/template" id="caseListRowTemplate">
	<tr data-id="<\%=v.id %>">
		<td><input type='checkbox' class='select' name="<\%=v.id %>" checked/></td>
		<td class='operation' nowrap>
			<span class="action glyphicon glyphicon-duplicate"></span>
			<span class="action glyphicon glyphicon-play single-test"></span>
		</td>
		<td nowrap>
			<a href="#" class="edit-case">
				<span class='glyphicon glyphicon-pencil'><\%=v.name()%></span>
			</a>
		</td>
		<td class='separater'>
			<div name='status' class='start'></div>
		</td>

		<\% _.each(h.propertyHeads, function(key){ %>
			<td class='meta'><\%=v.find('properties', key)%></td>
		<\% }) %>

		<td class='separater'></td>

		<\% _.each(h.queryParamHeads, function(key){ 
			var title="";
			var val = v.find('queryParams', key);
			val = val ? val : "";
			if(val && val.length>50) {
				title = val.replace(/"/g, '&quot;');
				val = "...";
			}
		%>
			<td class='param'>
				<span data-toggle="tooltip" title="<\%=title%>"><\%=val%></span>
			</td>
		<\% }) %>

		<td class='separater'></td>

		<\% _.each(h.validationRuleHeads, function(key){ %>
			<td class='response'><\%=v.find('validationRules',key)%></td>
		<\% }) %>

	</tr>
</script>
