<script type="text/template" id="caseMainViewTemplate">
<div class="case-panel">
	<ul class="nav nav-tabs">
		<li id="tabRequest" role="presentation"><a href="#">Request</a></li>
		<li id="tabChildren" role="presentation" class="active">
			<a href="#">Children</a>
		</li>
		<li role="presentation"><a href="#">Advanced</a></li>
	</ul>

	<div id="planContentPanel">
	</div>

</div>
</script>

<script type="text/template" id="caseEditViewTemplate">

<div class="edit-panel">

	<section id="requestSection">
		<div style="min-height: 35px;">
			<div id="urlElements"
				style="display: inline-block; position: absolute; right: 425px;">
				<div class="input-group">
					<span id="baseUrl" class="input-group-addon inline-edit url-prefix" 
						data-name="baseUrl"
						data-input-title="Input request path for this test case"
						style="margin-right: 0px; padding-right: 0px; border-right: 0px;"></span>
					<span id="requestPath" class="input-group-addon inline-edit url-prefix" 
						data-name="servicePath"
						data-input-title="Input request path for this test case"
						style="margin-right: 0px; padding-right: 0px; border-right: 0px;"></span>
					<span class="input-group-addon"
						style="padding: 0px; border-left: 0px; border-right: 0px;">?</span>
					<input id="urlParams" type="text" class="form-control">
				</div>
			</div>

			<div style="display: inline-block; width: 400px; float: right;">
				<div class="input-group-btn">
					<select id="requestMethod">
						<option value="GET">GET</option>
						<option value="POST">POST</option>
						<option value="PUT">PUT</option>
						<option value="DELETE">DELETE</option>
						<option value="OPTIONS">OPTIONS</option>
						<option value="HEAD">HEAD</option>
					</select>
					<button class="btn btn-default" data-toggle="collapse"
						data-target="#queryParams">
						<span class="glyphicon glyphicon-edit"></span> URL Params <span
							class="badge"></span>
					</button>
					<button class="btn btn-default" data-toggle="collapse"
						data-target="#requestHeaders">
						<span class="glyphicon glyphicon-edit"></span> Headers <span
							class="badge"></span>
					</button>
					<button class="btn btn-default" data-toggle="collapse" data-target="#properties">
						<span class="glyphicon glyphicon-edit"></span> Vars <span class="badge"></span>
					</button>
				</div>
			</div>
		</div>
	</section>


	<section id="caseParameterSection">
<div id="queryParams" class="param-group collapse" data-name-placeholder="Query Parameter Key">
</div>
<div id="requestHeaders" class="param-group collapse" data-name-placeholder="Header">
</div>
<div id="properties" class="param-group collapse" data-name-placeolder="">
</div>

<div id="data" class="collapse">
	<div class="btn-group" role="group" aria-label="...">
		<button type="button" class="btn btn-default">from-data</button>
		<button type="button" class="btn btn-default">x-www-form-urlencoded</button>
		<div class="btn-group" role="group">
			<button type="button" class="btn btn-default dropdown-toggle active"
				data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				raw <span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				<li><a data-editor-mode="text" data-language="text">Text</a></li>
				<li><a data-editor-mode="javascript" data-language="json">JSON</a></li>
				<li><a data-editor-mode="xml" data-language="xml">XML</a></li>
			</ul>
		</div>
	</div>

	<div id="body-data-containers">
		<textarea name="data" id="requestBody" tabindex="4" class="inputText"></textarea>
	</div>
</div>
	</section>

	<section id="validatorSection">
		<div name="validator-selector-panel">
		<div class="btn-group" role="group">
			<span class="dropdown-toggle"
				data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Validation Rules <span class="caret"></span>
			</span>
			<ul id="responseValidatorSelector" class="dropdown-menu">
				<li><a data-validator="header" data-toggle="clappse"
					data-target="#headerValidator">Validate Headers</a></li>
				<li><a data-validator="body" data-toggle="clappse"
					data-target="#bodyValidator">Validate Response Body</a></li>
				<li><a data-validator="custom">Customized Rule</a></li>
				<li><a data-validator="jsion-schema">JSON Schema Validator</a></li>
				<li><a data-validator="json-template">JSON Template
						Validator</a></li>
			</ul>
		</div>
		</div>


		<div id="responseValidatorPanel">
			<div 
				id="headerRules" 
				class="validator-group collapse in" 
				data-prefix="H."
				data-title="Validate Response Header"
				data-name-placeholder="Header"
			></div>
			<div id="bodyRules"   class="validator-group collapse in" data-prefix="$."
				data-title="Validate Json Response Body" data-name-placeholder="Response Data Path"></div>
			<div id="customRules"   class="validator-group"
				data-title="Customized Validation Rule" data-name-placeholder="Validation Key"></div>
		</div>
	</section>

	<secton id="case-cmds">
		<button id="save" class="btn btn-primary">Save</button>
		<button id="preview" class="btn">Preview</button>
		<button id="execute" class="btn">Execute</button>
		<button id="remove" class="btn">Delete</button>
		<!-- Use the current case as parent, spawn child cases -->
		<button id="spawn" class="btn">Spawn</button>
	</section>

</div>

<div id="responsePanel" class="response-panel">
</div>
</script>


<script type="text/template" id="responseValidationPanelTemplate">
<div class="panel panel-default" style="display: block;">
	<div class="panel-heading">
		<span class="num"><\%=title%></span> 
<a href="#"><span class="glyphicon glyphicon-remove"></span></a> 
<a href="#"	data-toggle="collapse" data-target="#<\%=id%>">
<span class="glyphicon glyphicon-minus"></span></a> 
<a href="#"><span class="glyphicon glyphicon-question-sign"></span></a>
	</div>

	<div id="<\%=id%>" class="panel-body collapse in">
<\% 

var tpl = _.template($("#responseValdationRuleTemplate").html());
_.each(rules, function(rule, key){ 
	print(tpl({
		dataPath: rule.dataPath,
		op: rule.op,
		value: rule.right,
		placeHolder:placeHolder,
	}));

}) 
	print(tpl({
		op			:null,
		dataPath	: "",
		value		: "",
		placeHolder	:placeHolder
	}));

%>
	</div>
</div>
</script>

<script type="text/template" id="responseValdationRuleTemplate">

		<div class="param" data-id="<\%=dataPath%>">
			<input class="name" value="<\%= dataPath ? dataPath : "" %>" placeholder="<\%=placeHolder%>">
			<select class="op">
			<\%   
				var options = {
					"eq" : "==",
					"gt" : "&gt;",
					"ge" : "&gt;=",
					"lt" : "&lt;",
					"le" : "&lt;=",
					"ct" : "contains",
					"in" : "in",
					"mt" : "matches",
				};
				
				_.each(options, function(val, key){
			%>
				<option value="<\%=key%>" <\%= (op==key) ? "selected" : "" %>> <\%=val%> </option>

			<\% }) %>
			</select> 

			<input class="value" value="<\%- value ? value : "" %>" placeholder="Value"> 
			<span class="glyphicon glyphicon-remove-sign remove-param" />
		</div>

</script>





<script type="text/template" id="caseReportTemplate">
<div class="modal fade" id="caseReport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Case Execution Report</h4>
      </div>
      <div class="modal-body">
	<div>
		<div>
			<span class="label">CaseID:</span> <span class="text"><\%=id%></span>
			<span class="label">Result:</span> <span class="text"><\%=result%></span>
		</div>
		<div>
			<span class="label">Description:</span>
			<span><\%=description%></span>
		</div>
<\% if (result === "failed") { %>
		<div>
			<span class="label">Errors:</span>
			<div class="text"><\%=errors%></div>
		</div>
<\%}%>
		<hr/>
		<div>
			<div class="label">URL:</div>
			<div class="text"><\%=url%></div>
		</div>
		<div>
			<div><span class="label">Response:</span></div>
			<\% 
				var tpl = _.template($("#caseReportResponseBodyTemplate").html());
				print(tpl({responseHeader:responseHeader, response:response}))%>

		</div>
	</ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</script>

<script type="text/template" id="caseReportResponseBodyTemplate">
<div id="responseFormatters" class="btn-group" role="group"
	aria-label="...">
	<button type="button" class="btn btn-default"
		data-target="#responseHeader">Headers</button>
	<button type="button" class="btn btn-default active"
		data-target="#responseBody">Raw</button>
	<button type="button" class="btn btn-default"
		data-target="#responseBodyJson">Json</button>
	<button type="button" class="btn btn-default"
		data-target="#responseBodyXml">Xml</button>
</div>
<div id="caseReportResponseBodyPanel">
	<div id="responseHeader" class="large-text collapse">
		<pre><\%-responseHeader%></pre>
	</div>
	<div id="responseBody" class="large-text collapse in">
		<pre><\%- response %></pre>
	</div>
	<div id="responseBodyJson" class="large-text collapse">
		<pre>
				<\%-response ? vkbeautify.json(response) : ""%></pre>
	</div>
	<div id="responseBodyXml" class="large-text collapse">
		<pre>
				<\%-response ? vkbeautify.xml(response) : ""%></pre>
	</div>
</div>
</script>


<script type="text/template" id="paramGroupTemplate">
<\% _.each(list, function(ex, idx) { %>
    <div class="param <\%=_.result(ex, 'inherited')?"inherited":""%>" data-id="<\%=ex.attributes.id%>">
        <input class="name" value="<\%=ex.attributes.name%>" <\%=_.result(ex, 'inherited')?"readonly":""%>>: 
        <input class="value" value="<\%-ex.attributes.value%>" <\%=_.result(ex, 'inherited')?"readonly":""%>> 
        <span  class="glyphicon glyphicon-remove-sign remove-param"/>
    </div>
<\% }) %>

    <div class="new param">
        <input class="name" value="" placeholder="<\%=namePlaceHolder%>">:
        <input class="value" value="" placeholder="Value"> 
        <span  class="glyphicon glyphicon-remove-sign remove-param" />
    </div>
	<div class="alert alert-warning" role="alert">hello</div>	
</script>
