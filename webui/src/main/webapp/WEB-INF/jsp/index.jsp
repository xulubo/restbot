<!DOCTYPE html>
<html>
<head>
    <title>RESTFul API Automation Test System</title>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <script src="/libs/jquery/jquery-2.1.4.js"></script>
	<script src="/libs/underscore.js/underscore-1.8.3.js"></script>    
	<script src="/libs/backbone.js/backbone-1.2.3.js"></script>    
	<script src="/libs/dropzone/4.0.1/dropzone.js"></script>
	<script src="/libs/jquery-contextmenu/1.6.5/jquery.contextMenu.js"></script>
	<script src="/libs/purl/2.3.1/purl.min.js"></script>
	<script src="/libs/bootstrap/3.3.5/js/bootstrap.js"></script>
	<script src="/libs/codemirror/5.7.0/codemirror.js"></script>
	<script src="/libs/jquery.codemirror.js"></script>
	<script src="/libs/vkbeautify.js"></script>
	
	<script src="/script/models.js"></script>    
	<script src="/script/webapi.js"></script>    
	<script src="/script/options.js"></script>    
	<script src="/script/views.js"></script>    
	<script src="/script/case-editor.js"></script>    
	<script src="/script/restbot.js"></script>   
	 
	<link href="/libs/dropzone/4.0.1/basic.css" rel="stylesheet" type="text/css"/>
	<link href="/libs/dropzone/4.0.1/dropzone.css" rel="stylesheet" type="text/css"/>
	<link href="/libs/jquery-contextmenu/1.6.5/jquery.contextMenu.css" rel="stylesheet" type="text/css"/>
	<link href="/libs/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />	
	<link href="/libs/codemirror/5.7.0/codemirror.css"/>
    <link href="/css/admin.css" rel="stylesheet" type="text/css" />
</head>
	<body><jsp:include page="main-layout.jsp" />

<script>
$(function(){
	window.mainView = new MainView();
	window.mainView.render();
	new OptionsView();
	
	//listenUrlRequest();
	
	var urlRequestListenerTimeout;
	
	function postResponse(id, data, textStatus, xhr) {
		var response = {
			id: id,
			body: data,
			statusCode: xhr.status
		};
		console.log("post response: " + JSON.stringify(response));
		$.ajax({
			method: "POST",
			url:  "/detour/response?requestId=" + id,
			contentType: "application/json",
			data: JSON.stringify(response)
		});
	}

	function executeRequest(request) {
		if (request === "") {
			return;
		}
		console.log("received request: id=" + request.id + " url=" + request.url);
		$.ajax({
			method: request.method,
			url:  request.url,
			data: request.data,
			headers: request.headers
		}).success(function(data, textStatus, xhr){
			postResponse(request.id, data, textStatus, xhr);
		}).error(function(xhr, textStatus, errorThrown){
			postResponse(request.id, xhr.responseText, textStatus, xhr);			
		});
	}

	function listenUrlRequest() {
		if (urlRequestListenerTimeout != null) {
			clearTimeout(urlRequestListenerTimeout);
		}
		var start = new Date();
		console.log("listening to request");
		$.get("/detour/request?timeout=5", executeRequest).complete(function(){
			var end = new Date();
			if (end.getTime() - start.getTime() < 1000) {
				setTimeout(listenUrlRequest, 1000);
			} else {
				setTimeout(listenUrlRequest, 1);				
			}
		});
	}
	
	$(document).on('click', function(event) {
		  if (!$(event.target).closest('#caseReport').length) {
		  	$("#caseReport").remove();
		  }
		});	
})
</script>

<jsp:include page="templates/templates.jsp"/>

</body>
</html>

