//being executed right away with plus symbol
$(function(){
	
	function Plan(planName) {
		
		function Suite(suiteName) {
			var Model = Backbone.Model.extend({
				suiteName: suiteName, 
				url : function() {
					return "/api/plans/" + planName + "/suites/" + suiteName;
				},
				param : function(name, successCallback, ctx) {
					var url = this.url() + "/params/" + name;
					var model = new Backbone.Model();
					model.url = url;
					
					if (successCallback) {
						model.on("sync", function(data){
							successCallback.call(ctx, data.get(name));
						});					
					}
					model.fetch();
				}
			});
			
			return new Model();		
		}
		
		var Model = Backbone.Model.extend({
			url : function() {
				return "/api/plans/" + planName;
			},
			param : function(name, successCallback, ctx) {
				var url = this.url() + "/params/" + name;
				var model = new Backbone.Model();
				model.url = url;
				
				if (successCallback) {
					model.on("sync", function(data){
						successCallback.call(ctx, data.get(name));
					});					
				}
				model.fetch();
			},
			
			suite : function(name) {
				return new Suite(name);
			}
		});
		
		return new Model();
	}
	

	
	$.restapi = {
		plan : function(name) {
			return new Plan(name);
		}	
	};
	
/*	
	var plan = $.restapi.plan("WVP");
	plan.param("baseUrl", function(val){
		console.log(val);
	});
*/	
});