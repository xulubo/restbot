var OptionsView = Backbone.View.extend({
	url : "/api/options",
	el : "#testOptions",
	events : {
		"click .option" : "save"
	},
	initialize: function() {
		this.model = new Backbone.Model();
		this.model.url = this.url;
		this.listenTo(this.model, "sync", this.render);
		this.model.fetch();
	},
	render: function() {
		var view = this;
		var obj = this.model.toJSON();
		console.log(obj);
		$.each(obj, function(key, value){
			view.$("#" + key).attr("checked", value);
		});
	},
	save : function(e) {
		this.model.set(e.target.id, e.target.checked);
		this.model.save();
	}
});

window.OptionsView = OptionsView;

