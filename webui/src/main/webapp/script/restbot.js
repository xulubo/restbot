function PlanNavView(el, planCollection) {
	
	var View = Backbone.View.extend({
		el : el,
		collection : planCollection,
		template : _.template($("#planListTemplate").html()),
		events: {
			"click ul#planList>li.plan>a" 		: "onPlanChange",
			"click ul#planList>li#newplan>a" 	: "createPlan",
			"click .remove" 					: "removePlan"
		},
	
		initialize : function() {
			this.listenTo(this.collection, "destroy", this.render);
			this.listenTo(this.collection, "sync", this.render);
			this.listenTo(this.collection, "change", this.render);
			this.collection.fetch();
		},
	
		render : function() {
			console.log(this.collection.toJSON());
			this.$el.html(this.template({
				plans: this.collection.toJSON()
			}));
			
			return this;
		},
	
		onPlanChange : function(e) {
			e.preventDefault();
			var planName = $(e.target).html();
			console.log("selected plan " + planName);
			mainView.changePlan(planName);			
		},
		
		createPlan : function() {
			var planName = prompt("Please enter new plan name", "");
			if (planName != null) {
				this.collection.create({name : planName});
			}
		}
	});
	return new View();
}



$(function(){

	window.MainView = Backbone.View.extend({
		el 			: "#main",
		activePlan	: "unplanned",
		breadcrumbs : null,
		
		events: {
			"click #reload"			: "reload",
		},

		initialize : function() {
			this.planList = new PlanCollection();
			new PlanNavView(this.$("#planListPanel"), this.planList).render();

			this.breadcrumbs = new BreadCrumbsView(this.$("#breadcrumbs"), this.$("#content")).render();
		},

		render : function() {
			return this;
		},
		
		content : function(viewNav) {
			this.breadcrumbs.show(viewNav);
		},
		
		//called when the current selected test plan is changed
		changePlan : function(name) {
			this.activePlan = this.planList.getPlan(name);
			var caseViewNav = new CaseMainViewNav(null, this.activePlan);
			this.content(caseViewNav);			
		}
	});

});

