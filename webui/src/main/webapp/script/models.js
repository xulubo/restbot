$(function(){
	
	_.mixin({
		trim : function(s) { return _.isString(s) ? s.trim() : s; }
	})
	
	//Validation Rules
	var ParamModel = Backbone.Model.extend({
		validate: function(attrs, options){
			if (!attrs.name) {
				return "parameter key must be set";
			}
		},
		
		//to name value pair
		toPair : function() {
			return this.attributes.name + "=" + this.attributes.value;
		}
	});

	var ParamList = Backbone.Collection.extend({
		model: ParamModel,
		
		toQueryString : function() {
			var v = _.invoke(this.models, 'toPair').join("&");
			//return _.map(this.models, function(m){return m.toPair();}).join("&");
			return v;
		},
		
		save: function(options) {
			options = options || {};
			options = _.extend(options, {
				url: _.result(this, 'url') + "?action=reset"
			})
			this.sync("create", this, options);
		}
	});

	var ComponentModel = Backbone.Model.extend({


	});
	

	var ReportModel = ComponentModel.extend({

	});
	
	
	var CaseModel = ComponentModel.extend({
		
		defaults : {
			//httpHeaders		: [],
			//properties 		: [],
			//queryParams 	: [],
			//validationRules : [],
			//children 		: []
		},
		
		initialize : function() {
			
		},
		
		url : function() {
			var id = this.get("id");
			var urlRoot = _.result(this, 'urlRoot') || _.result(this.collection, 'url');
		
			//case/a:b == case/a/children/b for GET, PUT, DELETE operation , remove "/children" for shortening the URL
			//when id is undefined, it is used for create a new method, so /children must exists in that case
			if (this.id) {
				urlRoot = urlRoot.replace(/\/children$/, '');
			}
			
			//assure urlRoot is tailing with a single colon ($& means lastMatch)
			return id 
					? (this.isRoot ? urlRoot + id : urlRoot.replace(/[^:]$/, '$&:') + id) 
					: urlRoot;
		},
		
		parse : function(response, options) {					
			//console.log("received:" + JSON.stringify(response));
			return response;
		},
		name : function() {
			var p = _.findWhere(this.attributes.properties, {name:"name"});
			return p ? p.value : this.get("id");
		},
		hasChildren : function() {
			return !_.isEmpty(this.attributes.children);
		},
		children : function() {
			children = new CaseList(_.sortBy(_.values(this.attributes.children), "id"));
			children.parent = this;
			children.url = this.url().replace(/[^\/]$/,'$&/') + "children";
	
			return children;
		},
		
		//find value matching name in an array of [{name:aa, value:12},...]
		//ex: find("queryParams", "description"), it is equivalent to get("queryParams").description
		//but find() will handle the case of undefined value
		find : function(key1, name) {
			var v = this.get(key1);
			v=_.findWhere(v, {name:name});
			if (v) return v.value;
			
			//find parameter in parent model
			if (this.collection && this.collection.parent) {
				return this.collection.parent.find(key1, name);
			}
			
			return "";
		},
		
		//return an attribute of array as a collection, it is used for getting queryParams, properties, httpHeaders and validationRules as collection
		list : function(key) {
			var C = ParamList.extend({
				model: ParamModel,
				url : this.url() + "/" + key
			});
			
			var v = new C(this.get(key));
			if (this.collection && this.collection.parent) {
				var l = this.collection.parent.list(key);
				_.each(l.models, function(m){
					if (v.where({name:m.get("name")}).length == 0)	{
						m.inherited=true;
						
						//because "id" is from parent parameter set, it could be duplicated in the
						//current set, and so may not be able to be added in collection
						//and the parameter is also regarded as new to the current model, so the id 
						//value is useless
						m.unset("id")
						v.add(m);
					}
				});
			}

			this[key] = v;
			return v;
		},		

		//get rules matching specified pattern
		filterRule : function(array, pattern) {
			return _.filter(this.get("validationRules"), function(r){
				return r.dataPath.match(pattern);
			});
		},
		
		//TODO: build request URL
		requestUrl : function() {

		},
		
		//execute the case and return a report model
		execute : function() {
			var url = this.url();
			var M = Backbone.Collection.extend({
				url : url + "/" + "exe", 
				save : function(options) { Backbone.sync('create', this, options) }
			});
			var e = new M();
			console.log("url: " + e.url);
			e.save({
				success:function(response, status, jqXHR){ 
					e.add(response);	
				}
			});
			return e;
		},
		
		getReport : function() {
			return new ReportMode({
				urlRoot : this.url() + "/reports"
			});
		},
		
		//get a child case model
		getChild : function(id) {
			if (this.children) {
				return this.children.get(id);
			}
		}
	});
	
	var CaseList = Backbone.Collection.extend({
		model: CaseModel,
		
		//create table heads by parsing all the cases in the list
		heads : function() {
			var propertyHeads = [], queryParamHeads = [], validationRuleHeads = [];

			_.each(this.models, function(m) {
				if (!m.inherited) {
					m = m.attributes;
					var i = _.map(m.properties, _.iteratee('name'));
					propertyHeads = _.union(propertyHeads, 		_.map(m.properties, _.iteratee('name')));
					queryParamHeads = _.union(queryParamHeads, 		_.map(m.queryParams, _.iteratee('name')));
					validationRuleHeads = _.union(validationRuleHeads, 	_.map(m.validationRules, _.iteratee('name')));					
				}
			});
			
			return {
				propertyHeads 		: _.sortBy(propertyHeads),
				queryParamHeads 	: _.sortBy(queryParamHeads),
				validationRuleHeads : _.sortBy(validationRuleHeads)
			}
		}
		
	});


	//list all plan names
	var PlanCollection = Backbone.Collection.extend({
		url	: "/api/plan/",
		model : CaseModel,
		getPlan : function(name) {
			var plan = this.findWhere({name:name});
			plan.isRoot = true;
			return plan;
		}
	});
	
	window.PlanCollection = PlanCollection;
	window.CaseModel = CaseModel;
	window.CaseList = CaseList;
});