$(function(){
	//navigable view holder, used to navigate view tree, it is used in BreadCrumbsView
	// * --- page content
	// + --> navigate to next level
	//Home
	//   + --> Plan1
	//   |      *---children (add,delete,clone,execute,report)
	//   |      |       + --> case1
	//   |      |       |         * --- children (add, delete, clone, execute, report)
	//   |      |       |         |            +--> case1.1
	//   |      |       |         * --- edit (save, execute, preview, cancel)
	//   |      |       |                  
	//   |      |       + --> case2
	//   |      |
	//   |      | 
	//   |      |
	//   |      *---Parameters
	//   |
	//   | 
	//   + --> Plan2
	window.Navigable = function(){
		var ctx = this;
		return {

			cid : _.uniqueId("nav"),
			
			//view name, used as identifier
			name : function() {
				return _.result(ctx, 'name') || 'undefined';
			},

			//return or set the parent nav view
			parent: function(p) {
				if (p != undefined) {
					ctx.parentNav = p;
				}
				return ctx.parentNav;
			},

			//render the view
			render : function() {
				return new ctx.View().render();
			}

		};
	};
	
	var ContentView = Backbone.View.extend({
		content : function(nav) {
			mainView.content(nav);
		}
	});

	window.ContentView = ContentView;
});

function BreadCrumbsView (el, dest) {
	var View = Backbone.View.extend({
		el : el,
		dest	: dest,
		template: _.template($("script#breadcrumbsTemplate").html()),
		crumbs : {},
		
		events : {
			"click .breadcrumb a" 	: "changeView"			
		},
		
		initialize : function() {
		},
		
		show : function(viewNav) {
			this.$el.html(this.template());
			this.crumbs = {};
			if (viewNav) {
				this.dest.html(viewNav.render().el);
				var crumbTemplate = _.template("<li id='<%=viewId%>'><a href='#'><%=title%></a></li>");
				var v = viewNav;
				do {
					this.crumbs[v.cid] = v;
					var title = v.name();
					this.$("ol.breadcrumb>#homecrumb").after(crumbTemplate({
						viewId : v.cid,
						title  : title
					}));

					v = v.parent();
				} while(v != null);				
			}
			
			return this;
		},
		
		changeView : function(e) {
			var li = $(e.target).parent("li");
			var id = li.attr("id");
			var view = this.crumbs[id];
			li.nextAll("li").remove();
			window.mainView.content(view);
		}
	
	});
	
	return new View();
};

function CaseMainViewNav(parentNav, planModel) {
	this.name = function() {
		return planModel.name();
	};
	
	//Navigable will call this variable to navigate to parent view
	this.parentNav = parentNav;
	var nav = Navigable.apply(this);	

	this.View = ContentView.extend({
		model: planModel,
		template : _.template($("#caseMainViewTemplate").html()),
		
		events: {
			"click li#tabPlanParam>a"	: "showParamEditor",
			"click li#tabChildren>a"	: "showChildren",
			"click li#tabRequest>a"		: "showRequest"
		},
	
		initialize : function() {
			this.listenTo(this.model, "sync", this.render);
			var url = _.result(this.model, 'url');
			if (url != undefined) {
				console.log("fetching " + url);
				this.model.fetch();				
			}
		},
		
		render : function() {
			console.log("rendering");
			this.$el.html(this.template());
			if (this.model.hasChildren()) {
				this.showChildren();				
			} else {
				this.showRequest();
			}
			return this;
		},
		
		showActiveTab : function(s) {
			this.$("ul.nav-tabs>li.active").removeClass("active");
			this.$("ul.nav-tabs>li" + s).addClass("active");							
		},

		showRequest : function() {
			this.showActiveTab("#tabRequest");
			var view = new CaseEditView(nav, this.model);
			this.$("#planContentPanel").html(view.render().el);						
		},
		
		//the tab containing children collection
		showChildren	: function() {
			this.showActiveTab("#tabChildren");
			var listView = new CaseListView(nav, _.result(this.model, 'children'));
			this.$("#planContentPanel").html(listView.render().el);							
		},
		
		showParamEditor : function(e) {
			this.showActiveTab(e);
			var paramsEditView = new ParamsEditView(this.model.getParameterList());
			this.$("#planContentPanel").html(paramsEditView.render().el);			
		}
		
	});

	return nav;
}

function CaseListView(parentNav, caseList) {

	var ReportDialog = Backbone.View.extend({
		
	});
	
	var View = ContentView.extend({
		collection 		: caseList,
		template		: _.template($("#caseListViewTemplate").html()),
		parentNav		: parentNav,
		
		events: {
			"change #selectAll"					: "selectAll",
			"click #test"						: "testSelected",
			"click .single-test"				: "testSingle",
			"click .edit-case"					: "editCase",
			"click th.separater>div"			: "operateColumns",
			"click div[name='status']" 			: "showCaseReport",
			"click #deleteSuite"				: "removeChildren",
			"click #addNewCase"					: "addNewCase",
			"click .operation>.glyphicon-duplicate"	: "duplicateCase",
		},

		initialize : function() {
			this.reportDialog = new ReportDialog();
			this.listenTo(this.collection, "sync", this.render);
			
			//model will trigger event to notify case status change, mainly for execution status update
			this.listenTo(this.collection, "event", this.update);

			//don't fetch it, because it is already filled along with the parent model
			//this.collection.fetch();
		},

		render : function() {
			if (this.collection) {
				this.$el.html(this.template({
					heads : this.collection.heads(),
					models: this.collection.models
					}));				
			} else {
				this.$el.html(this.template({
					heads : null,
					models: null
					}));								
			}
			return this;
		},

		editCase : function(e) {
			var caseId = $(e.target).parents("tr").attr("data-id");
			var child = this.collection.findWhere({id:caseId});
			
			var childNav = new CaseMainViewNav(this.parentNav, child);
			this.content(childNav);
		},
		
		addNewCase : function(e) {
			var p = this;
			var child = this.collection.create(null, {
				error: function(model, resp, options) {
					console.error(resp.responseText);
				}
			});
		},
		
		selectAll: function(e) {
//			this.$("input.select").each(function(idx, checkbox){
//				checkbox.checked = e.target.checked;
//			});			
			this.$("input.select").attr("checked", e.target.checked);
		},

		showCaseReport : function(e) {
			var caseId = this.findTargetCaseId(e.target);
			this.reportDialog.render(this.model.get(caseId));
		},

		singleTest : function(e) {
			var exe = this.model.getExeModel();
			exe.set(this.findTargetCaseId(e.target));
			exe.start();
		},

		batchTest: function() {
			var exe = this.model.getExeModel();
			var caseIds = this.$("table.listing .select:checked").attr("data-id");
			exe.set(caseIds);
			exe.start();
		},
		
		findTargetCaseId : function(target) {
			var id = $(target).parents("tr").attr("data-id");
			if (!id) {
				console.error("tr[data-id] is not set");
			}
			return id;
		},
		
		cloneCases : function(e) {
			var clone = this.model.getCloneModel();
			clone.add(this.findTargetCaseId(e.target));
			clone.start();
		},
		
		//update status upon received events
		update : function() {
			
		},
		
		removeChildren : function() {
			console.log("removing...");
			this.content(parentNav.parent());
		}

	});

	return new View();	
};

function ReportView(modelUrl, modal) { 

	var View = Backbone.View.extend({
		template 		: _.template($("#caseReportResponseBodyTemplate").html()),
		modalTemplate 	: _.template($("#caseReportTemplate").html()),

		events : {
			"click #responseFormatters>.btn"	: "switchResponseView",
		},
		
		render : function() {
			this.model.once("sync", modal ? this.showModal : this.show, this);
			this.model.fetch();
			
			return this;
		},

		show : function() {
			this.$el.html(this.template(this.model.toJSON()));
		},

		showModal : function() {
			this.$el.html(this.modalTemplate(this.model.toJSON()));
			this.$("#caseReport").modal('show');			
		},

		switchResponseView : function(e) {
			this.$("#caseReportResponseBodyPanel>.in").removeClass("in");
			var target = $(e.target).attr("data-target");
			$(target).addClass("in");
			this.$("#responseFormatters>button.active").removeClass("active");
			$(e.target).addClass("active");
		}
	});
	
	var report = new Backbone.Model();
	report.url= modelUrl;

	return new View({model:report});
};


