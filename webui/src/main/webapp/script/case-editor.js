//attributes is {list: <parameter list>, namePlaceHolder:}
function ParamGroupView(el, paramCollection) {
	var View = Backbone.View.extend({
		el			: el,
		collection	: paramCollection,
		template	: _.template($("#paramGroupTemplate").html()),
		events		: {
			"change .param>input"		: "save",
			"click .remove-param"		: "remove",
			"click .param:last>input" 	: "create"
		},
		
		initialize : function() {
			this.listenTo(this.collection, "invalid", this.error);
			this.listenTo(this.collection, "sync", this.update);
			this.listenTo(this.collection, "reset", this.render);
		},
		
		render : function () {
			this.$el.html(this.template({
				list : this.collection.models,
				namePlaceHolder : this.$el.attr("data-name-placeholder")
			}));
			this.$(".param:last").css("display:none");
			return this;
		},
		
		create : function(e) {
			var p = $(e.target).parents(".param");
			p.after(p.clone());
			p.removeClass("new");
			p.addClass("unsaved");
		},
		
		save : function(e) {
			var p = $(e.target).parents(".param");
			if (p.hasClass("inherited")) {
				console.log("inherited parameter, not allowed to edit or overwrite currently, will do later")
				return;
			}
			
			var id = p.attr("data-id");
			var name = _(p.children(".name").val()).trim();
			var val = _(p.children(".value").val()).trim();
			
			var m = id ? this.collection.get(id) : this.collection.create();
			if (m.attributes.name != name || m.attributes.value != val) {
				console.log("saving parameter, url:" + m.url());
				p.addClass("unsaved");
				m.save({name:name, value:val}, {success:function(model){
					if (!id) p.attr("data-id", model.get("id"));
					p.removeClass("unsaved");					
				}});				
			}
		},
		
		remove : function(e) {
			var p = $(e.target).parents(".param");
			var id = p.attr("data-id");
			if (!id) {
				p.remove();
				return;
			}
			var m = this.collection.get(id);
			if (m && confirm("Are you sure to delete this parameter?")) {
				m.destroy({success:function(){
					p.remove();
				}, error: function(model, response) {
					alert(response);
				}});					
			} else {
				console.error("model doesn't exist? new created? check code")					
			}
		},
		
		error : function(model, error) {
			this.$(".alert").html(error);
			this.$el.addClass("error");
		}
	});
	
	return new View();
}

function RuleGroupView(el, ruleModel) {
	var View = Backbone.View.extend({
		el			: el,
		model	: ruleModel,
		template	: _.template($("#responseValidationPanelTemplate").html()),
		
		render : function (params) {
			this.$el.html(this.template({
				rules : this.model.toJSON(),
				id		: this.$el.attr("id"),
				title: this.$el.attr("data-title"),
				placeHolder : this.$el.attr("data-name-placeholder")
			}));
			
			return this;
		},
	});
	
	return new View();
}

function ResponseView(el, reportModel) {
	var View = Backbone.View.extend({
		el : el,
		model : reportModel,
		template : _.template($("#caseReportResponseBodyTemplate").html()),
		events : {
			"click #responseFormatters>.btn"	: "switchResponseView",
		},

		render : function() {
			this.$el.html(this.template(this.model.toJSON()));
		},
		
		switchResponseView : function(e) {
			this.$("#caseReportResponseBodyPanel>.in").removeClass("in");
			var target = $(e.target).attr("data-target");
			$(target).addClass("in");
			this.$("#responseFormatters>button.active").removeClass("active");
			$(e.target).addClass("active");
		}		
	});
	
	return new View();
}

function CaseEditView (parentNav, caseModel) {
	var View = ContentView.extend({
		model				: caseModel,
		template 			: _.template($("#caseEditViewTemplate").html()),
		
		events : {
			"blur  .param>input"					: "doUpdateParam",			//save on any parameter change
			"blur  .rule>input"						: "doUpdateRule",			//save on any rule change
			"click .param:last,.rule:last"			: "doAddNewParam",			//add new a new parameter line 
			"click .param>.remove"					: "doRemoveParam",			//remove a parameter line
			"click .rule>.remove"					: "doRemoveRule",			//remove a parameter line
			"click #case-cmds>#save" 				: "doSave",					//save case
			"click #case-cmds>#remove"				: "doRemove",				//remove case
			"click #case-cmds>#execute"				: "doExecute",				//execute case after save
			"click #case-cmds>#preview"				: "doPreview",				//preview request after save
			"click #responseValidatorSelector"		: "doShowRuleGroup",		//show selected rule panel
			"click #rules>.group-remove" 			: "doRemoveRuleGroup",		//remove the whole group of parameters
			"change #urlParams"						: "doParseUrl",				//parse request URL
			"change #requestMethod"					: "onChangeMethod",			//toggle request body panel on method change
		},
		
		initialize : function() {
			this.$el.html(this.template(this.model.attributes));

			this.listenTo(this.model, "sync", this.render);
			this.listenTo(this.queryParams, "change", this.updateRequestUrl);

			this.listenTo(this.model, "change", this.updateBadge);
			this.listenTo(this.model, "destroy", this.back);
		},

		
		render : function() {
			this.queryParams = this.model.list('queryParams');
			new ParamGroupView(this.$("#queryParams"), this.queryParams).render();
			
			this.properties = this.model.list('properties');
			new ParamGroupView(this.$("#properties"), this.properties).render();
			
//			this.$("#requestHeaders").html(new ParamGroupView(this.model.requestHeaders).render().el);
//			this.$("#caseParams").html(new ParamGroupView(this.model.caseParams).render().el);
//			this.$("#headRules").html(new RuleGroupView(this.model.headRules).render().el);
			new RuleGroupView(this.$("#bodyRules"), this.model.list('validationRules')).render();
//			this.$("#customRules").html(new RuleGroupView(this.model.customRules).render().el);
			
			this.updateRequestUrl();
			return this;
		},
		
		back : function() {
			this.content(parentNav.parent());
		},
		
		//update badge
		updateBadge : function(sel, size) {
			if (sel) {
				var badge = this.$("button[data-target='" + sel + "']>.badge");
				badge = (size && size>0 && badge) ? badge.html(size) :	badge.empty();
				return;
			}
			
			this.updateBadge("#queryParams", this.model.queryParams.size());
			this.updateBadge("#requestHeaders", this.model.requestHeaders.size());
		},
		
		//split the URL into baseUrl and queryParams
		doParseUrl : function(baseUrl) {
			console.log("baseUrl:" + baseUrl);
			var urlParams = this.$("#urlParams").val();
			
			var idx = urlParams.indexOf("?");
			if (idx) {
				this.$("#baseUrl").html(urlParams.substring(0,idx));
				this.$("#urlParams").val(urlParams.substring(idx+1));
			}

			var v = _.map($.url(urlParams).param(), function(v,k){return {name:k, value:v}});
			this.queryParams.reset(v);
			this.queryParams.save();
		},
		
		//TODO: update URL line with parameters
		updateRequestUrl : function() {
			var baseUrl = this.model.find("properties", "baseUrl") || "";
			var servicePath = this.model.find("properties", "servicePath") || "";
			var params = this.queryParams.toQueryString();

			var url = servicePath 
					? baseUrl.replace(/[^\/]$/, '$&/') + servicePath.replace(/^[\/]/, '')
					: baseUrl;
			url +=  '?' + params;
			this.$("#urlParams").val(url);
		},
		

		//remove a panel
		onRemovePanel : function(e) {
			var panel = $(e.target).parents(".panel").first();
			this.model.collections[panel.attr("data-group-name")].remove();
		},
		
		//to add a new parameter as clicking the last line, could use :last 
		addNewParam : function(e) {
			var param = $(e.target).parents(".param").first();
			param.after(param.clone());
		},
		
		//if request body panel should be shown up
		onChangeMethod : function(e) {
			var a = ["POST", "PUT"];
			var m = this.$("#requestMethod").val();
			this.$("#requestBodyPanel").collapse(a[m] ? "show" : "hide");
		},
		
		//add a validator panel
		showRuleGroup : function(e) {
			var name = $(e.target).attr("data-validator");
			this.validators[name].show();
		},

		//TODO: look all the way up until a baseUrl is found
		getParam: function(name) {
			var val = null;
			var m = this;
			do {
				val = m.getParam(name);
			} while(!val && (m = m.parent()));
			return val;
		},
		
		//TODO: get URI string, which is before the question mark
		getUri : function(url) {
			var uri = url.attr('protocol') + "://" + url.attr('host');
			if (url.attr('port')) {
				uri = uri + ":" + url.attr('port');
			}
		
			uri = uri + url.attr('path')
			return uri;
		},
		
		//TODO: remove a parameter component (name + value)
		onClickRemoveParam : function(e) {
			var elParam = $(e.target).parents(".param");
			elParam.remove();
		},
		
		//invoked when a rule is changed
		onRuleModified : function(e) {
			$.asserts.notBlank(e);
			
			var rule = $(e.target).parents(".rule");
			var ruleId = rule.attr("data-rule-id");
			var groupName = rule.parents(".rule-group").attr("data-group-name");
			
			//rule model is created before element is added, so it mustn't be empty
			var o = this.model.rules[groupName].get(ruleId);
			o.dataPath = rule.find(".name").val();
			o.op = rule.find(".op").val();
			o.expr = rule.find(".value").val();
		},

		doExecute : function() {
			this.report = this.model.execute();
			this.listenTo(this.report, "update", this.onResponse);
		},
		
		doRemove : function() {
			if (confirm("This case has child cases, are you sure to delete it?")) {
				this.model.destroy();				
			}
		},
		
		onResponse : function(model) {
			console.log(model);
			new ResponseView(this.$("#responsePanel"), model.at(0)).render();
		}
	});
	
	return new View();
};