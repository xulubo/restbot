$(function(){
	
	var View = Backbone.View.extend({
		el: "#loobo",
		template : Handlebars.compile($("#TEST").html()),
		
		render : function() {
			alert("works");
			this.$el.html(this.template({title: "My New Post", body: "This is my first post!"}));
			
		}
	});
	
	new View().render();
});