#!/bin/sh

echo "Shutting down restbot ..."
PID=`ps ax | grep restbot | grep java | awk '{print $1}'`

if [ ! -z "$PID" ]; then
	kill -9 $PID
	if [ $? -eq 0 ]; then
		echo "process killed"
	fi
else
	echo "process not started"
fi
