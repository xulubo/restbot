#!/bin/sh

echo "Starting up restbot ..."
BIN_DIR=`dirname $0`
HOME_DIR=`dirname $BIN_DIR`

eval java -jar $BIN_DIR/restbot.war $HOME_DIR/work | tee $HOME_DIR/logs/restbot.out &