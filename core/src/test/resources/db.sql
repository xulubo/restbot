CREATE TABLE "SETTING" 
(  "KEY" VARCHAR2(56 CHAR) NOT NULL , 
   "VALUE" VARCHAR2(1024 CHAR) NOT NULL
);

INSERT INTO setting(key, value) VALUES('city', 'Toronto');
INSERT INTO setting(key, value) VALUES('mode', 'fahrenheit');
INSERT INTO setting(key, value) VALUES('input', 'weather');
INSERT INTO setting(key, value) VALUES('weather', '.* Fahrenheit');


