package com.quickplay.restbot.validators;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.junit.Test;

import com.quickplay.restbot.caze.filter.ResponseTemplateValidationFilter;

public class ResponseTemplateCheckerTest {

	@Test
	public void assertEqualsTest() throws FileNotFoundException, JSONException, IOException {
		File template = new File("src/test/resources/template_weather.json");
		File actual = new File("src/test/resources/response_weather.json");
		
		ResponseTemplateValidationFilter.assertEquals(
				IOUtils.toString(new FileInputStream(template)),
				IOUtils.toString(new FileInputStream(actual)));
	}
}
