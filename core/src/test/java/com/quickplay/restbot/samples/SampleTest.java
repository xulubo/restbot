package com.quickplay.restbot.samples;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.file.Files;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.core.io.UrlResource;

import com.quickplay.restbot.annotations.EmbeddedDataSource;
import com.quickplay.restbot.annotations.Host;
import com.quickplay.restbot.annotations.ResourceFiles;
import com.quickplay.restbot.runner.SuiteRunner;
import com.quickplay.restbot.test.H2DataSource;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

@SuppressWarnings({ "restriction" })
@RunWith(SuiteRunner.class)
@ResourceFiles(
	value="file:src/test/resources/samplecase.xlsx"
	, allows = { "case1" }
	)
@Host(host="localhost", port=8082)
@EmbeddedDataSource(dsClass = H2DataSource.class)
public class SampleTest {

	static PseudoLocalServer server = new PseudoLocalServer(8082);
	
	@BeforeClass
	public static void setupEnv() throws Exception {
		server.start();
	}
	
	@AfterClass
	public static void cleanEnv() {
		server.stop();
	}
}


@SuppressWarnings("restriction")
class PseudoLocalServer {

	HttpServer server;
	int port;
	
	PseudoLocalServer(int port) {
		this.port = port;
	}
	
    public void start() throws Exception {
		server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/api", new ApiHandler("file:src/test/resources/response_weather.json"));
        
        // creates a default executor
        server.setExecutor(null);
        server.start();
    }

    public void stop() {
    	server.stop(0);
    }
    
    class ApiHandler implements HttpHandler {
    	
    	private String resourceUrl;
    	ApiHandler(String url) {
    		this.resourceUrl = url;
    	}
    	
        @Override
        public void handle(HttpExchange t) throws IOException {
        	File resFile = new UrlResource(resourceUrl).getFile();
        	String response = new String(Files.readAllBytes(resFile.toPath()));
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

}


