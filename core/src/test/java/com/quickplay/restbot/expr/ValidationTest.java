package com.quickplay.restbot.expr;

import org.junit.Test;

import com.quickplay.restbot.exceptions.CaseResponseValidationException;
import com.quickplay.restbot.response.validators.ExpressionValidator;

public class ValidationTest {

	ExpressionValidator v = new ExpressionValidator(null);
	
	@Test
	public void parseMatch() {
		v.validate("72 Fahrenheit", "/.*Fahrenheit$/");
	}
	
	@Test(expected=CaseResponseValidationException.class)
	public void shouldFailParseMatch() {
		v.validate("72", "/.*Fahrenheit$/");
	}
	
	@Test
	public void testSingleCompareMix() {
		v.validate(5.0, "<${valueOf(6.0)}");
	}
	
	@Test(expected=CaseResponseValidationException.class)
	public void shouldFail() {
		v.validate(5.0, ">${valueOf(6.0)}");
	}
	
	
}
