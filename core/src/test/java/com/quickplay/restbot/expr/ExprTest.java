package com.quickplay.restbot.expr;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.stereotype.Component;

import com.quickplay.restbot.AppContext;
import com.quickplay.restbot.annotations.ExprHandler;
import com.quickplay.restbot.annotations.ExprMethod;
import com.quickplay.restbot.exceptions.ExprAssertionException;

@Component
@ExprHandler
public class ExprTest {
	
	String STR = "\\\"\\a!b\\\"c de-f,g. *\\\\";
	String SYMBOL = "_+-=~`!@#$%^&*(){}|[]\\\\:\";'<>?,./";

	ExprEvaluator exprEvaluator = ExprEvaluator.INSTANCE;

	@Before
	public void init() {
		AppContext.autowireBean(this);
	}
	
	@Test
	public void parseLong() {
		assertEquals("1234",  eval("${valueOf(1234)}"));
		assertEquals("-1234", eval("${valueOf(-1234)}"));
	}
	
	@Test
	public void parseDouble() {
		assertEquals("1234.23",  eval("${valueOf(1234.23)}"));
		assertEquals("-1234.23", eval("${valueOf(-1234.23)}"));
	}
	
	@Test
	public void parseString() {
//		assertEquals("abcd",  eval("${valueOf(\"abcd\")}"));		
//		assertEquals("true",  eval("${eq(\"ab\\\"cd\", \"ab\\\"cd\")}"));		
//		assertEquals("ab\"\"\\cd",  eval("${valueOf(\"ab\\\"\\\"\\\\a\")}"));		
		assertEquals("a$b\"\"\\cd",  eval("${valueOf(\"a$b\\\"\\\"\\\\cd\")}"));		

//		assertEquals(SYMBOL,  eval("${valueOf(\"" + SYMBOL + "\")}"));		
	}
	
	@Test
	public void parseExpansion() {
		//only literal in ${} will be expanded
		assertEquals("true",  eval("true"));		
		assertEquals("abc(1,32, bb(3,3))",  eval("abc(1,32, bb(3,3))"));		
		assertEquals("abc(3+2)",  eval("abc(3+2)"));		
		assertEquals("abc(1,3+2, bb(3,3))",  eval("abc(1,3+2, bb(3,3))"));		

		assertEquals("a:d:h",  eval("${a:d:h}"));		
		assertEquals("abc(a:d:h)",  eval("abc(${a:d:h})"));		
		assertEquals("abc(a:d:h, 1)",  eval("abc(${a:d:h}, 1)"));		
		assertEquals("abc(a:d:h, 1,3+2, bb(3,3))",  eval("abc(${a:d:h}, 1,3+2, bb(3,3))"));		

		assertEquals("true",  eval("${valueOf(true)}"));
		//escape ${ 
		assertEquals("${valueOf(true)}",  eval("\\${valueOf(true)}"));		
		assertEquals("aafalse234abcabc:33:bb",  eval("aa${valueOf(false)}234${abc}${abc:33:bb}"));		
	}
	
	@Test
	public void parseBoolean() {
		assertEquals("true",  eval("${isTrue(true)}"));
		
		try {
			assertEquals("false",  eval("${isTrue(false)}"));
			fail("should throw exception by isTrue function");
		} catch(ExprAssertionException e) {
			
		}
	}

	

	@Test
	public void parseFuncArgs() {
		assertEquals("true", eval("${eq(-1, -1)}"));	
		assertEquals("true", eval("${gt(2.3, -5.3)}"));	
	}
	
	
	@Test
	public void parseVar() {
		assertEquals("a1234", eval("${a1234}"));
	}

	@Test
	public void parseSimpleCaseVar() {
		assertEquals("param_01", eval("${param_01}"));
		assertEquals("c_01:param_01", eval("${c_01:param_01}"));
		assertEquals("1001:param_01", eval("${1001:param_01}"));
		assertEquals("suite01:c_01:param1", eval("${suite01:c_01:param1}"));
		
		//caseId can't be number only
		assertEquals("suite01:01:param1", eval("${suite01:01:param1}"));
	}

	@Test
	public void parseComplexCaseVar() {
		//var id can contains $ . /
		assertEquals("suite01:01:$.countries/$.city", eval("${suite01:01:$.countries/$.city}"));
	}
	
	@Test
	public void parseStringExpansion() {
//		assertEquals("abc", exprEvaluator.evaluate("abc"));
		assertEquals("abcdtrue", exprEvaluator.evaluate("abc${d}${eq(1,1)}"));
//		assertEquals("abcparam_01012", exprEvaluator.evaluate("?abc${param_01}012"));
	}

	
	@Test
	public void testEmbeddedFunc() throws Exception {
		assertEquals("true", eval("${eq(-1, -1)}"));
		assertEquals("true", eval("${ne(1, 2)}"));
		assertEquals("false", eval("${ne(1, 1)}"));
	}
	
	@Test
	public void testNestedExpr() {
		assertEquals("true", eval("${eq(true, eq(1,1))}"));
		assertEquals("true", eval("${eq(\"abcd\",\"abcd\")}"));
		assertEquals("true", eval("${eq(\"abcd\",${abcd})}"));
	}
	


	/**
	 * framework user can defined their own script function
	 */
	@Test 
	public void testCustomizedFunction() {
		//strcat is defined in the current class
		assertEquals("abc"+3+4, eval("${strcat(\"abc\", 3,4)}"));		
	}


	@Test
	public void testSingleCompare() {
		eval(-2, "<100");
		eval(2, "<100");
		eval(2, ">1");
		eval(2L, "<100");
		eval(2L, ">1");
		eval(2.0, ">1");
		eval(2.0, "<3");
		eval(2.0, ">1.1");
		eval(2.0, "<3.1");
	}

	


	private Object eval(String expr) {
		return exprEvaluator.evaluate(expr);
	}
	
	private Object eval(Object firstArg, String expr) {
		return exprEvaluator.validate(firstArg, expr, null);
	}
	
	@ExprMethod
	public String strcat(String a, Object b, Object c) {
		return a + b + c;
	}
}
