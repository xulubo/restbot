package com.quickplay.restbot.utils;

import static org.junit.Assert.*;

import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.Maps;

public class ParamUtilsTest {

	@Test
	public void test() {
		Set<String> set;

		set = ParamUtils.extractVariableNames("${name1}");
		assertEquals("name1", set.iterator().next());

		set = ParamUtils.extractVariableNames("abc${name1}ad");
		assertEquals("name1", set.iterator().next());

		set = ParamUtils.extractVariableNames("abc\\${name1}ad");
		assertEquals(0, set.size());

		set = ParamUtils.extractVariableNames("abc\\\\${name1}ad");
		assertEquals("name1", set.iterator().next());

		set = ParamUtils.extractVariableNames("abc\\\\\\${name1}ad");
		assertEquals(0, set.size());
		
	}

	@Test
	public void test1() {
		String result;
		Map<String, String> map = Maps.newHashMap();
		map.put("name1", "VAL1");
		map.put("name2", "VAL2");
		
		result = ParamUtils.extend("${name1}", map);
		assertEquals("VAL1", result);

		result = ParamUtils.extend("abc${name1}ad", map);
		assertEquals("abcVAL1ad", result);

		result = ParamUtils.extend("abc\\${name1}ad", map);
		assertEquals("abc\\${name1}ad", result);

		result = ParamUtils.extend("abc\\${name1}ad${name2}d", map);
		assertEquals("abc\\${name1}adVAL2d", result);

		
		result = ParamUtils.extend("abc\\\\${name1}ad", map);
		assertEquals("abc\\\\VAL1ad", result);

		result = ParamUtils.extend("abc\\\\\\${name1}ad", map);
		assertEquals("abc\\\\\\${name1}ad", result);
		
	}

}
