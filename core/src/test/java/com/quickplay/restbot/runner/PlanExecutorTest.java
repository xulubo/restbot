package com.quickplay.restbot.runner;

import java.io.File;

import org.junit.Test;

import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.persist.reader.excel.ExcelPlanReader;

public class PlanExecutorTest {

	@Test
	public void test() {
		ExcelPlanReader reader = new ExcelPlanReader();
		Plan plan = reader.read(new File("src/test/resources/samplecase.xlsx"));
		plan.setId("sample");
		Case c = plan.find(":test_pannous:case1");
		
		System.err.println(c.getPath());
		PlanExecutor pe = new PlanExecutor(plan);
		
		pe.execute(c);
	}
}
