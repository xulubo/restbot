package com.quickplay.restbot.persist.reader.xml;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.persist.reader.excel.ExcelPlanReader;

public class PlanWriterTest {

	@Test
	public void test() throws IOException {
		
		new File("tmp").mkdir();
		ExcelPlanReader excelReader = new ExcelPlanReader();
		Plan plan = excelReader.read(new File("src/test/resources/samplecase.xlsx"));
		
		new XmlPlanWriter().write(plan, new File("tmp/samplecase.xml"));
	}
}
