package com.quickplay.restbot.caze.core;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CaseSplitterTest {
	private static final Logger logger = LoggerFactory.getLogger(CaseSplitterTest.class);
	
	@Test
	public void getCombinationListTest() {
		logger.debug("Enter getCombinationListTest");
		ExpressionSet map = new ExpressionSet();
		map.add("index", "[1-3]");
		map.add("name", "[Alex,Bob,Collins]");
		map.add("location", "[Beijing, Toronto, New York]");
		map.add("task", "play");
		
		List<ExpressionSet> lst = CaseSplitter.getCombinationList(map.iterator());

		assertNotNull(lst);
		assertEquals(27, lst.size());
		
		for(ExpressionSet m : lst) {
			StringBuilder sb = new StringBuilder();
			for(Expression e : m) {
				if (sb.length()>0) {
					sb.append("&");
				}
				sb.append(e.getName()).append("=").append(e.getValue());
			}
			System.out.println(sb.toString());
		}
	}
	

}
