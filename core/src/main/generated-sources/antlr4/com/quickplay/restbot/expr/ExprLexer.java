// Generated from Expr.g4 by ANTLR 4.3
package com.quickplay.restbot.expr;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ExprLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__17=1, T__16=2, T__15=3, T__14=4, T__13=5, T__12=6, T__11=7, T__10=8, 
		T__9=9, T__8=10, T__7=11, T__6=12, T__5=13, T__4=14, T__3=15, T__2=16, 
		T__1=17, T__0=18, EscDollarCurlyBracket=19, FloatingPointLiteral=20, IntegerLiteral=21, 
		BooleanLiteral=22, Identifier=23, CaseVarPathLiteral=24, RegexLiteral=25, 
		RegexEscapeSequence=26, StringLiteral=27, WS=28;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'", "'\\u000F'", "'\\u0010'", "'\\u0011'", "'\\u0012'", 
		"'\\u0013'", "'\\u0014'", "'\\u0015'", "'\\u0016'", "'\\u0017'", "'\\u0018'", 
		"'\\u0019'", "'\\u001A'", "'\\u001B'", "'\\u001C'"
	};
	public static final String[] ruleNames = {
		"T__17", "T__16", "T__15", "T__14", "T__13", "T__12", "T__11", "T__10", 
		"T__9", "T__8", "T__7", "T__6", "T__5", "T__4", "T__3", "T__2", "T__1", 
		"T__0", "EscDollarCurlyBracket", "FloatingPointLiteral", "IntegerLiteral", 
		"BooleanLiteral", "Identifier", "Letter", "LetterOrDigit", "Sign", "Digits", 
		"CaseVarPathLiteral", "CaseVarPathCharacters", "CaseVarPathCharacter", 
		"RegexLiteral", "RegexCharacters", "RegexCharacter", "RegexEscapeSequence", 
		"StringLiteral", "StringCharacters", "StringCharacter", "StringEscapeSequence", 
		"WS"
	};


	public ExprLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Expr.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\36\u00d8\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\3\2\3\2\3\3\3\3"+
		"\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\13\3\13"+
		"\3\13\3\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\20\3\21"+
		"\3\21\3\22\3\22\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25"+
		"\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\5\27\u008f\n\27"+
		"\3\30\3\30\7\30\u0093\n\30\f\30\16\30\u0096\13\30\3\31\3\31\3\32\3\32"+
		"\3\33\3\33\3\34\6\34\u009f\n\34\r\34\16\34\u00a0\3\35\3\35\3\35\3\35\3"+
		"\35\3\35\3\36\6\36\u00aa\n\36\r\36\16\36\u00ab\3\37\3\37\3 \3 \3 \3 \3"+
		"!\6!\u00b5\n!\r!\16!\u00b6\3\"\3\"\5\"\u00bb\n\"\3#\3#\3#\3$\3$\5$\u00c2"+
		"\n$\3$\3$\3%\6%\u00c7\n%\r%\16%\u00c8\3&\3&\5&\u00cd\n&\3\'\3\'\3\'\3"+
		"(\6(\u00d3\n(\r(\16(\u00d4\3(\3(\2\2)\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21"+
		"\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30"+
		"/\31\61\2\63\2\65\2\67\29\32;\2=\2?\33A\2C\2E\34G\35I\2K\2M\2O\36\3\2"+
		"\13\5\2C\\aac|\6\2\62;C\\aac|\4\2--//\3\2\62;\7\2*+>>@@}}\177\177\3\2"+
		"\61\61\4\2$$^^\5\2$$^^vv\5\2\13\f\16\17\"\"\u00d6\2\3\3\2\2\2\2\5\3\2"+
		"\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21"+
		"\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2"+
		"\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3"+
		"\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\29\3\2\2\2\2?\3\2\2"+
		"\2\2E\3\2\2\2\2G\3\2\2\2\2O\3\2\2\2\3Q\3\2\2\2\5S\3\2\2\2\7U\3\2\2\2\t"+
		"W\3\2\2\2\13Y\3\2\2\2\r[\3\2\2\2\17]\3\2\2\2\21_\3\2\2\2\23b\3\2\2\2\25"+
		"d\3\2\2\2\27g\3\2\2\2\31j\3\2\2\2\33m\3\2\2\2\35o\3\2\2\2\37q\3\2\2\2"+
		"!t\3\2\2\2#v\3\2\2\2%x\3\2\2\2\'{\3\2\2\2)\177\3\2\2\2+\u0083\3\2\2\2"+
		"-\u008e\3\2\2\2/\u0090\3\2\2\2\61\u0097\3\2\2\2\63\u0099\3\2\2\2\65\u009b"+
		"\3\2\2\2\67\u009e\3\2\2\29\u00a2\3\2\2\2;\u00a9\3\2\2\2=\u00ad\3\2\2\2"+
		"?\u00af\3\2\2\2A\u00b4\3\2\2\2C\u00ba\3\2\2\2E\u00bc\3\2\2\2G\u00bf\3"+
		"\2\2\2I\u00c6\3\2\2\2K\u00cc\3\2\2\2M\u00ce\3\2\2\2O\u00d2\3\2\2\2QR\7"+
		"+\2\2R\4\3\2\2\2ST\7\60\2\2T\6\3\2\2\2UV\7.\2\2V\b\3\2\2\2WX\7-\2\2X\n"+
		"\3\2\2\2YZ\7,\2\2Z\f\3\2\2\2[\\\7/\2\2\\\16\3\2\2\2]^\7*\2\2^\20\3\2\2"+
		"\2_`\7&\2\2`a\7}\2\2a\22\3\2\2\2bc\7>\2\2c\24\3\2\2\2de\7?\2\2ef\7\u0080"+
		"\2\2f\26\3\2\2\2gh\7#\2\2hi\7?\2\2i\30\3\2\2\2jk\7>\2\2kl\7?\2\2l\32\3"+
		"\2\2\2mn\7\"\2\2n\34\3\2\2\2op\7@\2\2p\36\3\2\2\2qr\7?\2\2rs\7?\2\2s "+
		"\3\2\2\2tu\7\61\2\2u\"\3\2\2\2vw\7\177\2\2w$\3\2\2\2xy\7@\2\2yz\7?\2\2"+
		"z&\3\2\2\2{|\7^\2\2|}\7&\2\2}~\7}\2\2~(\3\2\2\2\177\u0080\5\67\34\2\u0080"+
		"\u0081\7\60\2\2\u0081\u0082\5\67\34\2\u0082*\3\2\2\2\u0083\u0084\5\67"+
		"\34\2\u0084,\3\2\2\2\u0085\u0086\7v\2\2\u0086\u0087\7t\2\2\u0087\u0088"+
		"\7w\2\2\u0088\u008f\7g\2\2\u0089\u008a\7h\2\2\u008a\u008b\7c\2\2\u008b"+
		"\u008c\7n\2\2\u008c\u008d\7u\2\2\u008d\u008f\7g\2\2\u008e\u0085\3\2\2"+
		"\2\u008e\u0089\3\2\2\2\u008f.\3\2\2\2\u0090\u0094\5\61\31\2\u0091\u0093"+
		"\5\63\32\2\u0092\u0091\3\2\2\2\u0093\u0096\3\2\2\2\u0094\u0092\3\2\2\2"+
		"\u0094\u0095\3\2\2\2\u0095\60\3\2\2\2\u0096\u0094\3\2\2\2\u0097\u0098"+
		"\t\2\2\2\u0098\62\3\2\2\2\u0099\u009a\t\3\2\2\u009a\64\3\2\2\2\u009b\u009c"+
		"\t\4\2\2\u009c\66\3\2\2\2\u009d\u009f\t\5\2\2\u009e\u009d\3\2\2\2\u009f"+
		"\u00a0\3\2\2\2\u00a0\u009e\3\2\2\2\u00a0\u00a1\3\2\2\2\u00a18\3\2\2\2"+
		"\u00a2\u00a3\7&\2\2\u00a3\u00a4\7}\2\2\u00a4\u00a5\3\2\2\2\u00a5\u00a6"+
		"\5;\36\2\u00a6\u00a7\7\177\2\2\u00a7:\3\2\2\2\u00a8\u00aa\5=\37\2\u00a9"+
		"\u00a8\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00a9\3\2\2\2\u00ab\u00ac\3\2"+
		"\2\2\u00ac<\3\2\2\2\u00ad\u00ae\n\6\2\2\u00ae>\3\2\2\2\u00af\u00b0\7\61"+
		"\2\2\u00b0\u00b1\5A!\2\u00b1\u00b2\7\61\2\2\u00b2@\3\2\2\2\u00b3\u00b5"+
		"\5C\"\2\u00b4\u00b3\3\2\2\2\u00b5\u00b6\3\2\2\2\u00b6\u00b4\3\2\2\2\u00b6"+
		"\u00b7\3\2\2\2\u00b7B\3\2\2\2\u00b8\u00bb\n\7\2\2\u00b9\u00bb\5E#\2\u00ba"+
		"\u00b8\3\2\2\2\u00ba\u00b9\3\2\2\2\u00bbD\3\2\2\2\u00bc\u00bd\7^\2\2\u00bd"+
		"\u00be\7\61\2\2\u00beF\3\2\2\2\u00bf\u00c1\7$\2\2\u00c0\u00c2\5I%\2\u00c1"+
		"\u00c0\3\2\2\2\u00c1\u00c2\3\2\2\2\u00c2\u00c3\3\2\2\2\u00c3\u00c4\7$"+
		"\2\2\u00c4H\3\2\2\2\u00c5\u00c7\5K&\2\u00c6\u00c5\3\2\2\2\u00c7\u00c8"+
		"\3\2\2\2\u00c8\u00c6\3\2\2\2\u00c8\u00c9\3\2\2\2\u00c9J\3\2\2\2\u00ca"+
		"\u00cd\n\b\2\2\u00cb\u00cd\5M\'\2\u00cc\u00ca\3\2\2\2\u00cc\u00cb\3\2"+
		"\2\2\u00cdL\3\2\2\2\u00ce\u00cf\7^\2\2\u00cf\u00d0\t\t\2\2\u00d0N\3\2"+
		"\2\2\u00d1\u00d3\t\n\2\2\u00d2\u00d1\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4"+
		"\u00d2\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6\u00d7\b("+
		"\2\2\u00d7P\3\2\2\2\r\2\u008e\u0094\u00a0\u00ab\u00b6\u00ba\u00c1\u00c8"+
		"\u00cc\u00d4\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}