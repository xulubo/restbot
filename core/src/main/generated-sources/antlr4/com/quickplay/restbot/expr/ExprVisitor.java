// Generated from Expr.g4 by ANTLR 4.3
package com.quickplay.restbot.expr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ExprParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ExprVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code opMatchExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpMatchExpr(@NotNull ExprParser.OpMatchExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#expressionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionList(@NotNull ExprParser.ExpressionListContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(@NotNull ExprParser.ExprContext ctx);

	/**
	 * Visit a parse tree produced by the {@code funcExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncExpr(@NotNull ExprParser.FuncExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#functionInvocationExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionInvocationExpression(@NotNull ExprParser.FunctionInvocationExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#embedibleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmbedibleExpression(@NotNull ExprParser.EmbedibleExpressionContext ctx);

	/**
	 * Visit a parse tree produced by the {@code opCompareExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpCompareExpr(@NotNull ExprParser.OpCompareExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#parameterAssigmentExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameterAssigmentExpression(@NotNull ExprParser.ParameterAssigmentExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#functionVarExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionVarExpression(@NotNull ExprParser.FunctionVarExpressionContext ctx);

	/**
	 * Visit a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpExpr(@NotNull ExprParser.OpExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary(@NotNull ExprParser.PrimaryContext ctx);

	/**
	 * Visit a parse tree produced by the {@code primaryExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryExpr(@NotNull ExprParser.PrimaryExprContext ctx);

	/**
	 * Visit a parse tree produced by the {@code signExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSignExpr(@NotNull ExprParser.SignExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#nonExpansionChars}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNonExpansionChars(@NotNull ExprParser.NonExpansionCharsContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(@NotNull ExprParser.LiteralContext ctx);
}