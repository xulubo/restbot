// Generated from Expr.g4 by ANTLR 4.3
package com.quickplay.restbot.expr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ExprParser}.
 */
public interface ExprListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code opMatchExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterOpMatchExpr(@NotNull ExprParser.OpMatchExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code opMatchExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitOpMatchExpr(@NotNull ExprParser.OpMatchExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExprParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void enterExpressionList(@NotNull ExprParser.ExpressionListContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void exitExpressionList(@NotNull ExprParser.ExpressionListContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExprParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(@NotNull ExprParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(@NotNull ExprParser.ExprContext ctx);

	/**
	 * Enter a parse tree produced by the {@code funcExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFuncExpr(@NotNull ExprParser.FuncExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funcExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFuncExpr(@NotNull ExprParser.FuncExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExprParser#functionInvocationExpression}.
	 * @param ctx the parse tree
	 */
	void enterFunctionInvocationExpression(@NotNull ExprParser.FunctionInvocationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#functionInvocationExpression}.
	 * @param ctx the parse tree
	 */
	void exitFunctionInvocationExpression(@NotNull ExprParser.FunctionInvocationExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExprParser#embedibleExpression}.
	 * @param ctx the parse tree
	 */
	void enterEmbedibleExpression(@NotNull ExprParser.EmbedibleExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#embedibleExpression}.
	 * @param ctx the parse tree
	 */
	void exitEmbedibleExpression(@NotNull ExprParser.EmbedibleExpressionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code opCompareExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterOpCompareExpr(@NotNull ExprParser.OpCompareExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code opCompareExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitOpCompareExpr(@NotNull ExprParser.OpCompareExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExprParser#parameterAssigmentExpression}.
	 * @param ctx the parse tree
	 */
	void enterParameterAssigmentExpression(@NotNull ExprParser.ParameterAssigmentExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#parameterAssigmentExpression}.
	 * @param ctx the parse tree
	 */
	void exitParameterAssigmentExpression(@NotNull ExprParser.ParameterAssigmentExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExprParser#functionVarExpression}.
	 * @param ctx the parse tree
	 */
	void enterFunctionVarExpression(@NotNull ExprParser.FunctionVarExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#functionVarExpression}.
	 * @param ctx the parse tree
	 */
	void exitFunctionVarExpression(@NotNull ExprParser.FunctionVarExpressionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterOpExpr(@NotNull ExprParser.OpExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitOpExpr(@NotNull ExprParser.OpExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExprParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterPrimary(@NotNull ExprParser.PrimaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitPrimary(@NotNull ExprParser.PrimaryContext ctx);

	/**
	 * Enter a parse tree produced by the {@code primaryExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPrimaryExpr(@NotNull ExprParser.PrimaryExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code primaryExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPrimaryExpr(@NotNull ExprParser.PrimaryExprContext ctx);

	/**
	 * Enter a parse tree produced by the {@code signExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSignExpr(@NotNull ExprParser.SignExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code signExpr}
	 * labeled alternative in {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSignExpr(@NotNull ExprParser.SignExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExprParser#nonExpansionChars}.
	 * @param ctx the parse tree
	 */
	void enterNonExpansionChars(@NotNull ExprParser.NonExpansionCharsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#nonExpansionChars}.
	 * @param ctx the parse tree
	 */
	void exitNonExpansionChars(@NotNull ExprParser.NonExpansionCharsContext ctx);

	/**
	 * Enter a parse tree produced by {@link ExprParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(@NotNull ExprParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(@NotNull ExprParser.LiteralContext ctx);
}