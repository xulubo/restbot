Case Variable: will be evaluated in runtime
	Grammar:  ${[suiteName:][caseId:]paramName}
		parameter discover sequence:
			case.queryParams
			if not found then case.pathVariables
			if not found then case.expectations
			if not found then case.metas
			if not found then suite.params
			if not found then plan.params
			if not found then runner.params
			
	Example:  ${paramName}
	
FunctionCall: expression is evaluated as a embedded function call
	Grammar: leading with a equal sign and in the format of a Java function
	Example: ${=someFunction("abc", 234)}
	
ExtendibleString: default expression type, embedded variable will be replaced in runtime, any expression not matching the 
                  above types will be thought of this type. 
	Grammar:  [[string] ${variable}]{n}
	

Response Expectation Grammar
	Value comparison operator
		==, eq(v)			: equals
		<=, le(v)			: less than or equal
		<,  lt(v)			: less than
		>=, ge(v)			: great than or equal
		>,  gt(v)			: great than
		(v1,v2),			: greater than the first and less than the second
		[v1,v2],			: greater than or equal to the first and less than or equal to second
		(v1,v2],			: greater than the first value and less than or equal the second
		[v1,v2],			: greater than or equal to the first value and less than the second
		<>, ne				: not equal	
		/../, match			:	regex match, in the format of =~ "pattern", /pattern/ or match("pattern"), 
		                        if string not quoted, then the whole string after =~ will be used as pattern with prefix/tailing spaces trimmed
		in(v1,v2...vn) 		: in provided list
		has(v1,v2...vn])	: contains all listed values
		
		'...'				: constant string, string will be used as is
		"..."				: extendible string
		
		if expression doesn't match either of above pattern, == will be used as default operator, that is, the whole string
		will be regarded as a extendible string with leading and tailing spaces trimmed 
		
	example:
		== 2
		== "abcd"
		== ${caseId}
		== ${db.query()} 			// in this case, db.query must return only on result
		in ${db.query()} 			// db.query can return any amount of result
		contains(["a","b","c"])		// contains all listed value
		contains("dd")		 		// contains dd
		contains("${db.query()}")	// contains all results returned by the query
		
		