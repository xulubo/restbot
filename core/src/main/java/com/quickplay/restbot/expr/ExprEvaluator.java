package com.quickplay.restbot.expr;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.exceptions.ExprMethodNotFoundException;
import com.quickplay.restbot.exceptions.ExprParseException;
import com.quickplay.restbot.exceptions.ExprUnresolvableException;

public enum ExprEvaluator {
	INSTANCE;
	
	static final Logger logger = LoggerFactory.getLogger(ExprEvaluator.class);
	

	public Object evaluate(String expr) {
		return evaluate(expr, new ExprParseVisitor());
	}
	
	public Object validate(Object obj, String methodExpression, ExprParseListener exprParseListener) {
		ExprParseVisitor visitor = new ExprParseVisitor(obj, exprParseListener);
		return evaluate(methodExpression, visitor);
	}
	
	public Object evaluate(String expr, final Case c)  {
		ExprParseVisitor visitor = new ExprParseVisitor(new ExprParseListener(){

			@Override
			public Object getCaseVarVal(String varPath) {
				return c.readValue(varPath);
			}
			
		});
		return evaluate(expr, visitor);
	}
	
	public Object evaluate(String expr, ExprParseListener exprParseListener)  {
		ExprParseVisitor visitor = new ExprParseVisitor(exprParseListener);
		return evaluate(expr, visitor);
	}
	
	private Object evaluate(final String expr, ExprParseVisitor visitor)  {
		logger.debug("<<< --- Parsing {}", expr);

		ANTLRInputStream input = new ANTLRInputStream(expr);
		ExprLexer lexer = new ExprLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		ExprParser parser = new ExprParser(tokens);
		parser.setBuildParseTree(true);
		ParseTree tree = parser.expr();
		// show tree in text form
		logger.debug("--- Parse Tree {}", tree.toStringTree(parser));
		logger.debug("tree instance = {}", tree.getClass().getSimpleName());
		//ParseTreeWalker walker = new ParseTreeWalker();
		
		try {
			//walker.walk(exprParseListener, tree);
			return tree.accept(visitor);
		} catch(ExprMethodNotFoundException | ExprParseException e) {
			//e.printStackTrace();
			throw new ExprUnresolvableException("no applicable method found for " + expr, e);
		} finally {
			logger.debug(">>>");			
		}
	}

}
