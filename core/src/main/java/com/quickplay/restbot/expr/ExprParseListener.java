package com.quickplay.restbot.expr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class ExprParseListener extends ExprBaseListener {
	
	static final Logger logger = LoggerFactory.getLogger(ExprParseListener.class);
	
	abstract public Object getCaseVarVal(String varName);

}
