package com.quickplay.restbot.expr;

import java.util.LinkedList;
import java.util.List;

public class MethodExpr {

	private final String instanceName;
	private final String methodName;
	private final List<Object> parameters = new LinkedList<Object>();

	public MethodExpr(String instanceName, String methodName) {
		this.instanceName = instanceName;
		this.methodName = methodName;
	}
	
	public MethodExpr(String instanceName, String methodName, List<Object> args) {
		this(instanceName, methodName);
		if (args != null) {
			parameters.addAll(args);
		}
	}
	
	public MethodExpr(String instanceName, String methodName, Object[] args) {
		this(instanceName, methodName);
		if (args != null) {
			for(Object arg: args) {
				parameters.add(arg);
			}
		}
	}
	
	public String getInstanceName() {
		return this.instanceName;
	}
	
	public String getMethodName() {
		return methodName;
	}

	public void addParameter(Object param) {
		this.parameters.add(param);
	}

	public List<Object> getParameters() {
		return parameters;
	}

}
