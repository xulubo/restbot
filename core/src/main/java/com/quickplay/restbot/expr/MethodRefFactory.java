package com.quickplay.restbot.expr;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.quickplay.restbot.annotations.ExprHandler;
import com.quickplay.restbot.annotations.ExprMethod;
import com.quickplay.restbot.exceptions.ExprAssertionException;
import com.quickplay.restbot.exceptions.ExprEvaluationException;
import com.quickplay.restbot.exceptions.ExprMethodNotFoundException;
import com.quickplay.restbot.utils.ClassLoaderUtils;

public class MethodRefFactory {
	private static final Logger logger  = LoggerFactory.getLogger(MethodRefFactory.class);
	private static Map<String, String> typeMap = Maps.newHashMap();
	private static MethodRefFactory instance = null;

	private Map<String, Set<MethodRef>> methodMap = new HashMap<String, Set<MethodRef>>();
	static {
		typeMap.put("boolean", 				"Z");
		typeMap.put("java.lang.Boolean", 	"Z");
		typeMap.put("byte", 				"B");
		typeMap.put("java.lang.Byte", 		"B");
		typeMap.put("char", 				"C");
		typeMap.put("java.lang.Char", 		"C");
		typeMap.put("long", 				"J");
		typeMap.put("java.lang.Long", 		"J");
		typeMap.put("int", 					"I");
		typeMap.put("java.lang.Integer", 	"I");
		typeMap.put("short", 				"S");
		typeMap.put("java.lang.Short", 		"S");
		typeMap.put("float", 				"F");
		typeMap.put("java.lang.Float", 		"F");
		typeMap.put("double", 				"D");
		typeMap.put("java.lang.Double", 	"D");
		typeMap.put("java.lang.Object", 	"L");
		typeMap.put("array", 				"[");
	}
	
	public static MethodRefFactory instance() {
		if (instance == null) {
			instance = new MethodRefFactory();
		}
		
		return instance;
	}
	
	public MethodRefFactory() {
		try {
			init();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void init() throws Exception {
		logger.debug(">>>BEGIN looking up internal in-test methods in system class path, basePackage=com.quickplay");
		lookupMethods(null, "com.quickplay");
		logger.debug("<<<END of looking up");
	}
	
	public void lookupMethodInPath(String libPath, String basePackage) throws Exception {
		logger.debug(">>>BEGIN looking up customized in-test methods in path {}, basePage={}", libPath, basePackage);
		//ClassLoaderUtils.addLibDirToClassPath(libPath);
		lookupMethods(ClassLoaderUtils.newResourceLoader(libPath), basePackage);
		logger.debug("<<<END of looking up");
	}
	
	private void lookupMethods(ResourceLoader resourceLoader, String basePackage) throws Exception {
		ClassPathScanningCandidateComponentProvider scanner =
				new ClassPathScanningCandidateComponentProvider(false);

		if (resourceLoader != null) {
			scanner.setResourceLoader(resourceLoader);
		}
		scanner.addIncludeFilter(new AnnotationTypeFilter(ExprHandler.class));

		for (BeanDefinition bd : scanner.findCandidateComponents(basePackage)) {
			logger.trace("find class {}", bd.getBeanClassName());
			Object o = (resourceLoader == null) ? Class.forName(bd.getBeanClassName()).newInstance() : 
				resourceLoader.getClassLoader().loadClass(bd.getBeanClassName()).newInstance();
			lookupMethodInBean(o);
		}
	}

	public Object invoke(MethodExpr method) {
		Object retVal = null;
		Object[] args = new Object[method.getParameters().size()];
		method.getParameters().toArray(args);
		MethodRef methodRef = getMethodRef(method.getInstanceName(), method.getMethodName(), args);
		if (methodRef == null) {
			throw new ExprEvaluationException("Method " + method.getMethodName() + " is not defined");
		}

		try {
			retVal = methodRef.invoke(args);
			logger.debug("result: " + retVal);
		} catch (InvocationTargetException e) {
			if (e.getCause() instanceof ExprAssertionException) {
				throw (ExprAssertionException)e.getCause();
			}
			throw new ExprEvaluationException(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ExprEvaluationException(e);
		} 

		return retVal;
	}
	
	private void lookupMethodInBean(Object o) {
		logger.trace("looking up methods in class {}", o.getClass().getName());
		ExprHandler h = o.getClass().getAnnotation(ExprHandler.class);
		if (h == null) {
			logger.trace("bean ClassLoader={}", o.getClass().getClassLoader().toString());
			logger.trace("annotation ClassLoader={}", ExprHandler.class.getClassLoader().toString());
			logger.trace("system ClassLoader={}", ClassLoader.getSystemClassLoader().toString());

			Annotation[] annotations = o.getClass().getAnnotations();
			for(Annotation a : annotations) {
				logger.debug("found annotation {}", a.toString());
			}
			return;
		} else {
			logger.trace("found annotation");
		}
		
		String instanceName = h.value();
		Method[] methods = o.getClass().getMethods();
		for(Method m : methods) {
			if (m.isAnnotationPresent(ExprMethod.class)) {
				MethodRef ref = new MethodRef(o, m);

				if (StringUtils.isEmpty(instanceName)) {
				logger.trace("found [{}#{}] in {}", 
						m.getName(), 
						m.getParameterTypes().length, m.getDeclaringClass().getSimpleName());
				} else {
					logger.trace("found [{}.{}#{}] in {}", 
							instanceName, 
							m.getName(), 
							m.getParameterTypes().length, m.getDeclaringClass().getSimpleName());					
				}
				registerMethod(instanceName, m.getName(), m.getParameterTypes(), ref);

				ExprMethod annotation = m.getAnnotation(ExprMethod.class);
				if (!StringUtils.isBlank(annotation.alias())) {
					logger.trace("add alias [{}.{}#{}]", instanceName, annotation.alias(), m.getParameterTypes().length);
					registerMethod(instanceName, annotation.alias(), m.getParameterTypes(), ref);
				}
			}
		}		
	}
	private void registerMethod(String instanceName, String methodName, Class<?>[] parameterTypes, MethodRef ref) {
		String key = methodKey(instanceName, methodName, parameterTypes.length);
		Set<MethodRef> refs = methodMap.get(key);
		if (refs == null) {
			refs = new HashSet<MethodRef>();
			methodMap.put(key, refs);
		}
		else if (refs.contains(ref) ) {
			throw new RuntimeException("method " + ref.getSignature() + " has duplicated definiation");
		}
		
		refs.add(ref);		
	}
	
	private static String methodKey (String instanceName, String methodName, int numOfArgs) {
		String prefix = StringUtils.isBlank(instanceName) ? "" : (instanceName + ".");
		return prefix + methodName + ":" + numOfArgs;

	}

	private static String parametersSignature(Object[] args) {
		List<Class<?>> types = Lists.newLinkedList();
		for(Object arg : args) {
			types.add(arg.getClass());
		}
		return parametersSignature(types);
	}
	
	private static String parametersSignature(Collection<Class<?>> types) {
		StringBuilder signature = new StringBuilder();
		for(Class<?> v : types) {
			String type = typeMap.get(v.getName());
			if (type != null) {
				signature.append(type);
			}
			else if (v.getName().startsWith("[")) {
				signature.append(v.getName());
			}
			else {
				signature.append("L").append(v.getName()).append(";");
			}
		}
		return signature.toString();
	}
	
	public static class MethodRef {
		Method method;
		Object instance;
		
		MethodRef(Object instance, Method method) {
			this.instance = instance;
			this.method = method;
		}

		public Class<?>[] getParameterTypes() {
			return method.getParameterTypes();
		}

		public boolean isInvokable(Object[] args) {
			boolean applicable = true;
			Class<?>[] parameterTypes = getParameterTypes();
			
			for(int i=0; i<parameterTypes.length && applicable; i++) {
				Class<?> t = parameterTypes[i];
				if (t.isAssignableFrom(args[i].getClass())) {
					continue;
				}
				else if (i==parameterTypes.length-1 && t.isArray()) {
					Class<?> ct = t.getComponentType();
					for(int j=i; j<args.length && applicable; j++) {
						applicable = ct.isAssignableFrom(args[i].getClass()); 
					}
				}
				else {
					logger.debug("{} can't be assigned to parameter type {}", args[i].getClass(), parameterTypes[i]);
					applicable = false;
					break;
				} 
			}
			
			return applicable;
		}
		
		public Object invoke(Object[] args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
			Class<?>[] types = method.getParameterTypes();
			Object[] tmpArgs = new Object[types.length];
			for(int i=0; i<types.length; i++) {
				Class<?> t = types[i];
				if (t.isAssignableFrom(args[i].getClass())) {
					tmpArgs[i] = args[i];
				}
				else if (i==types.length-1 && t.isArray()) {
					Class<?> ct = t.getComponentType();
					Object[] array = new Object[args.length - i];
					for(int j=i; j<args.length; j++) {
						if (ct.isAssignableFrom(args[i].getClass())) {
							array[j-i] = args[i];
						}
						else {
							throw new IllegalArgumentException("not allowed to assign arg from " + args[i].getClass() + " to " + ct.getClass());
						}
						tmpArgs[i] = array;
					}
				}
			}
			return method.invoke(instance, tmpArgs);
		}
		
		private String getSignature() {
			String signature = parametersSignature(Arrays.asList(method.getParameterTypes()));
			return signature;
		}

		@Override
		public int hashCode() {
			return getSignature().hashCode();
		}
	}

	public MethodRef getMethodRef(String instanceName, String methodName, Object[] args) {
		String methodKey = methodKey(instanceName, methodName, args.length);
		logger.debug("looking for method {}", methodKey);
		
		Set<MethodRef> refs = methodMap.get(methodKey);
		if (refs == null) {
			logger.debug("method {} is not registered", methodKey);
			throw new ExprMethodNotFoundException("method '" + methodKey + "' couldn't be found");
		}
		
		for(MethodRef ref : refs) {
			if (ref.isInvokable(args)) {
				logger.debug("found method identified by key: {}", methodKey);
				return ref;
			}
		}
		
		logger.debug("method {} is not registered", methodKey);
		throw new ExprMethodNotFoundException("method " + methodKey + "(" + parametersSignature(args) + ") couldn't be found");
	}
}
