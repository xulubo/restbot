/**
 * 
 * caseVar
 */
grammar Expr;

expr
	: 	parameterAssigmentExpression EOF
    ;

parameterAssigmentExpression
	:	embedibleExpression
	|	nonExpansionChars
	| 	parameterAssigmentExpression parameterAssigmentExpression+
	;

nonExpansionChars
	:	~('${')
	|	EscDollarCurlyBracket
	;

EscDollarCurlyBracket
	:	'\\${'
	;

//anyChars
//	:	~'${'								//don't plus +, because that way $ doesn't match, but '${' will match this condition
//	|	'^' | '\\$' | '@' | '(' | ')'		//list all non letter non number here
											//special symbols have to be listed in the rule embedded, if put them in 
											//in token, will confuse the token parser
											//TODO: need to make up a sample to explain this
//	;
	
embedibleExpression
	: functionVarExpression
	| CaseVarPathLiteral
	;
	
functionVarExpression
	:	'${' expression '}'
	;
	
functionInvocationExpression
	: Identifier ('.' Identifier)? '(' expressionList? ')'
	;

expressionList
    :   expression (',' ' '* expression)*
    ;
    
expression
    :   primary																		#primaryExpr												
    |	functionInvocationExpression												#funcExpr
	|	sign=('+' | '-') right=expression											#signExpr
    |	left=expression op=('+'|'-') right=expression								#opExpr
    |	left=expression op=('*'|'/') right=expression								#opExpr
    |	left=expression op=('==' | '!=' | '>' | '>=' | '<=' | '<') right=expression	#opCompareExpr
	|	left=expression '=~' RegexLiteral											#opMatchExpr
	;

primary
    :   '(' expression ')'
    | 	literal
	;


literal
    :   FloatingPointLiteral
    |   IntegerLiteral
    |   StringLiteral
    |   BooleanLiteral
    |	CaseVarPathLiteral
    |	Identifier
    ;

FloatingPointLiteral
	: Digits '.' Digits
	;

IntegerLiteral
	: Digits
	;

BooleanLiteral
    :   'true'
    |   'false'
    ;
    
Identifier
	:
	 Letter LetterOrDigit*
	 ;
    
fragment
Letter
    :   [a-zA-Z_]
    ;

fragment
LetterOrDigit
    :   [a-zA-Z0-9_];

fragment    	
Sign
	: [-+]
	;
	    		         
fragment
Digits
    :   [0-9]+
    ;

CaseVarPathLiteral
	: '${' CaseVarPathCharacters '}'
	;

fragment
CaseVarPathCharacters
	: CaseVarPathCharacter+
	;

fragment	
CaseVarPathCharacter
	:   ~[{}()<>]
    ;   

RegexLiteral
	:	'/'	RegexCharacters '/'
	;

fragment
RegexCharacters
	:	RegexCharacter+
	;	
	
fragment
RegexCharacter
	:	~[/] | RegexEscapeSequence
	;

RegexEscapeSequence
	:	'\\/'
	;
	   
StringLiteral
    :   '"' StringCharacters? '"'
    ;

fragment
StringCharacters
    :   StringCharacter+
    ;
    
fragment
StringCharacter
    :   ~["\\]
    |   StringEscapeSequence
    ;        
    
fragment
StringEscapeSequence
    :   '\\' [t"\\]
    ;    

	
WS  :  [ \t\r\n\u000C]+ -> skip
    ;