package com.quickplay.restbot.expr.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.quickplay.restbot.annotations.ExprHandler;
import com.quickplay.restbot.annotations.ExprMethod;
import com.quickplay.restbot.caze.core.Case;

@Component
@ExprHandler
public class CaseVarHandler {
	static final Logger logger = LoggerFactory.getLogger(CaseVarHandler.class);
	
	@ExprMethod
	public Object eval(Case owner, Object paramName) {
		logger.debug("owner:{} valName:{}", owner.getId(), paramName);
		return owner.readValue(paramName.toString());
	}
}
