package com.quickplay.restbot.expr;

import java.lang.reflect.InvocationTargetException;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quickplay.restbot.exceptions.ExprAssertionException;
import com.quickplay.restbot.exceptions.ExprEvaluationException;
import com.quickplay.restbot.expr.ExprParser.EmbedibleExpressionContext;
import com.quickplay.restbot.expr.ExprParser.ExprContext;
import com.quickplay.restbot.expr.ExprParser.FunctionInvocationExpressionContext;
import com.quickplay.restbot.expr.ExprParser.FunctionVarExpressionContext;
import com.quickplay.restbot.expr.ExprParser.LiteralContext;
import com.quickplay.restbot.expr.ExprParser.NonExpansionCharsContext;
import com.quickplay.restbot.expr.ExprParser.ParameterAssigmentExpressionContext;
import com.quickplay.restbot.expr.ExprParser.PrimaryContext;
import com.quickplay.restbot.expr.ExprParser.PrimaryExprContext;
import com.quickplay.restbot.expr.ExprParser.SignExprContext;
import com.quickplay.restbot.expr.MethodRefFactory.MethodRef;



public class ExprParseVisitor extends ExprBaseVisitor<Object> {

	static final Logger logger = LoggerFactory.getLogger(ExprParseVisitor.class);
	ExprParseListener listener = null;
	Object firstFunctionParameter = null;

	public ExprParseVisitor() {
		
	}
	
	public ExprParseVisitor(ExprParseListener listener) {
		this.listener = listener;
	}

	public ExprParseVisitor(Object obj, ExprParseListener listener) {
		this.firstFunctionParameter = obj;
		this.listener = listener;
	}
	
	@Override
	public Object visitPrimaryExpr(PrimaryExprContext ctx) {
		return this.visit(ctx.primary());	
	}


	@Override
	public Object visitPrimary(PrimaryContext ctx) {
		if (ctx.literal() != null) {
			return this.visit(ctx.literal());
		} 
		else {
			return this.visit(ctx.expression());
		}	
	}

	@Override
	public Object visitSignExpr(SignExprContext ctx) {
		String sign = ctx.sign.getText();
		Object right = this.visit(ctx.right);
		switch(sign) {
		case "-":
			if (right instanceof Long) {
				return 0 - (Long)right;
			} else if (right instanceof Double) {
				return 0 - (Double)right;
			} else {
				return this.defaultResult();
			}
		default:
			return right;
		}
	}

	@Override
	public Object visitExpr(ExprContext ctx) {
		return this.visit(ctx.parameterAssigmentExpression());
	}

	@Override
	public Object visitFunctionInvocationExpression(
			FunctionInvocationExpressionContext ctx) {
		String instanceName=null, methodName;
		
    	if (ctx.Identifier().size() == 1) {
    		methodName = ctx.Identifier().get(0).getText(); 
    	}
    	else {
        	instanceName = ctx.Identifier(0).getText(); 
        	methodName = ctx.Identifier(1).getText();
    	}

    	
    	MethodExpr mi = new MethodExpr(instanceName, methodName);
    	
    	if (firstFunctionParameter != null) {
    		mi.addParameter(firstFunctionParameter);
    	}
    	
    	for(ParseTree t : ctx.expressionList().expression()) {
    		Object v = t.accept(this);
    		logger.debug("parsing {} result {}", t.getText(), v);
			mi.addParameter(v);
		}
		
		return call(mi);
	}

	@Override
	public Object visitEmbedibleExpression(EmbedibleExpressionContext ctx) {
		if (ctx.functionVarExpression() != null) {
			return ctx.functionVarExpression().accept(this);
		} else if (ctx.CaseVarPathLiteral() != null) {
			return getCaseVar(ctx.CaseVarPathLiteral());
		} else {
			throw new RuntimeException("unsupported ctx");
		}
	}

	@Override
	public Object visitParameterAssigmentExpression(
			ParameterAssigmentExpressionContext ctx) {
		StringBuilder sb = new StringBuilder();
		for(ParseTree t : ctx.children) {
			logger.trace("parsing " + t.getText());
			Object s;
			if (t instanceof EmbedibleExpressionContext) {
				s = t.accept(this);
			} else if (t instanceof NonExpansionCharsContext) {
				s = t.accept(this);
			} else if (t instanceof ParameterAssigmentExpressionContext) {
				s = t.accept(this);
			} else {
				throw new RuntimeException("can't parse " + ctx.getText());
			}
			logger.trace("result {}", s);
			sb.append(s);
		}
		logger.trace("setting result {}", sb.toString());
		return sb.toString();
	}

	@Override
	public Object visitFunctionVarExpression(FunctionVarExpressionContext ctx) {
		return ctx.expression().accept(this);
	}


	@Override
	public Object visitNonExpansionChars(NonExpansionCharsContext ctx) {
		return ctx.EscDollarCurlyBracket()==null ? ctx.getText() : ctx.getText().substring(1);
	}

	@Override
	public Object visitLiteral(LiteralContext ctx) {
		if (ctx.IntegerLiteral() != null) {
			return Long.valueOf(ctx.IntegerLiteral().getText());
		} else 	if (ctx.BooleanLiteral() != null) {
			return Boolean.valueOf(ctx.BooleanLiteral().getText());
		} else if (ctx.FloatingPointLiteral() != null) {
			return Double.valueOf(ctx.getText());
		} else if (ctx.StringLiteral() != null) {
			String s = ctx.getText();
			return StringEscapeUtils.unescapeJava(s.substring(1, s.length()-1));
		} else if (ctx.CaseVarPathLiteral() != null) {
			return getCaseVar(ctx.CaseVarPathLiteral());
		}
		
		return this.defaultResult();
	}

	private Object getCaseVar(TerminalNode ctx) {
		String s = ctx.getText();
		String varName = s.substring(2, s.length()-1);
		if (this.listener != null) {
			logger.debug("calling listener to grap case var {}", varName);
			return this.listener.getCaseVarVal(varName);
		} else {
			logger.debug("listenr is not set, returning varname {}", varName);
			return varName;
		}		
	}
	
	private Object call(MethodExpr method) {
		Object retVal = null;
		Object[] args = new Object[method.getParameters().size()];
		method.getParameters().toArray(args);
		
		MethodRef methodRef = MethodRefFactory.instance().getMethodRef(method.getInstanceName(), method.getMethodName(), args);
		if (methodRef == null) {
			throw new ExprEvaluationException("Method " + method.getMethodName() + " is not defined");
		}

		try {
			retVal = methodRef.invoke(args);
			logger.debug("result: " + retVal);
		} catch (InvocationTargetException e) {
			if (e.getCause() instanceof ExprAssertionException) {
				throw (ExprAssertionException)e.getCause();
			}
			throw new ExprEvaluationException(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ExprEvaluationException(e);
		} 

		return retVal;
	}
	
}
