package com.quickplay.restbot.expr.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.quickplay.restbot.annotations.ExprHandler;
import com.quickplay.restbot.annotations.ExprMethod;
import com.quickplay.restbot.caze.session.CaseSessionHolder;
import com.quickplay.restbot.net.HttpClient;
import com.quickplay.restbot.utils.BeforeRequestStartListener;

@Component
@ExprHandler("req")
public class RequestOperationHandler {
	static final Logger logger = LoggerFactory.getLogger(RequestOperationHandler.class);
	
	@ExprMethod
	public void header(final String name, final String value) {
		BeforeRequestStartListener l = new BeforeRequestStartListener() {

			@Override
			public void onRequest(HttpClient client) {
				client.addRequestHeader(name, value);
			}
			
		};
		CaseSessionHolder.currentSession().setAttribute(BeforeRequestStartListener.class.getName(), l);
	}


}
