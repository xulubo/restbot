package com.quickplay.restbot.expr.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.quickplay.restbot.annotations.ExprHandler;
import com.quickplay.restbot.annotations.ExprMethod;

@Component
@ExprHandler
public class StringFunctions {
	static final Logger logger = LoggerFactory.getLogger(StringFunctions.class);

	@ExprMethod
	public Object strcat(Object...args) {
		StringBuilder sb = new StringBuilder();
		for(Object o : args) {
			sb.append(o);
		}
		
		return sb.toString();
	}
}
