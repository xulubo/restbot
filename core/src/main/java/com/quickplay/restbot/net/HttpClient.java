package com.quickplay.restbot.net;

import org.springframework.http.ResponseEntity;

import com.quickplay.restbot.utils.BeforeRequestStartListener;

public interface HttpClient {

	void setBeforeRequestStartListener(BeforeRequestStartListener l);

	ResponseEntity<String> get(String url);

	ResponseEntity<String> delete(String url);

	ResponseEntity<String> put(String url, String requestBody);

	ResponseEntity<String> post(String url, String requestBody);

	ResponseEntity<String> head(String url);

	void addRequestHeader(String name, String value);

}
