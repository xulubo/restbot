package com.quickplay.restbot.net;

import com.quickplay.restbot.caze.core.TestContext;
import com.quickplay.restbot.utils.RestClient;

public class DefaultHttpClientFactory implements HttpClientFactory {

	@Override
	public HttpClient getHttpClient(TestContext ctx) {
			return new RestClient();
	}

}
