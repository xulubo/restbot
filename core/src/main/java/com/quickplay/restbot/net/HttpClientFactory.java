package com.quickplay.restbot.net;

import com.quickplay.restbot.caze.core.TestContext;

public interface HttpClientFactory {

	public HttpClient getHttpClient(TestContext ctx);
}
