package com.quickplay.restbot.caze.filter;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quickplay.restbot.AppContext;
import com.quickplay.restbot.annotations.Filter;
import com.quickplay.restbot.annotations.Filters;
import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.core.TestContext;
import com.quickplay.restbot.exceptions.CaseNotExecutedException;

public class DefaultCaseFilterChainFactory {
	private static final Logger logger = LoggerFactory.getLogger(DefaultCaseFilterChainFactory.class);
	
	private List<CaseFilter> caseFilters;
	
	public DefaultCaseFilterChainFactory(TestContext ctx) {
		caseFilters  = Arrays.asList(
				new com.quickplay.restbot.caze.filter.SessionInjectionFilter(),
				new com.quickplay.restbot.caze.filter.PreScriptFilter(),
				new com.quickplay.restbot.caze.filter.CaseExecutionFilter(ctx),
				new com.quickplay.restbot.response.validators.ResponseValidationFilter(),
				new com.quickplay.restbot.caze.filter.ResponseSchemaValidationFilter(ctx),
				new com.quickplay.restbot.caze.filter.ResponseTemplateValidationFilter(ctx),
				new com.quickplay.restbot.caze.filter.PostScriptFilter());
	}
	
	private void add(String name, CaseFilter f, boolean before) {
		AppContext.autowireBean(f);

		for(int i=0; i<caseFilters.size(); i++) {
			if (caseFilters.get(i).name().equals(name)) {
				caseFilters.add(i + (before ? 0 : 1), f);
				return;
			}
		}
		throw new RuntimeException("can't find filter " + name);
	}
	
	public void inject(Filters filters) {
		if (filters == null || filters.value() == null || filters.value().length==0) {
			return;
		}
		
		try {
			for(Filter f : filters.value()) {
				if (!StringUtils.isBlank(f.before())) {
					this.add(f.before(), f.value().newInstance(), true);
					continue;
				}
				if (!StringUtils.isBlank(f.after())) {
					this.add(f.after(), f.value().newInstance(), false);
					continue;
				}
			}
		} catch(Throwable t) {
			throw new RuntimeException(t.getMessage(), t);
		}
	}
	
	public CaseFilterChain newChain() {
		return new MyFilterChain(caseFilters.iterator());
	}
	
	class MyFilterChain implements CaseFilterChain {
		Iterator<CaseFilter> filterIter;
		
		MyFilterChain(Iterator<CaseFilter> iter) {
			this.filterIter = iter;
		}
		
		@Override
		public void doFilter(Case c) {
			try {
				if (filterIter.hasNext()) {
					filterIter.next().filter(c, this);
				}
			} catch(CaseNotExecutedException e) {
				throw e;
			} catch(Exception e) {
				logger.debug("Exception: {}", e.getMessage(), e);
				c.result().addError(e.getMessage(), e);
			}
		}
	}
}
