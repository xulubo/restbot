package com.quickplay.restbot.caze.core;

public abstract class ContextAware {
	private TestContext ctx;
	public ContextAware(TestContext ctx) {
		this.ctx = ctx;
	}
	
	public TestContext getContext() {
		return this.ctx;
	}
}
