package com.quickplay.restbot.caze.core;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

import com.google.common.collect.Maps;
import com.quickplay.restbot.expression.ValidationRule;

public class ValidationRuleSet extends AbstractSet<ValidationRule> {

	private transient Map<String, ValidationRule> map = Maps.newHashMap();
	private transient Map<Integer, ValidationRule> idMap = Maps.newHashMap();
	private Integer nextId = 1;
	
	
	public ValidationRule get(String key) {
		return map.get(key);
	}
	
	public void add(String name, String value) {
		add(new ValidationRule(name, value));
	}
	
	private synchronized Integer nextId() {
		while(idMap.containsKey(nextId)) {
			nextId++;
		}
		
		return nextId++;
	}

	public ValidationRule get(Integer id) {
		return idMap.get(id);
	}

	public ValidationRule save(ValidationRule e) {
		synchronized(nextId) {
			if (e.getId() == null) {
				e.setId(nextId());
			}
			//TODO: is it necessary to handle name duplication?
			map.put(e.getDataPath(), e);
			return idMap.put(e.getId(), e);
		}
	}

	public ValidationRule removeById(Integer id) {
		ValidationRule e = idMap.remove(id);
		if (e != null) {
			map.remove(e.getDataPath());
		}
		
		return e;
	}

	public ValidationRule removeByName(String name) {
		ValidationRule e = map.remove(name);
		if (e != null && e.getId() != null) {
			idMap.remove(e.getId());
		}
		
		return e;
	}

	//-------------------------------------------------------------------------
	// Implementation of Set interface
	//-------------------------------------------------------------------------
	@Override
	public boolean add(ValidationRule e) {
		return save(e) == null;
	}

	
	@Override
	public Iterator<ValidationRule> iterator() {
		return map.values().iterator();
	}

	@Override
	public int size() {
		return map.size();
	}
}
