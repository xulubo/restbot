package com.quickplay.restbot.caze.core;

public interface CaseAllowFilter {
	public boolean isCaseAllowed(String casePath);

}
