package com.quickplay.restbot.caze.core;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Maps;
import com.quickplay.restbot.expression.ExpressionParser;
import com.quickplay.restbot.persist.reader.xml.PlanParamName;

public class ExpressionSet extends AbstractSet<Expression> implements ParameterFinder {

	private transient Map<String, Expression> map = Maps.newHashMap();
	private transient Map<Integer, Expression> idMap = Maps.newHashMap();
	private Integer nextId = 1;
	
	
	public Expression get(String key) {
		return map.get(key);
	}
	
	public Expression get(PlanParamName name) {
		return map.get(name.name());
	}
	
	public void add(String name, String value) {
		add(ExpressionParser.create(name, value));
	}
	
	public String eval(ExpressionEvaluator ee, String name) {
		Expression e = map.get(name);
		if (e == null) {
			return null;
		}
		
		Object ret = e.eval(ee);
		return ret == null ? null : ret.toString();
	}

	private synchronized Integer nextId() {
		while(idMap.containsKey(nextId)) {
			nextId++;
		}
		
		return nextId++;
	}

	public Expression get(Integer id) {
		return idMap.get(id);
	}

	public Expression save(Expression e) {
		synchronized(nextId) {
			if (e.getValue() != null) {
				e.setValue(e.getValue().trim());
			}
			
			if (StringUtils.isBlank(e.getName())) {
				throw new RuntimeException("name is not allowed to be empty");
			}
			if (e.getId() == null) {
				if (map.containsKey(e.getResult())) {
					throw new RuntimeException("failed to add new param, name conflict " + e.getName());
				}
				e.setId(nextId());
			}

			Expression old = map.get(e.getName());
			if (old != null) {
				idMap.remove(old.getId());
			}
			 old = idMap.put(e.getId(), e);
			if (old != null) {
				map.remove(old.getName());
			}

			map.put(e.getName(), e);
			return old;
		}
	}

	public Expression removeById(Integer id) {
		Expression e = idMap.remove(id);
		if (e != null) {
			map.remove(e.getName());
		}
		
		return e;
	}

	public Expression removeByName(String name) {
		Expression e = map.remove(name);
		if (e != null && e.getId() != null) {
			idMap.remove(e.getId());
		}
		
		return e;
	}

	//-------------------------------------------------------------------------
	// Implementation of Set interface
	//-------------------------------------------------------------------------
	@Override
	public boolean add(Expression e) {
		return save(e) == null;
	}

	
	@Override
	public Iterator<Expression> iterator() {
		return idMap.values().iterator();
	}

	@Override
	public int size() {
		return map.size();
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Implementation of interface ParameterFinder
	///////////////////////////////////////////////////////////////////////////
	@Override
	public String find(String key) {
		Expression e = map.get(key);
		return e == null ? null : e.getValue();
	}

	@Override
	public boolean containsKey(String key) {
		return map.containsKey(key);
	}


}
