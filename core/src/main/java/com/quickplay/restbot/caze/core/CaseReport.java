package com.quickplay.restbot.caze.core;

import java.util.List;

public class CaseReport {
	private String id;
	private String description;
	private String result;
	private String errors;
	private String url;
	private String request;
	private String responseHeader;
	private String response;
	private List<CaseReport> childReports;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getErrors() {
		return errors;
	}
	public void setErrors(String errors) {
		this.errors = errors;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	
	public String getResponseHeader() {
		return responseHeader;
	}
	
	public void setResponseHeader(String responseHeader) {
		this.responseHeader = responseHeader;
	}
	
	public List<CaseReport> getChildReports() {
		return this.childReports;
	}
	
	public void setChildReports(List<CaseReport> childReports) {
		this.childReports = childReports;
	}
}
