package com.quickplay.restbot.caze.filter;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.core.CaseUrlBuilder;
import com.quickplay.restbot.caze.core.TestContext;
import com.quickplay.restbot.caze.session.CaseSessionHolder;
import com.quickplay.restbot.exceptions.CaseExecutionException;
import com.quickplay.restbot.exceptions.CaseParseException;
import com.quickplay.restbot.net.HttpClient;
import com.quickplay.restbot.utils.BeforeRequestStartListener;

public class CaseExecutionFilter implements CaseFilter {
	static Logger logger = LoggerFactory.getLogger(CaseExecutionFilter.class);

	private TestContext ctx;
	
	public CaseExecutionFilter(TestContext ctx) {
		this.ctx = ctx;
	}
	
	@Override
	public String name() {
		return "CASE_EXECUTION";
	}
	
	@Override
	public void filter(Case c, CaseFilterChain chain) {
		logger.trace("Enter filter");
		
		try {
			execute(c);
		} catch (HttpClientErrorException e) {
			MediaType mediaType = e.getResponseHeaders().getContentType();
			if (!mediaType.includes(MediaType.APPLICATION_JSON)) {
//				throw new CaseExecutionException(e.getMessage(), e);
				logger.debug("mediaTypes of response doesn't contains JSON");
				c.setResponseEntity(new ResponseEntity<String>(e.getResponseBodyAsString(), e.getResponseHeaders(), e.getStatusCode()));
			}
			else {			
				c.setResponseEntity(new ResponseEntity<String>(e.getResponseBodyAsString(), e.getResponseHeaders(), e.getStatusCode()));
			}
		} catch (Exception e) {
			throw new CaseExecutionException(e.getMessage(), e);		
		}

		c.result().setExecuted();
		

		chain.doFilter(c);
	}

	private boolean execute(Case c) {
		HttpClient restClient = ctx.getHttpClient();
		BeforeRequestStartListener l = (BeforeRequestStartListener)CaseSessionHolder.currentSession().getAttribute(BeforeRequestStartListener.class.getName());
		restClient.setBeforeRequestStartListener(l);
		String url = CaseUrlBuilder.buildUrl(c);
		logger.debug("requesting {}", url);
		c.result().addInfo("url: " + url);

		ResponseEntity<String> responseEntity = null;
		switch(method(c.getRequestMethod())) {
		case GET:
			responseEntity = restClient.get(url);
			break;
		case DELETE:
			responseEntity = restClient.delete(url);
			break;
		case PUT:
			responseEntity = restClient.put(url, c.getRequestBody());
			break;
		case POST:
			responseEntity = restClient.post(url, c.getRequestBody());
			break;
		case HEAD:
			responseEntity = restClient.head(url);
			break;
			
		//do nothing, use this method to represent a util case, like sleep to wait last case to finish
		//because WebAPI call may contain backend task which still run after response is returned
		case PATCH:
			responseEntity = new ResponseEntity<String>(HttpStatus.OK);
			break;
		default:
			throw new CaseExecutionException("request method is not supported ");		
		}
		
		if (responseEntity == null) {
			throw new CaseExecutionException("couldn't get any response");		
		}
		
		logger.debug("request was executed successfully");
		c.setResponseEntity(responseEntity);
		return true;
	}
	
	private HttpMethod method(String requestMethod) {
		if (StringUtils.isBlank(requestMethod)) {
			return HttpMethod.GET;
		}

		try {
			return HttpMethod.valueOf(requestMethod.toUpperCase());
		}
		catch (Throwable t){
			throw new CaseParseException("Illegal method " + requestMethod, t);
		}
	}
}
