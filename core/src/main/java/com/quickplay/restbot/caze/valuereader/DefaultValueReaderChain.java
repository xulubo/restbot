package com.quickplay.restbot.caze.valuereader;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.quickplay.restbot.AppContext;
import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.exceptions.CaseNotExecutedException;
import com.quickplay.restbot.exceptions.VariableUnresolvableException;

public class DefaultValueReaderChain implements ValueReaderChain {

	List<CaseValueReader> responseValueReaders = Arrays.asList(
			new ResponseHeaderValueReader(),
			new JsonResponseValueReader(),
			new DefaultValueReader());
	
	private Iterator<CaseValueReader> readerIterator;

	public DefaultValueReaderChain() {
		AppContext.autowireBean(this);
		readerIterator = responseValueReaders.iterator();
	}
	
	@Override
	public Object read(Case caze, String valName) {
		try {
			if (readerIterator.hasNext()) {
				CaseValueReader reader = readerIterator.next();
				return reader.read(caze, valName, this);
			}
		}
		catch(CaseNotExecutedException e) {
			throw e;
		}
		catch(Throwable t) {
			throw new VariableUnresolvableException("can't resolve variable " + valName + "msg: " + t.getMessage(), t);
		}
				
		return null;
	}
}
