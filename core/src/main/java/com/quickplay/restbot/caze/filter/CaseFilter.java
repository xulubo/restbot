package com.quickplay.restbot.caze.filter;

import com.quickplay.restbot.caze.core.Case;

public interface CaseFilter {

	void filter(Case c, CaseFilterChain chain);
	String name();
}
