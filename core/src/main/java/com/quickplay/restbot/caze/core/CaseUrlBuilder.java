package com.quickplay.restbot.caze.core;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriComponentsBuilder;

import com.quickplay.restbot.exceptions.ConfigurationError;
import com.quickplay.restbot.persist.reader.xml.PlanParamName;
import com.quickplay.restbot.utils.ParamUtils;

public class CaseUrlBuilder {
	private static Logger logger = LoggerFactory.getLogger(CaseUrlBuilder.class);

	final public static String buildUrl(Case c) {
		logger.debug("building url for case {}", c.getId());
		String url = null;
		String p = c.getServicePath();

		UriComponentsBuilder builder = getUriBuilder(c);
		if (StringUtils.isNotBlank(p)) {
			p = p.trim();
			p = ParamUtils.extend(p, c.getProperties());
			//separate query string from path
			String parts[] = p.split("\\?", 2);
			builder.path(parts[0]);
			if (parts.length>1) {
				builder.query(parts[1]);
			}
		}
		
		p=c.getCommonParams();
		if (StringUtils.isNotBlank(p)) {
			p = p.trim();
			
			//TODO: is this still necessary? param should be parsed and extended in "get" methods
			//builder.query(ParamUtils.extend(p, c.getSuite().getParamMap()));
			builder.query(p);
		}

		for(Expression e : c.getQueryParams()) {
			String name = e.getName();
			String value = c.eval(e);
			
			logger.debug("name={} value={}", name, value);
			try {
				value = URLEncoder.encode(value, "UTF-8");
			} catch (UnsupportedEncodingException ex) {
				logger.error("failed to encode param {}", ex.getMessage());
			}
			builder.queryParam(name, value);
		}

		url = builder.build(false).toUriString();
		//TODO: does it still need to be extended?
		//maybe it is better to extend through a call back rather than passing a parameter map
		//url = ParamUtils.extend(url, c.getSuite().getParamMap());

		return url;
	}
	

	private static UriComponentsBuilder getUriBuilder(Case c) {
		String p;
		UriComponentsBuilder builder = null;
		
		String baseUrl = c.getParam(PlanParamName.baseUrl.name());
		if (StringUtils.isNotBlank(baseUrl)) {
			builder = UriComponentsBuilder.fromHttpUrl(baseUrl);
		} else {
			String host = c.getParam(PlanParamName.host.name());
			if (StringUtils.isNotBlank(host)) {
				builder = UriComponentsBuilder.newInstance();
				builder.scheme("http");
				builder.host(host);
				if (StringUtils.isNotBlank(p=c.getParam(PlanParamName.port.name()))) {
					builder.port(Integer.valueOf(p.trim()));
				}
			}
		}
		
		if (builder == null) {
			throw new ConfigurationError("neither baseUrl nor host defined for case " + c.getId());
		}

		return builder;
	}	
}
