package com.quickplay.restbot.caze.filter;

import com.quickplay.restbot.caze.core.Case;

public interface CaseFilterChain {

	public void doFilter(Case c);
}
