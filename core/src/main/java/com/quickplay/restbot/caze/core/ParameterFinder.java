package com.quickplay.restbot.caze.core;

public interface ParameterFinder {

	String find(String key);
	boolean containsKey(String match);
}
