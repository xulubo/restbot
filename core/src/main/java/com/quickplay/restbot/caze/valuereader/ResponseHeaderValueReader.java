package com.quickplay.restbot.caze.valuereader;

import org.springframework.http.ResponseEntity;

import com.quickplay.restbot.caze.core.Case;

/*
 * Read the case response header value denoted by the path starting with H.
 */
public class ResponseHeaderValueReader implements CaseValueReader {

	@Override
	public Object read(Case caze, String valName, ValueReaderChain chain) {
		if (!accept(valName)) {
			return chain.read(caze, valName);
		}
		
		ResponseEntity<String> response = caze.getResponseEntity();
		String headerName = valName.substring(2);
		if (headerName.equalsIgnoreCase("status")) {
			return response.getStatusCode().value();
		}
		else {
			String headerValue = response.getHeaders().getFirst(headerName);
			return headerValue;
		}
	}

	public boolean accept(String path) {
		return path.startsWith("H.");
	}

}
