package com.quickplay.restbot.caze.valuereader;

import com.quickplay.restbot.caze.core.Case;


public interface ValueReaderChain {

	public Object read(Case caze, String valName);
}
