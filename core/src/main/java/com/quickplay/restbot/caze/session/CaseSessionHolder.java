package com.quickplay.restbot.caze.session;


public class CaseSessionHolder {

	private static CaseSession globalSession = new CaseSession();
	
    private static final ThreadLocal<CaseSession> contextHolder =
        new ThreadLocal<CaseSession>() {
            @Override protected CaseSession initialValue() {
                return new CaseSession();
        }
    };

    public static CaseSession currentSession() {
        return contextHolder.get();
    }
    
    public static CaseSession globalSession() {
    	return globalSession;
    }

}
