package com.quickplay.restbot.caze.core;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class BasicCaseAllowFilter implements CaseAllowFilter {

	private Set<String> allowedCases = new HashSet<String>();
	private List<Pattern> patterns = new LinkedList<Pattern>();

	public BasicCaseAllowFilter() {
		
	}
	
	public BasicCaseAllowFilter(Collection<String> casePaths) {
		addAllowedCasePaths(casePaths);
	}
	
	public boolean isCaseAllowed(String casePath) {
		
		if (!patterns.isEmpty()) {
			for(Pattern m : patterns) {
				if (!m.matcher(casePath).matches()) {
					return false;
				}
			}
		}
		
		if (allowedCases.isEmpty()) {
			return true;
		}
		
		if (allowedCases.contains(casePath)) {
			return true;
		}
		
		return false;
	}
	
	public void addAllowedCasePaths(Collection<String> casePaths) {
		allowedCases.addAll(casePaths);
	}

	/*
	 * caseId must match the passed pattern
	 */
	public void addIDPattern(String pattern) {
		this.patterns.add(Pattern.compile(pattern));
	}	
}
