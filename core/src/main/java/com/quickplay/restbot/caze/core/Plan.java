package com.quickplay.restbot.caze.core;

import com.quickplay.restbot.persist.reader.xml.BaseEntity;


/**
 * Plan is the root case
 * 
 * @author robertx
 *
 */
public class Plan extends Case {

	public Plan() {}
	public Plan(BaseEntity entity) {
		super(null, entity);
	}
}
