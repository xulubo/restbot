package com.quickplay.restbot.caze.core;

public interface ExpressionEvaluator {

	Object eval(String e);
}
