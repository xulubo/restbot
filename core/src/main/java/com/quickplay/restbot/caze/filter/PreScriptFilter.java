package com.quickplay.restbot.caze.filter;

import org.springframework.stereotype.Component;

import com.quickplay.restbot.caze.core.Case;

@Component
public class PreScriptFilter extends ScriptFilter {

	@Override
	public String name() {
		return "PRE_SCRIPT";
	}
	
	@Override
	public void filter(Case c, CaseFilterChain chain) {
		execute(c.getPreScript());
		chain.doFilter(c);
	}
}
