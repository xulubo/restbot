package com.quickplay.restbot.caze.core;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;

public class CaseFinder {
	private static Logger logger = LoggerFactory.getLogger(CaseFinder.class);
	
	private static Case findCaseByAbsolutePath(Case root, List<String> pathList) {
		Case cur = root;
		for(String path : pathList) {
			if (path.isEmpty()) continue;		//empty path will be ignored, so :::, will equal to :
			cur = cur.children().get(path);
			if (cur == null) {
				return null;
			}
		}

		return cur;
	}

	/**
	 * 
	 * @param current
	 * @param casePath   if a var refers to a case denoted by a:b:c in current entity,  it means ../../../a/b/c corresponding to a file system tree
	 * @return
	 */
	private static Case findSiblingCase(Case current, String caseId) {
		Case parent = current.getParent();
		if (parent == null) {
			return null;
		}
		

		return parent.children().get(caseId);
	}
	
	/**
	 * :a:b:c and a:b:c all mean to find a case by absolute path
	 * a single id (without colon) mean to find a case in the same parent case
	 * 
	 * @param current
	 * @param path
	 * @return
	 */
	public static Case findCaseByPath(Case current, String path) {
		if (path.isEmpty()) {
			logger.warn("path is empty, current case is returned");
			return current;
		}
		
		List<String> list = Splitter.on(":").splitToList(path);

		return list.size() == 1 ? findSiblingCase(current, list.get(0)) : findCaseByAbsolutePath(current.getRoot(), list);
	}

	
	public static Case find(Case current, String casePath) {
		return findCaseByPath(current, casePath);
	}
}
