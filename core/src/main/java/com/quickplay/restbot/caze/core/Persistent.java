package com.quickplay.restbot.caze.core;

public interface Persistent {
	void save();
	void delete();
}
