package com.quickplay.restbot.caze.core;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import com.google.common.collect.Lists;

public class CaseReporter {
	private static Logger logger = LoggerFactory.getLogger(CaseReporter.class);

	public static CaseReport createReport(Case c) {
		
		CaseReport r = new CaseReport();
		ResponseEntity<String> responseEntity = c.getResponseEntity();
		
		r.setId(c.getId());
		r.setDescription(c.getDescription());

		if (!c.result().isExecuted()) {
			r.setResult("Not Started");
		}

		r.setResult(c.result().isPassed() ? CaseStatus.passed.name() : CaseStatus.failed.name());
		try {
			r.setUrl(CaseUrlBuilder.buildUrl(c));
		} catch(Exception e) {
			logger.error("Case ID: " + c.getId());
			r.setUrl("failed to build url");
		}

		StringBuilder sb = new StringBuilder();
		for(String error : c.result().getErrors()) {
			if (sb.length() > 0) {
				sb.append("\n");
			}
			sb.append(error);
		}
		r.setErrors(sb.toString());

		if (c.getRequestBody() != null) {
			r.setRequest(c.getRequestBody());
		}
		
		if (responseEntity != null) {
			StringBuilder header = new StringBuilder();
			header.append(String.format("HTTP/1.1 %d %s\n", 
					responseEntity.getStatusCode().value(), 
					responseEntity.getStatusCode().getReasonPhrase()));
			for(Entry<String, List<String>> e : responseEntity.getHeaders().entrySet()) {
				for(String v : e.getValue()) {
					header.append(e.getKey()).append(":").append(v).append("\n");
				}
			}
			r.setResponseHeader(header.toString());
			
			if (responseEntity.getBody() != null) {
				try {
					//check if a valid UTF-8 String
					responseEntity.getBody().getBytes("UTF-8");
					r.setResponse(responseEntity.getBody());
				} catch (UnsupportedEncodingException e1) {
					r.setResponse("[binary length=" + responseEntity.getBody().length() + "]");
				}
				
			}
		}
		
		if (!c.isLeaf()) {
			List<CaseReport> childReports = Lists.newLinkedList();
			for(Case cc : c.children().values()) {
				childReports.add(createReport(cc));
			}
			r.setChildReports(childReports);
		}
		return r;
	}

}
