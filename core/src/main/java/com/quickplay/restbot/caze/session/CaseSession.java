package com.quickplay.restbot.caze.session;

import java.util.Map;

import com.google.common.collect.Maps;
import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.core.TestContext;

public class CaseSession {

	private TestContext ctx;
	Case caze;
	
	private Map<String, Object> attributes = Maps.newHashMap();
	
	public Case getCase() {
		return caze;
	}

	public void setCase(Case caze) {
		this.caze = caze;
	}
	
	public TestContext getContext() {
		return ctx;
	}
	
	public void setContext(TestContext ctx) {
		this.ctx = ctx;
	}
	
	public Object getAttribute(String name) {
		return attributes.get(name);
	}
	
	public void setAttribute(String name, Object object) {
		attributes.put(name, object);
	}
	
	public void clear() {
		caze = null;
		attributes.clear();
	}	
}
