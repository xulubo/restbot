package com.quickplay.restbot.caze.core;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.http.util.Asserts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Maps;
import com.quickplay.restbot.caze.valuereader.DefaultValueReaderChain;
import com.quickplay.restbot.exceptions.CaseNotFoundException;
import com.quickplay.restbot.expression.CaseVariable;
import com.quickplay.restbot.expression.ExpressionEvaluatorImpl;
import com.quickplay.restbot.persist.reader.xml.BaseEntity;
import com.quickplay.restbot.persist.reader.xml.EntityDelegate;
import com.quickplay.restbot.persist.reader.xml.PlanParamName;
import com.quickplay.restbot.response.validators.ExpressionValidator;

public class Case extends EntityDelegate {
	final static Logger logger = LoggerFactory.getLogger(Case.class);

	//named values set at runtime
	private Result result = new Result();
	private ExpressionEvaluatorImpl evalFactory;
	private ExpressionValidator validator;
	private LinkedHashMap<String, Case> deps;
	private Case parent;
	Map<String, Case> children = Maps.newHashMap();

	
	protected ResponseEntity<String> responseEntity;


	/*
	 * to create a root case
	 */
	public Case() {
		super(new BaseEntity());
	}
	
	/*
	 * to create a child case
	 */
	public Case(Case parent) {
		this(parent, new BaseEntity());
	}

	/*
	 * add a existent case in the case tree
	 */
	public Case(Case parent, BaseEntity entity) {
		super(entity);
		this.parent = parent;

		evalFactory = new ExpressionEvaluatorImpl(this);
		validator = new ExpressionValidator(this);
		
		for(BaseEntity e : getEntity().getChildren().values()) {
			Case c = new Case(this, e);
			children.put(c.getId(), c);
		}
	}
		
	public final String getResponseBody() {
		if (this.responseEntity == null) {
			return null;
		}

		return this.responseEntity.getBody();
	}
	
	public Case getParent() {
		return this.parent;
	}

	public void addChild(BaseEntity e) {
		
		synchronized(this) {
			if (e.getId() == null) {
				int id = 0;
				for(BaseEntity b: getEntity().getChildren().values()) {
					if (NumberUtils.isDigits(b.getId())) {
						id = Math.max(id, Integer.valueOf(b.getId()));						
					}
				}
				id++;
				e.setId(String.valueOf(id));
			}
		}
		
		Asserts.notBlank(e.getId(), "Entity ID");
		if (StringUtils.isEmpty(e.getProperties().get(PlanParamName.name))) {
			e.getProperties().add(PlanParamName.name.name(), e.getId());
		}
		if (e instanceof BaseEntity) {
			getEntity().getChildren().put(e.getId(), e);
			children.put(e.getId(), new Case(this, e));
		} else {
			throw new RuntimeException("Unsupported entity type " + e.getClass().getName());
		}
	}

	public Map<String, Case> children() {		
		return Collections.unmodifiableMap(children);
	}
	
	public Case getRoot() {
		Case p = this;
		for(; !p.isRoot(); p=p.getParent());
		return p;
	}
	
	@JsonIgnore
	public ResponseEntity<String> getResponseEntity() {
		return this.responseEntity;
	}

	public void setResponseEntity(ResponseEntity<String> responseEntity) {
		this.responseEntity = responseEntity;
	}

	public Object readValue(String valPath) throws CaseNotFoundException {
		CaseVariable var = CaseVariable.parse(valPath);
		Case c = this;
		DefaultValueReaderChain chain = new DefaultValueReaderChain();
		
		if (var.getCasePath() != null) {
			c = CaseFinder.find(this, var.getCasePath());
			if (c == null) {
				throw new CaseNotFoundException(var.getCasePath());
			}
		}
		
		return chain.read(c, var.getValName());
	}
	
	public Result result() {
		return result;
	}
		
	public String eval(Expression ex) {
		if (ex == null) {
			return null;
		}
		
		Object ret = ex.eval(evalFactory);
		
		return ret == null ? null : ret.toString();
	}

	public Object eval(String ex) {
		return evalFactory.eval(ex);
	}
	
	public void validate(Object val, String rule) {
		validator.validate(val, rule);
	}
	


	public LinkedHashMap<String, Case> findDependency() {
		if (deps != null) {
			return deps;
		}
		
		deps = CaseDependencyDiscover.discoverDependencies(this);
		return deps;
	}

	public CharSequence buildUrl() {
		return CaseUrlBuilder.buildUrl(this);
	}

	public Case find(String casePath) {
		Case c =  CaseFinder.findCaseByPath(this, casePath);
		if (c == null) {
			throw new CaseNotFoundException(casePath);
		}
		
		return c;
	}

	public int newChildId() {
		int i = 0;
		for(BaseEntity e : getChildren().values()) {
			try {
				i = Math.max(i, Integer.valueOf(e.getId()));
			} catch(Exception ex) {
				//ignore
			}
		}
		return i+1;
	}

	public void remove() {
		if (getParent() != null) {
			getParent().getChildren().remove(this.getId());
		}
	}

	public CaseReport createReport() {
		return CaseReporter.createReport(this);
	}
}
