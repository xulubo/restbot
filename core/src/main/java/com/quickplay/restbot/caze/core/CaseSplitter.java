package com.quickplay.restbot.caze.core;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.google.common.collect.Lists;
import com.quickplay.restbot.expression.CollectionExpression;

public class CaseSplitter {

	/**
	 * Is this a case with any ranged parameter inside
	 * @return
	 */
	final public boolean isCollectionCase(Case c) {
		for(Expression e : c.getQueryParams()) {
			if (e instanceof CollectionExpression) {
				return true;
			}
		}

		return false;
	}
	
	public static List<ChildCase> split(Case c) {
		List<ChildCase> childCases = Lists.newLinkedList();	
		List<ExpressionSet> mapList = getCombinationList(c.getQueryParams().iterator());
		if (mapList.size()>1) {
			int seqNo=0;
			for(ExpressionSet params : mapList) {
				ChildCase cc = new ChildCase(c, params, seqNo++);
				childCases.add(cc);
			}			
		}
		
		return childCases;
	}
	
	/**
	 * Iterate the whole parameters list to get all the combinations
	 * 
	 * It is a recursive call, result = list of a collection + (all combination of later collections)
	 * 
	 * @param iter
	 * @return
	 */
	protected static List<ExpressionSet> getCombinationList(Iterator<Expression> iter) {
		List<ExpressionSet> ret = Lists.newLinkedList();
		if (!iter.hasNext()) {
			ret.add(new ExpressionSet());
			return ret;
		}

		
		Expression expression = iter.next();
		String paramName = expression.getName();
		
		List<ExpressionSet> lst = getCombinationList(iter);
				
		if (expression instanceof CollectionExpression) {
			Collection<String> paramValues = ((CollectionExpression)expression).values();

			for(ExpressionSet l : lst) {
				for(String paramValue : paramValues) {
					ExpressionSet tmp = new ExpressionSet();
					tmp.addAll(l);
					tmp.add(paramName, paramValue);
					ret.add(tmp);
				}
			}

		}
		else {
			for(ExpressionSet l : lst) {
				l.add(expression);
			}
			
			ret = lst;
		}
	
		return ret;
	}	
}
