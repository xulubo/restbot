package com.quickplay.restbot.caze.core;


public class ChildCase extends Case {

	final Case parent;
	final int seqNo;

	public ChildCase(Case parent, ExpressionSet queryParams, int seqNo) {
		super(parent);
		setId(parent.getId() + ":" + seqNo);
		this.parent = parent;
		this.seqNo = seqNo;
		this.setQueryParams(queryParams);
	}

	public int getSeqNo() {
		return seqNo;
	}
	
	@Override
	public String getId() {
		return super.getId() + "%" + getSeqNo();
	}
}
