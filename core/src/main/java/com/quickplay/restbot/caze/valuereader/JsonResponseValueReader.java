package com.quickplay.restbot.caze.valuereader;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.exceptions.CaseNotExecutedException;
import com.quickplay.restbot.exceptions.CaseResponseValidationException;

/*
 * Read the case response value denoted by the path starting with $.
 */
public class JsonResponseValueReader implements CaseValueReader {
	private Logger logger = LoggerFactory.getLogger(JsonResponseValueReader.class);

	@Override
	public Object read(Case caze, String jsonPath, ValueReaderChain chain) {
		if (accept(jsonPath)) {
			if (!caze.result().isExecuted()) {
				throw new CaseNotExecutedException(caze.getPath());
			}
			return read(caze.getResponseEntity().getBody(), jsonPath);
		}
		return chain.read(caze, jsonPath);
	}
	
	public Object read(String jsonData, String jsonPath) {
		if (jsonData == null) {
			return null;
		}
		
		ReadContext readContext;
		
		try {
			readContext = JsonPath.parse(jsonData);
		} catch (IllegalArgumentException e) {
			throw new CaseResponseValidationException("Not valid json data: " + jsonData);			
		}
		
		String parts[] = jsonPath.split("/", 2);
		if (parts.length == 1) {
			try {
				return readContext.read(jsonPath);
			} catch(IllegalArgumentException e) {
				logger.debug("Failed to read json element\n, JSON data: {}\n jsonPath:{}", jsonData, jsonPath);
				throw e;
			}
		}
		
		String arrayPath = parts[0];
		String elementPath = parts[1];
		Object array = readContext.read(arrayPath);
		if (!(array instanceof Collection)) {
			throw new CaseResponseValidationException("Object type is not a collection " + arrayPath);
		}
		
		List<Object> list = Lists.newLinkedList();
		for(Object elements : (Collection<?>)array) {
			try {
				String elementJson = new ObjectMapper().writeValueAsString(elements);
				Object value = read(elementJson, elementPath);
				list.add(value);
			} catch (JsonProcessingException e) {
				throw new CaseResponseValidationException(e.getMessage(), e);
			}
		}
		
		return list;
	}

	@Override
	public boolean accept(String path) {
		return path.startsWith("$.");
	}

}
