package com.quickplay.restbot.caze.core;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.quickplay.restbot.expr.ExprEvaluator;
import com.quickplay.restbot.expr.ExprParseListener;
import com.quickplay.restbot.expression.CaseVariable;

public class CaseDependencyDiscover {
	final static Logger logger = LoggerFactory.getLogger(CaseDependencyDiscover.class);

	public static LinkedHashMap<String, Case> discoverDependencies(Case caze) {
		CaseDependencyDiscover discover = new CaseDependencyDiscover();
		return discover.discoverDependencies(caze, new HashMap<String, Case>());
	}
	

	
	private LinkedHashMap<String, Case> discoverDependencies(final Case caze, final HashMap<String, Case> parentalCaseChain) {
		
		//cases depended on
		final LinkedHashMap<String, Case> deps = Maps.newLinkedHashMap();
		
		ExprParseListener l = new ExprParseListener(){
			@Override
			public Object getCaseVarVal(String varName) {
				CaseVariable v = CaseVariable.parse(varName);
				
				//case must not exist in the stacked dependency chain
				if (parentalCaseChain.containsKey(v.getCasePath())) {
					throw new RuntimeException("DependencyLoopException");
				}
				
				if (StringUtils.isNotBlank(v.getCasePath()) && !deps.containsKey(v.getCasePath())) {
					logger.debug("depending case {}", v.getCasePath());
					Case c = CaseFinder.findCaseByPath(caze, v.getCasePath());
					
					if (c != null) {
						deps.put(c.getPath(), c);
						LinkedHashMap<String, Case> childDeps = discoverDependencies(c, parentalCaseChain);
						deps.putAll(childDeps);
					} else {
						logger.error("can't find case {}", v.getCasePath());
					}
				}
				return varName;
			}
			
		};
		
		//child case must not depend on parental case, because it leads to a dead dependency loop
		parentalCaseChain.put(caze.getPath(), caze);
		for(Expression p : caze.getQueryParams()) {
			if (StringUtils.isNotBlank(p.getValue())) {
				ExprEvaluator.INSTANCE.evaluate(p.getValue(), l);
			}
		}
		parentalCaseChain.remove(caze.getPath());

		return deps;
	}
}
