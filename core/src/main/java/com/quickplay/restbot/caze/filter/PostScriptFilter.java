package com.quickplay.restbot.caze.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.expr.ExprEvaluator;

@Component
public class PostScriptFilter extends ScriptFilter {

	final static Logger logger = LoggerFactory.getLogger(PostScriptFilter.class);
	
	ExprEvaluator exprEvaluator = ExprEvaluator.INSTANCE;
	
	@Override
	public String name() {
		return "POST_SCRIPT";
	}
	
	@Override
	public void filter(Case c, CaseFilterChain chain) {
		execute(c.getPostScript());
		chain.doFilter(c);
	}
}
