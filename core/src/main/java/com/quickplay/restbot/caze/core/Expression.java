package com.quickplay.restbot.caze.core;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.Expose;

/**
 * use Gson to write to file, @Expose works
 * 
 * use Jackson to response to browser,  @JsonIgnore works
 * @author robertx
 *
 */
public class Expression {

	@XmlAttribute
	private Integer id;
	
	@Expose
	@XmlAttribute
	private String name;


	@Expose
	private String value;

	@JsonIgnore
	private Object result = null;	//used to cache the result of the expression evaluated
	
	public Expression() {}
	
	public Expression(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlValue
	public String getValue() {
		return this.value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	/*
	 * make it possible to auto cache the evaluated result with this method
	 */
	public Object eval(ExpressionEvaluator ee) {
		if (result == null) {
			result = ee.eval(this.getValue());
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		return getValue();
	}
}
