package com.quickplay.restbot.caze.core;

public enum CaseStatus {
	prepared, starting, error, failed, passed, finished
}