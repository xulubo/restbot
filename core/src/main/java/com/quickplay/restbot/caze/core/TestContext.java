package com.quickplay.restbot.caze.core;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.util.Assert;

import com.quickplay.restbot.AppContext;
import com.quickplay.restbot.exceptions.ResourceNotFoundException;
import com.quickplay.restbot.net.DefaultHttpClientFactory;
import com.quickplay.restbot.net.HttpClient;
import com.quickplay.restbot.net.HttpClientFactory;
import com.quickplay.restbot.persist.reader.xml.ParameterMap;
import com.quickplay.restbot.persist.reader.xml.PlanParamName;

/**
 * Shared by a suite of tests (an single excel file define a test suite)
 * Cases can be related, so a case can reference variable from another case within the same context
 * @author robertx
 *
 */
public class TestContext {
	static final Logger logger = LoggerFactory.getLogger(TestContext.class);
	private final Plan plan;
	//use LinkedHashMap to keep the elements order the same as they are inserted
	private TestReport report = new TestReport(this);
	private String outDir = "output";
	private String resDir = "resources";
	private JdbcTemplate jdbcTemplate;
	private String instanceOutputDir;
	private HttpClientFactory httpClientFactory = new DefaultHttpClientFactory();
	private DataSource dataSource = null;
	
	public TestContext(Plan plan) {
		this.plan = plan;
	}
	
	public TestContext(Plan plan, CaseAllowFilter filter, DataSource ds) {
		this.dataSource = ds;
		this.plan = plan;	
	}
	
	
	public Plan getPlan() {
		return this.plan;
	}
	
	public void setCaseFinished(Case c) {
		this.report.add(c);
	}

	public TestReport getReport() {
		return this.report;
	}
	

	public Resource getResource(String resourcePath) {
		Resource resource;

		String path = plan.getParam(PlanParamName.resourceFolder.name());
		if (StringUtils.isBlank(path)) {
			path = System.getProperty("user.dir");
		}
		
		path += "/" + resourcePath;
		resource = AppContext.instance().getApplicationContext().getResource(path);
		if (resource != null && resource.isReadable()) {
			return resource;
		}		

		// find resource in Windows-style absolute path
		else if (path.matches("[a-zA-Z]+:.*")) {
			logger.trace("not found {}", path);
			throw new ResourceNotFoundException("Couldn't read resource file " + path);
		}
		// find resource in path relative to the current folder
		else {
			path = "file:" + path;
		}
		
		logger.debug("reading {}", path);
		resource = AppContext.instance().getApplicationContext().getResource(path);
		
		if (resource == null || !resource.isReadable()) {
			throw new ResourceNotFoundException("Couldn't read resource file " + path);
		}

		return resource;
	}
	
	public String getOutputDir() {
		if (instanceOutputDir != null) {
			return instanceOutputDir;
		}
		
		File dir = new File(outDir);
		if (!dir.isDirectory()) {
			Assert.isTrue(dir.mkdir(), "make output folder");
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		
		instanceOutputDir = outDir + File.separator + sdf.format(new Date());
		new File(instanceOutputDir).mkdirs();
		
		return instanceOutputDir;
	}
	/**
	 * Set output directory, all output files will be saved in this folder
	 * 
	 * @param outDir
	 * @return
	 */
	public void setOutputDir(String outDir) {
		this.outDir = outDir;
		getOutputDir();
	}
	
	public String getResourceDir() {
		return resDir;
	}
	public void setResourceDir(String resDir) {
		this.resDir = resDir;
	}
	
	public File getOutputFile(String basename) {
		basename = basename.replaceAll("[\\\\/:*?\"<>|]", "_");
		return new File(getOutputDir() + "/" + basename);
	}	
	
	private DataSource getDataSource(String jdbcUrl, String username, String password) {
		if (this.dataSource == null && jdbcUrl.startsWith("jdbc:oracle:")) {
			this.dataSource = new SimpleDriverDataSource(new oracle.jdbc.driver.OracleDriver(), jdbcUrl, username, password);
		}

		return this.dataSource;
	}
	
	public JdbcTemplate getJdbcTemplate() {
		if (jdbcTemplate == null) {
			String url = plan.getParam(PlanParamName.dbUrl.name());
			String username = plan.getParam(PlanParamName.dbUsername.name());
			String password = plan.getParam(PlanParamName.dbPassword.name());
			jdbcTemplate = new JdbcTemplate(getDataSource(url, username, password));			
		}
		
		return jdbcTemplate;
	}

	public HttpClientFactory getHttpClientFactory() {
		return httpClientFactory;
	}

	public void setHttpClientFactory(
			HttpClientFactory defaultHttpClientFactory) {
		this.httpClientFactory = defaultHttpClientFactory;
	}
	
	public HttpClient getHttpClient() {
		return this.httpClientFactory.getHttpClient(this);
	}

	private ParameterMap<String> params = new ParameterMap<String>();
	//override plan param with system properties or Config annotation
	public void setParam(String name, String val) {
		params.put(name, val);
	}

	public String getParam(String paramName) {
		return params.get(paramName);			
	}

}

