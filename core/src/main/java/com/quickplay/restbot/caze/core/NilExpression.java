package com.quickplay.restbot.caze.core;

/*
 * NilExpression is used to override super parameters by always returning a null value
 */
public class NilExpression extends Expression {

	
	@Override
	public String getValue() {
		return null;
	}
}
