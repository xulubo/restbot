package com.quickplay.restbot.caze.valuereader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quickplay.restbot.caze.core.Case;

/*
 * Regard the path as a variable name and check if it exists in either of the variable pools pathVariables, queryParms
 * or context params
 */
public class DefaultValueReader implements CaseValueReader {
	private static final Logger logger = LoggerFactory.getLogger(DefaultValueReader.class);
	
	@Override
	public Object read(Case caze, String name, ValueReaderChain chain) {
		logger.trace("Enter DefaultValueReader.read()");
		String value;
		
		//check if it is a path variable
		value = caze.getProperty(name);
		if (value != null) {
			return value;
		}
		
		//check if it is a query parameter
		value = caze.getFirstQueryParameter(name).getValue();
		if (value != null) {
			return value;
		}
		
		return chain.read(caze, name);
	}

	@Override
	public boolean accept(String path) {
		return true;
	}
}
