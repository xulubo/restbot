package com.quickplay.restbot.caze.core;

import java.util.Collection;
import java.util.Iterator;

import com.quickplay.restbot.persist.reader.xml.PlanParamName;

public class UnmodifiableExpressionSet extends ExpressionSet {

	private ExpressionSet set;

	public UnmodifiableExpressionSet(ExpressionSet set) {
		this.set = set;
	}
	
	public Expression get(String key) {
		return set.get(key);
	}

	public Expression get(PlanParamName name) {
		return set.get(name);
	}

	public void add(String name, String value) {
		set.add(name, value);
	}

	public String eval(ExpressionEvaluator ee, String name) {
		return set.eval(ee, name);
	}

	public Expression get(Integer id) {
		return set.get(id);
	}

	public Expression save(Expression e) {
		throw new RuntimeException("Not allowed to modify");
	}

	public boolean equals(Object o) {
		return set.equals(o);
	}

	public Expression removeById(Integer id) {
		return set.removeById(id);
	}

	public Expression removeByName(String name) {
		return set.removeByName(name);
	}

	public boolean isEmpty() {
		return set.isEmpty();
	}

	public boolean add(Expression e) {
		return set.add(e);
	}

	public boolean contains(Object o) {
		return set.contains(o);
	}

	public Iterator<Expression> iterator() {
		return set.iterator();
	}

	public int size() {
		return set.size();
	}

	public String find(String key) {
		return set.find(key);
	}

	public int hashCode() {
		return set.hashCode();
	}

	public boolean containsKey(String key) {
		return set.containsKey(key);
	}

	public Object[] toArray() {
		return set.toArray();
	}

	public boolean removeAll(Collection<?> c) {
		return set.removeAll(c);
	}

	public <T> T[] toArray(T[] a) {
		return set.toArray(a);
	}

	public boolean remove(Object o) {
		return set.remove(o);
	}


	public boolean addAll(Collection<? extends Expression> c) {
		return set.addAll(c);
	}


	public String toString() {
		return set.toString();
	}
}
