package com.quickplay.restbot.caze.valuereader;

import com.quickplay.restbot.caze.core.Case;


/*
 * read case internal component value according to the path, for example
 * 
 * $.a.b denotes a JSON path in HTTP response
 * H.a.b denotes the value of a.b in HTTP response header
 */
public interface CaseValueReader {

	Object read(Case caze, String path, ValueReaderChain chain);

	//if the value path is recognized by the reader
	boolean accept(String path);
	
}
