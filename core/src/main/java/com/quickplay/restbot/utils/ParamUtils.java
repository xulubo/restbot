package com.quickplay.restbot.utils;

import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;
import com.quickplay.restbot.caze.core.ParameterFinder;
import com.quickplay.restbot.exceptions.ParameterNotFoundException;

public class ParamUtils {
	private static Logger logger = LoggerFactory.getLogger(ParamUtils.class);
	private static final Pattern VAR_PATTERN = Pattern
			//.compile("\\$\\{([^/]+?)\\}");
			.compile("(([\\\\]{2})*[\\\\]{0}(\\$\\{([^$/}]+?)\\}))");

	/*
	 * @Param defaultParamValue replacement of missing parameter,
	 * IllegalParameterException will be thrown if defaultStr is null and there
	 * is missing parameter
	 */
	public static Set<String> extractVariableNames(String template) 
	{
		System.err.println(template);
		Set<String> names = Sets.newHashSet();
		if (TextUtils.isEmpty(template)) {
			return names;
		}

		Matcher m = VAR_PATTERN.matcher(template);

		while (m.find()) {
			for(int i=0; i<=m.groupCount(); i++) {
				System.err.println(""+i+":"+m.group(i));
			}
			
			if (m.start()==0 || (m.start()>0 && template.charAt(m.start()-1)!='\\')) {
					String match = m.group(4);					
					names.add(match);
			}
		}

		return names;
	}
	
	/*
	 * @Param defaultParamValue replacement of missing parameter,
	 * IllegalParameterException will be thrown if defaultStr is null and there
	 * is missing parameter
	 */
	private static String doExtend(String template, ParameterFinder finder,
			String defaultParamValue) 
	{
		//Assert.hasText(template, "'template' must not be null");
		if (TextUtils.isEmpty(template)) {
			return "";
		}

		Matcher m = VAR_PATTERN.matcher(template);
		StringBuilder sb = new StringBuilder();

		int end = 0;
		while (m.find()) {
			if (m.start()==0 || (m.start()>0 && template.charAt(m.start()-1)!='\\')) {
				sb.append(template.substring(end, m.start(3)));
				String match = m.group(4);
	
				if (finder != null && finder.containsKey(match)) {
					sb.append(finder.find(match));
				} else if (defaultParamValue != null) {
					sb.append(defaultParamValue);
				} else {
					logger.error("couldn't extend string {}", template);
					throw new ParameterNotFoundException(match);
				}
	
				end = m.end(3);
			} else {
				sb.append(template.substring(end, m.end()));
				end = m.end();
			}
		}

		if (end > 0) {
			sb.append(template.substring(end));
		}
		else {
			return template;
		}

		return sb.toString();
	}

	public static boolean isExtendable(String input) {
			Matcher m = VAR_PATTERN.matcher(input);
			return m.find();
	}
			
	public static String extend(String template, ParameterFinder finder,
			String defaultParamValue) {
		return doExtend(template, finder, defaultParamValue);
	}

	public static String extend(String template, ParameterFinder finder) {
		
		String val = doExtend(template, finder, null);		
		return val;
	}

	public static boolean matchesIgnoreCase(String pattern, String input, Map<String, String> values) {

		Matcher m = Pattern.compile(pattern.trim().replaceAll(" +", " ").toLowerCase()).matcher(input.trim().toLowerCase());
		
		if (!m.matches()) {
			return false;
		}
		
		if (values != null) {
			for(int i=1; i<=m.groupCount();i++) {
				logger.debug("start {} end {}", m.start(i), m.end(i));
				values.put(String.valueOf(i), m.group(i));
			}
		}

		return true;
	}

	public static String extend(String template, final Map<String, String> map) {
		return extend(template, new ParameterFinder() {

			@Override
			public String find(String key) {
				return map.get(key);
			}

			@Override
			public boolean containsKey(String key) {
				return map.containsKey(key);
			}
			
		});
	}
}
