package com.quickplay.restbot.utils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.google.common.collect.Lists;

public class ClassLoaderUtils {
	private static final Logger logger = LoggerFactory.getLogger(ClassLoaderUtils.class);

	/*
	 * all jar files and classes folder will be added to system class path
	 */
	public static void addLibDirToClassPath(String libPath) throws IOException {
		File dir = new File(libPath);
		if (!dir.canRead() || !dir.isDirectory()) {
			return;
		}
		for(File f : dir.listFiles()) {
			if (f.isDirectory() && f.getName().equals("classes")) {
				addFileToClassPath(f);
			} else if (f.isFile() && f.getName().endsWith(".jar")) {
				addFileToClassPath(f);
			}
		}
	}
	
	//filePath could be path to a jar file or a directory holding all classes
	public static void addFileToClassPath(String filePath) throws IOException {
		addFileToClassPath(new File(filePath));
	}

	public static void addFileToClassPath(File file) throws IOException {
		addURLToClassPath(new Object[]{file.toURI().toURL()});
	}

	public static void addURLToClassPath(Object[] classPathUrls) throws IOException {
		URLClassLoader systemClassLoader = (URLClassLoader) ClassLoaderUtils.class.getClassLoader();
		addURLToClassPath(systemClassLoader, classPathUrls);
	}
	
	public static void addURLToSystemClassPath(Object classPathUrl) throws IOException {
		addURLToSystemClassPath(new Object[]{classPathUrl});
	}

	public static void addURLToSystemClassPath(Object[] classPathUrls) throws IOException {
		addURLToClassPath((URLClassLoader) ClassLoader.getSystemClassLoader(), classPathUrls);

	}
	
	public static void addURLToClassPath(URLClassLoader classLoader, Object[] classPathUrls) throws IOException {
		try {
			Class<?>[] parameters = new Class[]{URL.class};
			Method method = URLClassLoader.class.getDeclaredMethod("addURL", parameters);
			method.setAccessible(true);
			method.invoke(classLoader, classPathUrls);
		} catch (Throwable t) {
			t.printStackTrace();
			throw new IOException("Error, could not add URL to system classloader");
		}
	}
	public static ResourceLoader newResourceLoader(String libDirPath) throws MalformedURLException {
		File dir = new File(libDirPath);
		if (!dir.canRead() || !dir.isDirectory()) {
			logger.warn("path {} is unreadable, path is ignored", libDirPath);
			return null;
		}
		
		return newResourceLoader(dir);
	}
	
	public static ResourceLoader newResourceLoader(File dir) throws MalformedURLException {
		ArrayList<URL> urlList = Lists.newArrayList();
		for(File f : dir.listFiles()) {
			if (f.isDirectory() && f.getName().equals("classes")) {
				logger.trace("adding classpath {}", f.toURI().toURL());
				urlList.add(f.toURI().toURL());
			} else if (f.isFile() && f.getName().endsWith(".jar")) {
				logger.trace("adding jar {}", f.toURI().toURL());
				urlList.add(f.toURI().toURL());
			}
		}
		
		URL[] urls = new URL[urlList.size()];
		urlList.toArray(urls);
		final URLClassLoader classLoader = URLClassLoader.newInstance(urls, ClassLoaderUtils.class.getClassLoader());
		
		ResourceLoader resourceLoader = new PathMatchingResourcePatternResolver(classLoader);
		
		return resourceLoader;
	}
	
	public static class MyClassLoader extends URLClassLoader {
	
		public MyClassLoader(URLClassLoader classLoader) {
			super(classLoader.getURLs());
		}

		@Override
		public void addURL(URL url) {
			super.addURL(url);
		}

		public void addURL(URL[] urls) {
			for (URL url : urls) {
				addURL(url);
			}
		}
	}
}