package com.quickplay.restbot.utils;

import com.quickplay.restbot.net.HttpClient;


public interface BeforeRequestStartListener {

	void onRequest(HttpClient client);
}
