package com.quickplay.restbot.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class CaseNotFoundException extends RuntimeException{
	final static Logger logger = LoggerFactory.getLogger(CaseNotFoundException.class);
	private String casePath;
	public CaseNotFoundException(String casePath ) {
		super("case " + casePath + " was not found");
		this.casePath = casePath;
		logger.error("{} was not found", casePath);
	}
	
	public String getCasePath() {
		return casePath;
	}
}
