package com.quickplay.restbot.exceptions;

@SuppressWarnings("serial")
public class ExprAssertionException extends RuntimeException {

	public ExprAssertionException(String msg) {
		super(msg);
	}
}
