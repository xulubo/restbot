package com.quickplay.restbot.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.sql.DataSource;

/**
 * Indicate the test will use an embeded data source instead of creating from dbUrl
 * @author robertx
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface EmbeddedDataSource {

	Class<? extends DataSource> dsClass();
}
