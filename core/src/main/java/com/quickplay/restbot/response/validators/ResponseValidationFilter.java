package com.quickplay.restbot.response.validators;

import java.util.Collection;
import java.util.regex.PatternSyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.filter.CaseFilter;
import com.quickplay.restbot.caze.filter.CaseFilterChain;
import com.quickplay.restbot.exceptions.CaseResponseValidationException;
import com.quickplay.restbot.expression.ValidationRule;

public class ResponseValidationFilter implements CaseFilter {
	private static Logger logger = LoggerFactory.getLogger(ResponseValidationFilter.class);

	public ValueValidatorChain newChain() {
		return new DefaultValueValidatorChain();
	}
	
	private static final String NAME = "RESPONSE_VALIDATION";
	
	@Override
	public void filter(Case c, CaseFilterChain chain) {
		logger.trace("Enter filter ..");
		validateResponse(c);

		chain.doFilter(c);
	}

	@Override
	public String name() {
		return NAME;
	}

	//validate data in response body, data field is indicated in JsonPath
	private void validateResponse(Case c) {
		for (ValidationRule r : c.getValidationRules()) {
			validateResponse(c, r.getDataPath(), r.getExpr());
		}
	}
	
	//validate data in response body, data field is indicated in JsonPath
	private void validateResponse(Case c, String jsonPath, String rule) {
			if (StringUtils.isEmpty(rule)) {
				logger.trace("empty value, skip validation, path={}", jsonPath);
				return;
			}

			try {
				Object value = c.readValue(jsonPath);
				
				if (value instanceof Collection) {
					@SuppressWarnings("unchecked")
					Collection<Object> coll = (Collection<Object>) value;
					for(Object o : coll) {
						c.validate(o, rule);
					}
				} else {
					c.validate(value, rule);
					
				}


				// null value means value is absent which is a good situation for some cases
				//if (value != null) {
				//	newChain().validate(value, pattern);
				//}
				//else {
				//	throw new CaseResponseValidationException("Can't read value by " + jsonPath);					
				//}
			} catch(PatternSyntaxException e) {
				logger.error(e.getMessage(), e);
				throw new CaseResponseValidationException("pattern syntax error rule=" + rule, e);
			} catch (Throwable e) {
				throw new CaseResponseValidationException(String.format("path:[%s] rule:[%s], message:[%s]", jsonPath, rule, e.getMessage()), e);				
			}
		}


}
