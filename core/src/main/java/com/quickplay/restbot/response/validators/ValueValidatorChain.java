package com.quickplay.restbot.response.validators;


public interface ValueValidatorChain {

	public void validate(Object value, String validationExpression);
}
