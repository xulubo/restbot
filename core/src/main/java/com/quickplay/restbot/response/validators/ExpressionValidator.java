package com.quickplay.restbot.response.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.exceptions.CaseResponseValidationException;
import com.quickplay.restbot.exceptions.ExprMethodNotFoundException;
import com.quickplay.restbot.exceptions.ExprUnresolvableException;
import com.quickplay.restbot.expr.ExprEvaluator;
import com.quickplay.restbot.expr.MethodExpr;
import com.quickplay.restbot.expr.MethodRefFactory;


public class ExpressionValidator implements ValueValidator {
	private static final Logger logger = LoggerFactory.getLogger(ExpressionValidator.class);
	
	ExprEvaluator exprEvaluator = ExprEvaluator.INSTANCE;
	Case owner;
	
	public ExpressionValidator(Case owner) {
		this.owner = owner;
	}
	
	@Override
	public void validate(Object value, final String expression,
			ValueValidatorChain chain) {
		try {
			validate(value, expression);
		} catch (ExprUnresolvableException | ExprMethodNotFoundException e) {
			logger.debug("{}", e.getMessage(), e);
			if (chain != null) {
				chain.validate(value, expression);
			}
		} catch (Exception e) {
			logger.error("unexpected exception", e);
			if (chain != null) {
				chain.validate(value, expression);
			}
		}
	}
	
	public void validate(Object value, String expr) {
		expr = expr.trim();
		
		String[] OPERATORS = {
				"<=", ">=", "!=", "==", "=~", "<", ">"	//don't mess up the order, double characters need to be compared first 
		};
		
		for(String op : OPERATORS) {
			if (expr.startsWith(op)) {
				assertCorrect(op, value, expr.substring(op.length()));
				return;
			}
		}
		
		char c0 = expr.charAt(0), c1 = expr.charAt(expr.length()-1);
		if (c0 == '/' && c1 == '/') {
			assertMatch(value, expr.substring(1, expr.length()-1));
		} else if (c0 == '"' && c1=='"') {
			assertEquals(value, expr.substring(1, expr.length()-1));
		} 
		//should be a function call
		else {
			exprEvaluator.validate(value, "${" + expr + "}", null);
		}
	}
	
	private void assertEquals(Object received, String exepcted) {
		throw new CaseResponseValidationException(String.format("expect (%s) received (%s)", exepcted, received));		
	}
	
	private void assertCorrect(String op, Object left, String expr) {
		Object right;
		if (owner == null) {
			right = exprEvaluator.evaluate(expr);
		} else {
			right = owner.eval(expr);
		}
		MethodExpr m = new MethodExpr(null, op, new Object[]{left, right});
		Object o = MethodRefFactory.instance().invoke(m);
		
		if (!"true".equals(o.toString())) {
			throw new CaseResponseValidationException(String.format("(%s) %s (%s) is not true", left, op, expr));
		}
	}
	
	private void assertMatch(Object value, String regex) {
		if (!value.toString().matches(regex)) {
			throw new CaseResponseValidationException(String.format("%s doesn't match expression %s", value, regex));
		}
	}
}
