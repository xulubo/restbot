package com.quickplay.restbot.response.validators;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.quickplay.restbot.exceptions.VariableUnresolvableException;

public class DefaultValueValidatorChain implements ValueValidatorChain {

	List<ValueValidator> valueValidators = Arrays.asList(
			new com.quickplay.restbot.response.validators.ExpressionValidator(null),
			new com.quickplay.restbot.response.validators.DefaultValueValidator()
			);

	private Iterator<ValueValidator> validatorIterator;
	
	public DefaultValueValidatorChain() {
		this.validatorIterator = valueValidators.iterator();
	}
	
	public DefaultValueValidatorChain(Iterator<ValueValidator> iter) {
		this.validatorIterator = iter;
	}
	
	@Override
	public void validate(Object value, String validationExpression) {
		try {
			if (validatorIterator.hasNext()) {
				validatorIterator.next().validate(value, validationExpression, this);
			}
		}
		catch(Throwable t) {
			throw new VariableUnresolvableException(t.getMessage(), t);
		}
		
	}
}
