package com.quickplay.restbot.persist.reader;

import java.io.File;

import com.quickplay.restbot.caze.core.Plan;

public interface PlanReader {

	Plan read(File file);
	boolean accept(File file);
}
