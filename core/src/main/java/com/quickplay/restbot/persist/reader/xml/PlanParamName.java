package com.quickplay.restbot.persist.reader.xml;

public enum PlanParamName {
	id,
	name,
	description,
	responseTemplate,
	responseSchema,
	dependencies,
	commonParams,
	resourceFolder,
	method,
	body,
	bodyFile,
	postScript,
	preScript,	
	baseUrl, 
	host, 
	port, 
	servicePath, 
	fixedParams, 
	dbUrl, 
	dbUsername, 
	dbPassword;

}
