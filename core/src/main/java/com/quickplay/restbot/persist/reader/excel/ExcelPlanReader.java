package com.quickplay.restbot.persist.reader.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.persist.reader.PlanReader;

public class ExcelPlanReader implements PlanReader {
	static final Logger logger = LoggerFactory.getLogger(ExcelPlanReader.class);
	
	XSSFWorkbook  workbook;
	
	@Override
	public Plan read(File file) {
		try {
			logger.debug("reading {}", file.getAbsolutePath());
			InputStream is = new FileInputStream(file);
			Plan plan = read(is);
			is.close();
			return plan;
		} catch(IOException e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	@Override
	public boolean accept(File file) {
		return FilenameUtils.isExtension(file.getName(),"xlsx")
				&& !file.getName().startsWith("~");
	}
	
	protected void readInfoSheet(Plan plan, XSSFSheet sheet) {
		Iterator<Row> rowIterator = sheet.iterator();
		while(rowIterator.hasNext()) {
			Row row = rowIterator.next();
			Cell cell = row.getCell(0);
			if (cell == null) {
				continue;
			}
			
			String name = cell.getStringCellValue();
			if (name == null) {
				continue;
			}
			String value = ExcelUtils.getCellValue(row.getCell(1));
			if (value == null) {
				continue;
			}
			
			plan.getProperties().add(name, value);
		}		
	}
	
	public Plan read(InputStream excelInputStream) {
		try {
			Plan plan = new Plan();
			workbook = new XSSFWorkbook(excelInputStream);
			
			/**
			 * read global variables from the first sheet
			 */
			readInfoSheet(plan, workbook.getSheetAt(0));
			
			/**
			 * read test cases from sheet with name starting with test
			 */
			Iterator<XSSFSheet> sheetIterator = workbook.iterator();
			while(sheetIterator.hasNext()) {
				XSSFSheet testSheet = sheetIterator.next();
				if (!testSheet.getSheetName().startsWith("test")) {
					continue;
				}
				Case suite = new ExcelTestSheetReader().readResource(testSheet);
				plan.addChild(suite.getEntity());
			}
			
			excelInputStream.close();

			return plan;
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException("File not found", e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException("IOException", e);
		}		
	}
	
	


}
