package com.quickplay.restbot.persist.reader.xml;

import java.util.ArrayList;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.quickplay.restbot.caze.core.Expression;
import com.quickplay.restbot.caze.core.ExpressionSet;
import com.quickplay.restbot.expression.ExpressionParser;

class ExpressionMapAdapter extends XmlAdapter<MapEntryList, ExpressionSet> {
	boolean lowercaseKey = false;
	
	ExpressionMapAdapter(){}
	ExpressionMapAdapter(boolean lck) {
		lowercaseKey = lck;
	}
	
	@Override
    public MapEntryList marshal(ExpressionSet map) throws Exception {
		MapEntryList mapElements = new MapEntryList();
		mapElements.entry = new ArrayList<>(map.size());

        for (Expression entry : map)
            mapElements.entry.add(new MapElement(entry.getName(), entry.getValue()));

        return mapElements;
    }

    @Override
    public ExpressionSet unmarshal(MapEntryList v) throws Exception {
        ExpressionSet map = new ExpressionSet();
        if (v.entry != null) {
	        for (MapElement e : v.entry)
	            map.add(ExpressionParser.create(this.lowercaseKey ? e.key.toLowerCase() : e.key, e.value));
        }
        return map;
    }
}

