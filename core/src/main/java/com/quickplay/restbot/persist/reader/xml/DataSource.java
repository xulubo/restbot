package com.quickplay.restbot.persist.reader.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class DataSource {

	@XmlElement(name="dataSourceUrl", required=false)
	private String dataSourceUrl;

	@XmlElement(name="dataSourceUserName", required=false)
	private String dataSourceUserName;

	@XmlElement(name="dataSourcePassword", required=false)
	private String dataSourcePassword;
	
	public String getDataSourceUrl() {
		return this.dataSourceUrl;
	}

	public void setDataSourceUrl(String dataSourceUrl) {
		this.dataSourceUrl = dataSourceUrl;
	}

	public String getDataSourceUserName() {
		return this.dataSourceUserName;
	}
	
	public void setDataSourceUserName(String userName) {
		this.dataSourceUserName = userName;
	}

	public String getDataSourcePassword() {
		return this.dataSourcePassword;
	}
	
	public void setDataSourcePassword(String password) {
		this.dataSourcePassword = password;
	}

}
