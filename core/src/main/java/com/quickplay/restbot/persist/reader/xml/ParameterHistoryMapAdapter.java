package com.quickplay.restbot.persist.reader.xml;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

class ParameterHistoryMapAdapter extends XmlAdapter<MyParameterHistoryMapType, ParameterMap<Set<String>>> {
	@Override
    public MyParameterHistoryMapType marshal(ParameterMap<Set<String>> v) throws Exception {
		MyParameterHistoryMapType mapElements = new MyParameterHistoryMapType();
		mapElements.list = new ArrayList<>(v.size());

        for (Map.Entry<String, Set<String>> entry : v.entrySet())
            mapElements.list.add(new ParameterHistory(entry.getKey(), entry.getValue()));

        return mapElements;
    }

    @Override
    public ParameterMap<Set<String>> unmarshal(MyParameterHistoryMapType v) throws Exception {
    	ParameterMap<Set<String>> r = new ParameterMap<Set<String>>();
    	if (v!=null && v.list != null) {
	        for (ParameterHistory mapelement : v.list)
	            r.put(mapelement.key, mapelement.values);
    	}
        return r;
    }
}

class MyParameterHistoryMapType {

	@XmlElement(name="entry")
	List<ParameterHistory> list;
}


