package com.quickplay.restbot.persist.reader.excel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.expression.ValidationRule;
import com.quickplay.restbot.persist.reader.xml.CaseHeads;

/**
 * Excel format
 * 		Column groups are separated by at least one empty column
 * 		Column groups must be in the following order
 * 			1. meta infos
 * 			2. result columns filling by this program
 * 			3. request parameters
 * 			4. test expectation definitions, header is in the format of JsonPath, result is regular expression
 * 
 * @author Robert Xu
 *
 */
public class ExcelTestSheetReader {
	static final Logger logger = LoggerFactory.getLogger(ExcelTestSheetReader.class);
	final static String COL_NAMES[] = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q"
		,"R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
	final static int MAX_COL_GROUP = 3;
	final static int LAST_COL_INDEX_OF_META = 0;
	final static int LAST_COL_INDEX_OF_PARAM = 1;
	final static int LAST_COL_INDEX_OF_EXPECTATIONS = 2;
	
	private final ArrayList<Cell> headCells = new ArrayList<Cell>();
	private final int delimitColIndexes[] = new int[MAX_COL_GROUP];

	private CaseHeads parseHeader(Row row) {
		CaseHeads head = new CaseHeads();
		
		Iterator<Cell> cellIterator = row.cellIterator();
		int curColIndex=0, lastValidColIndex=0;
		int colGroupIndex=0; 

		while (cellIterator.hasNext()) {
			Cell cell = cellIterator.next();
			curColIndex = cell.getColumnIndex();
			
			// skip all empty cells
			if (isCellEmpty(cell)) {
				continue;
			}
			headCells.add(cell);

			if (curColIndex - lastValidColIndex > 1 && delimitColIndexes[colGroupIndex] != lastValidColIndex) {
				delimitColIndexes[colGroupIndex++] = lastValidColIndex;
				if (colGroupIndex >= MAX_COL_GROUP) {
					logger.debug("all delemeters have been found");
					break;
				}
			}
			
			String value = ExcelUtils.getCellValue(cell);
			switch(colGroupIndex) {
			case 0:
				head.getMetas().add(value.trim());
				break;
			case 1:
				if (value.startsWith("/")) {
					head.getPathVariables().add(value);
				} else {
					head.getQueryParameters().add(value); 
				}
				break;
			case 2:
				head.getResponseExpectations().add(value);
				break;
			default:
				break;
			}
			
			lastValidColIndex = curColIndex;
		}
		
		if (colGroupIndex<MAX_COL_GROUP) {
			delimitColIndexes[colGroupIndex] = lastValidColIndex;
		}
		
		for(Integer d : delimitColIndexes) {
			logger.trace("found dlimiter at col index " + d + " col name " + getColName(d));
		}
		
		return head;
	}

	private String getColName(int index) {
		int n = index+1;
		LinkedList<String> lst = new LinkedList<String>();
		while (n > 0) {
			if (n == 26) {
				lst.push(COL_NAMES[25]);				
			}
			else {
				lst.push(COL_NAMES[n%26 - 1]);
			}
			n /= 26;
		}
		
		StringBuilder sb = new StringBuilder();
		for(String a : lst) {
			sb.append(a);
		}
		
		return sb.toString();
	}
	
	private Case readCaseRow(String suiteName, Row values) {
		Case entity = new Case();
		
		for(Cell head : headCells) {
			int i = head.getColumnIndex();
			String name = head.getStringCellValue();

			Cell vcell = values.getCell(i);
			if (vcell == null) {
				vcell = values.createCell(i);
				vcell.setCellValue("");
			}
			String value = ExcelUtils.getCellValue(vcell);
			
			//blank value should be ignored
			if (StringUtils.isBlank(value)) {
				continue;
			}
			
			if (i <= delimitColIndexes[LAST_COL_INDEX_OF_META]) {
				name = name.toLowerCase();
				if ("id".equalsIgnoreCase(name)) {
					entity.setId(value);
				} else {
					entity.getProperties().add(name, value);
				}
			}
			else if (i <= delimitColIndexes[LAST_COL_INDEX_OF_PARAM]) {
				if (!StringUtils.isBlank(value)) {
					addQueryParam(entity, name, value);
				}
			}
			else if (i <= delimitColIndexes[LAST_COL_INDEX_OF_EXPECTATIONS]) {
				entity.getValidationRules().add(new ValidationRule(name, value));
			}
		}

		if (StringUtils.isBlank(entity.getId())) {
			logger.trace("case ignored because ID is absent");
			return null;
		}
		
		return entity;
	}
	
	public Case readResource(XSSFSheet testSheet) {
		Case suite = new Case();
		
		String suiteName = testSheet.getSheetName();
		suite.setId(suiteName);
		
		logger.trace("reading sheet {}", suiteName);
		Iterator<Row> rowIterator = testSheet.iterator();
		if (!rowIterator.hasNext()) {
			logger.warn("empty sheet, skipped");
			return suite;
		}
		
		boolean foundHeadRow = false;
		Row row = null;
		logger.trace("read suite parameters");
		while(rowIterator.hasNext()) {
			row = rowIterator.next();
			String name = ExcelUtils.getCellValue(row.getCell(0));
			if (StringUtils.isBlank(name)) {
				continue;
			}
			if ("ID".equalsIgnoreCase(name)) {
				foundHeadRow = true;
				break;
			}
			String value = ExcelUtils.getCellValue(row.getCell(1));
			suite.getProperties().add(name, value);
		}
		
		if (foundHeadRow) {
			logger.trace("read case headers");
			CaseHeads heads = parseHeader(row);
			
			logger.trace("read test cases ...");
			while (rowIterator.hasNext()) {
				row = rowIterator.next();
				Case c = readCaseRow(suiteName, row);
				if (c != null) {
					suite.addChild(c.getEntity());
				}
			}

			for(String name : heads.getMetas()) {
				if (name.equalsIgnoreCase("ID")) {
					heads.getMetas().remove(name);
					break;
				}
			}
		}
		
		return suite;
	}
	
	private boolean isCellEmpty(Cell cell) {
		if (cell.getStringCellValue().trim().length()==0) {
			return true;
		}
		
		return false;
	}

	private void addQueryParam(Case cc, String paranName, String paramValue) {
		cc.getEntity().getQueryParams().add(paranName, paramValue);
	}	
}
