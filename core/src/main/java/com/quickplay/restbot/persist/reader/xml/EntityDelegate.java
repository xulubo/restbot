package com.quickplay.restbot.persist.reader.xml;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.quickplay.restbot.caze.core.Expression;
import com.quickplay.restbot.caze.core.ExpressionSet;
import com.quickplay.restbot.caze.core.UnmodifiableExpressionSet;
import com.quickplay.restbot.expression.InheritedExpression;
import com.quickplay.restbot.expression.ValidationRule;

/*
 * Add extra convenient methods 
 */
public abstract class EntityDelegate extends EntityWrapper {
	
	public EntityDelegate(BaseEntity entity) {
		super(entity);
	}

	abstract public EntityDelegate getParent();
	
	public boolean isRoot() {
		return getParent() == null;
	}
	
	public boolean isLeaf() {
		return getEntity().getChildren().size() == 0;
	}
	
	public String getPath() {
		if (isRoot()){
			return ":";
		} else if (getParent().isRoot()) {
			return ":" + getId();
		}
		
		return getParent().getPath() + ":" + getId();		
	}
	
	@JsonIgnore
	final public String getName() {
		String name = getParam("name");
		if (StringUtils.isBlank(name)) {
			return getId();
		}
		return name;
	}

	final public void setName(String name) {
		param("name", name);
	}

	@JsonIgnore
	final public String getOrdinal() {
		return getParam("ordinal");
	}

	final public void setOrdinal(String ordinal) {
		param("ordinal", ordinal);
	}

	@JsonIgnore
	final public String getBaseUrl() {
		return getParam("baseUrl");
	}

	final public void setBaseUrl(String baseUrl) {
		param("baseUrl", baseUrl);
	}

	@JsonIgnore
	final public String getRequestMethod() {
		return getParam("requestMethod");
	}
	
	final public void setRequestMethod(String requestMethod) {
		param("requestMethod", requestMethod);
	}
	
	@JsonIgnore
	final public String getServicePath() {
		return getParam("servicePath");
	}
	
	final public void setServicePath(String servicePath) {
		param("servicePath", servicePath);
	}

	@JsonIgnore
	final public String getPreScript() {
		return getParam("preScript");
	}

	final public void setPreScript(String script) {
		param("preScript", script);
	}
	
	@JsonIgnore
	final public String getPostScript() {
		return getParam("postScript");
	}

	final public void setPostScript(String script) {
		param("postScript", script);
	}
	
	@JsonIgnore
	final public String getDescription() {
		return getParam("description");
	}
	
	final public void setDescription(String desc) {
		param("description", desc);
	}
	
	@JsonIgnore
	final public String getRequestBody() {
		return getParam("requestBody");
	}
	
	final public void setRequestBody(String body) {
		param("requestBody", body);
	}
	
	@JsonIgnore
	final public String getCommonParams() {
		return getParam("commonParams");
	}
	
	final public void setCommonParams(String params) {
		param("commonParams", params);
	}

	@JsonIgnore
	final public String getResponseSchema() {
		return getParam("commonParams");
	}
	
	final public void setResponseSchema(String schema) {
		param("responseSchema", schema);
	}
	
	@JsonIgnore
	final public String getResponseTemplate() {
		return getParam("commonParams");
	}
	
	final public void gstResponseTemplate(String t) {
		param("responseTemplate", t);
	}

	@JsonIgnore
	final public String getResourceFolder() {
		return getParam("commonParams");
	}
	
	final public void setResourceFolder(String t) {
		param("responseTemplate", t);
	}

	public String getParam(String name) {
		return getProperty(name);
	}
	
	public final void param(String name, String value) {
		getProperties().add(name, value);
	}
	
	//meta is in fact a parameter
	public String getProperty(String key) {
		Expression p = getProperties().get(key);
		
		if (p != null) {
			return p.getValue();
		}
				
		return (getParent() == null) ? null : getParent().getProperty(key);
	}
	
	public void setProperty(String key, String value) {
		getProperties().add(key, value);
	}
	
	public Expression getFirstQueryParameter(String key) {
		Expression p = this.getQueryParams().get(key);
		
		if (p == null) {
			return getParent() == null ? null : getParent().getFirstQueryParameter(key);			
		}
		
		return p;
	}
	
	public ValidationRule getValidationRule(String key) {
		ValidationRule r = (ValidationRule) this.getValidationRules().get(key);
		
		if (r == null) {
			return getParent() == null ? null : getParent().getValidationRule(key);			
		}
		
		return r;		
	}
	
	@Override
	public ExpressionSet getQueryParams() {
		
		ExpressionSet set = new ExpressionSet();
		set.addAll(super.getQueryParams());
		
		if (!isRoot()) {
			for(Expression e : getParent().getQueryParams()) {
				if (!set.containsKey(e.getName())) {
					if (e instanceof InheritedExpression) {
						set.add(e);
					} else {
						set.add(new InheritedExpression(e));
					}
				}
			}
		}
		
		return new UnmodifiableExpressionSet(set);
	}

	@Override
	public String toString() {
		return String.format("path=%s", getPath());
	}
}
