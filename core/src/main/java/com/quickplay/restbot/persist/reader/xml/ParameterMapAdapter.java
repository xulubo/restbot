package com.quickplay.restbot.persist.reader.xml;

import java.util.ArrayList;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

class ParameterMapAdapter extends XmlAdapter<MyHashMapType, ParameterMap<String>> {

	@Override
    public MyHashMapType marshal(ParameterMap<String> v) throws Exception {
		MyHashMapType mapElements = new MyHashMapType();
		mapElements.entry = new ArrayList<>(v.size());

        for (Map.Entry<String, String> entry : v.entrySet())
            mapElements.entry.add(new MapElement(entry.getKey(), entry.getValue()));

        return mapElements;
    }

    @Override
    public ParameterMap<String> unmarshal(MyHashMapType v) throws Exception {
    	ParameterMap<String> r = new ParameterMap<String>();
    	if (v != null && v.entry!= null) {
	        for (MapElement mapelement : v.entry)
	            r.put(mapelement.key, mapelement.value);
    	}
        return r;
    }
    

}



