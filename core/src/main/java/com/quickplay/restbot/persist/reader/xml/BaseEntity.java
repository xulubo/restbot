package com.quickplay.restbot.persist.reader.xml;

import java.util.Map;

import com.google.common.collect.Maps;
import com.google.gson.annotations.Expose;
import com.quickplay.restbot.caze.core.ExpressionSet;
import com.quickplay.restbot.caze.core.ValidationRuleSet;

public class BaseEntity  {

	@Expose
	private String id;
	
	@Expose
	private ExpressionSet properties = new ExpressionSet();
	
	@Expose
	private ExpressionSet queryParams = new ExpressionSet();
	
	@Expose
	private ValidationRuleSet validationRules = new ValidationRuleSet();
	
	@Expose
	private ExpressionSet httpHeaders = new ExpressionSet();

	@Expose
	private Map<String, BaseEntity> children = Maps.newHashMap();

	//@Override
	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	//@Override
	public ExpressionSet getProperties() {
		return this.properties;
	}

	//@Override
	public ExpressionSet getQueryParams() {
		return this.queryParams;
	}

	//@Override
	public ValidationRuleSet getValidationRules() {
		return this.validationRules;
	}

	//@Override
	public Map<String, BaseEntity> getChildren() {
		return this.children;
	}

	//@Override
	public ExpressionSet getHttpHeaders() {
		return this.httpHeaders;
	}

	//@Override
	public void setProperties(ExpressionSet p) {
		this.properties = p;
	}

	//@Override
	public void setQueryParams(ExpressionSet p) {
		this.queryParams = p;
	}

	//@Override
	public void setValidationRules(ValidationRuleSet p) {
		this.validationRules = p;
	}

	//@Override
	public void setHttpHeaders(ExpressionSet hh) {
		this.httpHeaders = hh;
	}

	//@Override
	public String toString() {
		return String.format("id: %s", getId());
	}
}
