package com.quickplay.restbot.persist.reader.xml;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.persist.reader.PlanReader;

class XmlPlanReader implements PlanReader {
	private static Logger logger = LoggerFactory.getLogger(XmlPlanReader.class);
	
	public Plan read(File xmlFile) {

		try {			 
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			//factory.setValidating(false); // use schema instead of DTD
			//factory.setNamespaceAware(true);
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			final Document document = builder.parse(xmlFile);
			JAXBContext jaxbContext = JAXBContext.newInstance(Plan.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			
			jaxbUnmarshaller.setEventHandler(new ValidationEventHandler(){

				@Override
				public boolean handleEvent(ValidationEvent event) {
					logger.error("XML unmarshall error {}", event.getMessage());
					return false;
				}
				
			});
			
			Plan plan = (Plan)jaxbUnmarshaller.unmarshal(document);
				
			return plan;
			
		} catch (JAXBException | IOException | ParserConfigurationException | SAXException e) {
			e.printStackTrace();
			return null;
		} 
	}

	
	@Override
	public boolean accept(File file) {
		return FilenameUtils.isExtension(file.getName(), "xml");
	}

}
