package com.quickplay.restbot.persist.reader.xml;

import javax.xml.bind.annotation.XmlAttribute;


class MapElement
{
  @XmlAttribute public String  key;
  @XmlAttribute public String value;

  protected MapElement() {} //Required by JAXB

  public MapElement(String key, String value)
  {
    this.key   = key;
    this.value = value;
  }
}