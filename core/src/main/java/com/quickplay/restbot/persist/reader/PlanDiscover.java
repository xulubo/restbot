package com.quickplay.restbot.persist.reader;

import java.io.File;

import com.quickplay.restbot.caze.core.Plan;

public class PlanDiscover implements PlanReader {
	
	@Override
	public Plan read(File dir) {
		Plan plan = null;
		if (dir.isDirectory()) {
			for(File file : dir.listFiles()) {
				if (PlanReaderFactory.INSTANCE.accept(file)) {
					Plan tmp = PlanReaderFactory.INSTANCE.read(file);
					if (plan == null) {
						plan = tmp;
					} else {
						//don't merge, only one file is supported now
						//plan.merge(tmp);
					}
				}
			}
		}
		
		return plan;
	}

	@Override
	public boolean accept(File dir) {
		return dir.isDirectory();
	}

}
