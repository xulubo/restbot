package com.quickplay.restbot.persist.reader.xml;

import java.util.HashSet;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class ParameterNameSetAdapter extends XmlAdapter<HashSet<String>, ParameterNameSet> {
	@Override
    public HashSet<String> marshal(ParameterNameSet v) throws Exception {
		HashSet<String> set = new HashSet<String>();
		set.addAll(v);
		return set;
    }

    @Override
    public ParameterNameSet unmarshal(HashSet<String> set) throws Exception {
    	ParameterNameSet r = new ParameterNameSet();
        
        if (set != null) {
	        for (String v : set)
	            r.add(v);
        }
        return r;
    }
    
}
