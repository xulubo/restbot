package com.quickplay.restbot.persist.reader.xml;

import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class ParameterHistory {

	  @XmlAttribute 
	  public String  key;
	  
	  @XmlElement(name="value") 
	  public Set<String> values;

	  protected ParameterHistory() {} //Required by JAXB

	  public ParameterHistory(String key, Set<String> values)
	  {
	    this.key   = key;
	    this.values = values;
	  }
	  
}
