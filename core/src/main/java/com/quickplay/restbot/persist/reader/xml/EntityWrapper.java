package com.quickplay.restbot.persist.reader.xml;

import java.util.Map;

import org.apache.http.util.Asserts;

import com.quickplay.restbot.caze.core.ExpressionSet;
import com.quickplay.restbot.caze.core.ValidationRuleSet;

/*
 * A wrapper to delegate all methods in Entity, no extra
 */
public class EntityWrapper {

	private BaseEntity entity;

	public EntityWrapper(BaseEntity entity) {
		Asserts.notNull(entity, "entity");
		this.entity = entity;
	}
	
	public BaseEntity getEntity() {
		return this.entity;
	}
	
	public BaseEntity modifier() {
		return getEntity();
	}
	
	public String getId() {
		return entity.getId();
	}

	public void setId(String id) {
		entity.setId(id);
	}
	
	public ExpressionSet getProperties() {
		return entity.getProperties();
	}

	public ExpressionSet getQueryParams() {
		return entity.getQueryParams();
	}

	public ValidationRuleSet getValidationRules() {
		return entity.getValidationRules();
	}

	public Map<String, BaseEntity> getChildren() {
		return entity.getChildren();
	}

	//@Override
	public ExpressionSet getHttpHeaders() {
		return entity.getHttpHeaders();
	}

	//@Override
	public void setProperties(ExpressionSet p) {
		entity.setProperties(p);
	}

	//@Override
	public void setQueryParams(ExpressionSet p) {
		entity.setQueryParams(p);
	}

	//@Override
	public void setValidationRules(ValidationRuleSet p) {
		entity.setValidationRules(p);
	}

	//@Override
	public void setHttpHeaders(ExpressionSet hh) {
		entity.setHttpHeaders(hh);
	}

	@Override
	public String toString() {
		return this.getId();
	}
}
