package com.quickplay.restbot.persist.reader.xml;

import java.util.Set;

public interface Parametric {

	public ParameterMap<String> getParameters();
	void setParameters(ParameterMap<String> parameters);

	public ParameterMap<Set<String>> getHistoricParameters();
	public void setHistoricParameters(ParameterMap<Set<String>> parameters);

}