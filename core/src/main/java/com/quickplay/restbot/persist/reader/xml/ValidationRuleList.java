package com.quickplay.restbot.persist.reader.xml;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.quickplay.restbot.expression.ValidationRule;


@SuppressWarnings("serial")
public class ValidationRuleList extends LinkedList<ValidationRule> {

	@XmlElement(name="validationRule", required=true)
	public List<ValidationRule> getRules() {
		return this;
	}

	public void setRules(List<ValidationRule> rules) {
		this.addAll(rules);
	}
	
	public ValidationRule get(String dataPath) {
		for(ValidationRule rule : this) {
			if (rule.getDataPath().equals(dataPath)) {
				return rule;
			}
		}
		
		return null;
	}
}
