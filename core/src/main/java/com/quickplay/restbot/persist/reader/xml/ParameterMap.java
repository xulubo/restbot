package com.quickplay.restbot.persist.reader.xml;

import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.Maps;

@SuppressWarnings("serial")
public class ParameterMap<V> extends HashMap<String, V> {

	private static Map<String, PlanParamName> nameMap = Maps.newHashMap();
	
	static {
		for(PlanParamName n : PlanParamName.values()) {
			nameMap.put(n.name().toLowerCase(), n);
		}
	}
	
	@Override
	public V put(String key, V value) {
		return super.put(parse(key), value);
	}
	
	@Override
	public V get(Object key) {
		return super.get(parse(key));
	}
	
	private String parse(Object key) {
		String nameKey = key.toString().toLowerCase();
		PlanParamName n = nameMap.get(nameKey);
		return n == null ? key.toString() : n.name();
	}
}
