package com.quickplay.restbot.persist.reader.xml;

import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.google.common.collect.Sets;

@XmlAccessorType(XmlAccessType.FIELD)
public class CaseHeads {

    @XmlJavaTypeAdapter(ParameterNameSetAdapter.class)
    @XmlElement(name="metas")
	ParameterNameSet metas = new ParameterNameSet();
	
	@XmlElementWrapper(name="pathVariables")
	@XmlElement(name="value")
	Set<String> pathVariables = Sets.newLinkedHashSet();

	@XmlElementWrapper(name="queryParams")
	@XmlElement(name="value")
	Set<String> queryParameters = Sets.newLinkedHashSet();

	@XmlElementWrapper(name="validationRules")
	@XmlElement(name="value")
	Set<String> responseExpectations = Sets.newLinkedHashSet();

	public ParameterNameSet getMetas() {
		return metas;
	}

	public void setMetas(ParameterNameSet metas) {
		this.metas = metas;
	}

	public Set<String> getPathVariables() {
		return pathVariables;
	}

	public void setPathVariables(Set<String> pathVariables) {
		this.pathVariables = pathVariables;
	}

	public Set<String> getQueryParameters() {
		return queryParameters;
	}

	public void setQueryParameters(Set<String> queryParameters) {
		this.queryParameters = queryParameters;
	}

	public Set<String> getResponseExpectations() {
		return responseExpectations;
	}

	public void setResponseExpectations(Set<String> responseExpectations) {
		this.responseExpectations = responseExpectations;
	}
}