package com.quickplay.restbot.persist.reader;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.persist.reader.excel.ExcelPlanReader;
import com.quickplay.restbot.persist.reader.json.JsonPlanReader;

public enum PlanReaderFactory implements PlanReader {
	INSTANCE;

	private List<? extends PlanReader> resourceReaders = Arrays.asList(
			new ExcelPlanReader(),
			new JsonPlanReader());


	@Override
	public Plan read(File file) {
		for(PlanReader r : resourceReaders) {
			if (r.accept(file)) {
				return r.read(file);
			}
		}
		
		return null;
	}
	
	@Override
	public boolean accept(File file) {
		for(PlanReader r : resourceReaders) {
			if (r.accept(file)) {
				return true;
			}
		}
		
		return false;
	}


}
