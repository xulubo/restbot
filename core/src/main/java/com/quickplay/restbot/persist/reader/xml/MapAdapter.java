package com.quickplay.restbot.persist.reader.xml;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.google.common.collect.Maps;

public class MapAdapter extends XmlAdapter<MyHashMapType, Map<String, String>> {
	@Override
    public MyHashMapType marshal(Map<String, String> v) throws Exception {
		MyHashMapType mapElements = new MyHashMapType();
		mapElements.entry = new ArrayList<>(v.size());

        for (Map.Entry<String, String> entry : v.entrySet())
            mapElements.entry.add(new MapElement(entry.getKey(), entry.getValue()));

        return mapElements;
    }

    @Override
    public Map<String, String> unmarshal(MyHashMapType v) throws Exception {
        Map<String, String> r = Maps.newLinkedHashMap();
        for (MapElement mapelement : v.entry)
            r.put(mapelement.key, mapelement.value);
        return r;
    }
}

class MyHashMapType {

	@XmlElement(name="entry")
	List<MapElement> entry;
}