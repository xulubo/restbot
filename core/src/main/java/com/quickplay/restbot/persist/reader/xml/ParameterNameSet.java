package com.quickplay.restbot.persist.reader.xml;

import java.util.HashSet;

@SuppressWarnings("serial")
public class ParameterNameSet extends HashSet<String> {
	
	@Override
	public boolean add(String key) {
		return super.add(computeKey(key));			
	}

	public String computeKey(String key) {
		try {
			return PlanParamName.valueOf(key.toLowerCase()).name();
		} catch(Exception e) {
			return key;
		}
	}
}
