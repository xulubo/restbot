package com.quickplay.restbot.persist.reader.xml;

import java.io.ByteArrayOutputStream;
import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quickplay.restbot.caze.core.Plan;

public class XmlPlanWriter {
	private static Logger logger = LoggerFactory.getLogger(XmlPlanWriter.class);

	public void write(Plan plan, File file) {
		try {

			JAXBContext jaxbContext = JAXBContext.newInstance(Plan.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			if (logger.isDebugEnabled()) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				jaxbMarshaller.marshal(plan.getEntity(), out);
				logger.debug("dumping XML before persistency\n{}", out.toString());
			}
			jaxbMarshaller.marshal(plan.getEntity(), file);

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	public void write(Object plan, File file) {
		try {

			JAXBContext jaxbContext = JAXBContext.newInstance(plan.getClass());
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(plan, file);
			jaxbMarshaller.marshal(plan, System.out);

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}	
}
