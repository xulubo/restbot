package com.quickplay.restbot.persist.reader.json;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.persist.reader.PlanReader;
import com.quickplay.restbot.persist.reader.xml.BaseEntity;

public class JsonPlanReader implements PlanReader{
	private static final Logger logger = LoggerFactory.getLogger(JsonPlanReader.class);
	
	@Override
	public Plan read(File file) {
		try {
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
			BaseEntity entity = gson.fromJson(new String(Files.readAllBytes(file.toPath())), BaseEntity.class);
			return new Plan(entity);
		} catch (JsonSyntaxException | IOException e) {
			logger.debug(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean accept(File file) {
		return file.getName().endsWith(".json");
	}

}
