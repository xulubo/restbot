package com.quickplay.restbot.persist.reader.xml;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

class MapEntryList {
	@XmlElement(name="entry")
	List<MapElement> entry;
}
