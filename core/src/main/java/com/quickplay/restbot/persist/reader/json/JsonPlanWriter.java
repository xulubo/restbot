package com.quickplay.restbot.persist.reader.json;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.persist.reader.xml.BaseEntity;

public class JsonPlanWriter {
	private static final Logger logger = LoggerFactory.getLogger(JsonPlanWriter.class);
	
	public void write(Plan plan, File file) {
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		BaseEntity entity = plan.getEntity();
		String s = gson.toJson(entity);
		
		try {
			Files.write(file.toPath(), s.getBytes());
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(e.getMessage());
		}
	}

}
