package com.quickplay.restbot.test;

import org.springframework.jdbc.datasource.DelegatingDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

public class H2DataSource extends DelegatingDataSource {

	//run mvn test on command line to get file blackouts.sql
	public H2DataSource() {
		super(new EmbeddedDatabaseBuilder()
		.setType(EmbeddedDatabaseType.H2)
		.addScript("file:./src/test/resources/db.sql")
		.build());
	}
	
}