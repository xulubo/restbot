package com.quickplay.restbot.runner;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.runner.Runner;
import org.junit.runners.model.InitializationError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.core.TestContext;

/**
 * A case depends on the others in 3 ways
 * 		1. declare in meta columns of Excel resource file 
 *         with title "dependencies" and value caseId1, caseId2
 *      2. refer to the other cases in parameter list
 *      3. refer to the other cases in expected response value list
 *      
 * Cases should be ordered while they are composed, referred class should appear prior to dependent case
 * This class was used to automatically order disordered cases, but consider that will be it will be hard
 * to maintain that case, so removed the feature.  Disordered case will lead to exceptions while case is running
 * @author robertx
 *
 */
public class OrderedRunnerPreparer {
	private final Logger logger = LoggerFactory.getLogger(OrderedRunnerPreparer.class);

	private LinkedHashMap <String, CaseRunner> runners = new LinkedHashMap <String, CaseRunner>();

	public void prepare(TestContext context, CaseRunnerFactory caseRunnerFactory) throws InitializationError {
		
		//put in list first, for ordering the cases, because different cases can depend on same case, so the list could contain duplicated cases
		//for keeping the same order in map, only add it in its first apperance
		LinkedList<Case> lst = Lists.newLinkedList();
		
		int i=0;
		for(Case suite : context.getPlan().children().values()) {
			for(Case caze : suite.children().values()) {
				Map<String, Case> deps = caze.findDependency();
				for(Case c : deps.values()) {
					//push dependency case up front
					lst.push(c);
				}
				logger.trace("{} adding runner for {}", ++i, caze.getId());
				lst.add(caze);				
			}
		}
		
		for(Case caze : lst) {
			CaseRunner runner = caseRunnerFactory.newCaseRunner(context, caze);
			
			if (!this.runners.containsKey(caze.getPath())) {
				this.runners.put(caze.getPath(), runner);
			}
		}
	}

	public List<Runner> getRunners() {
		return new ArrayList<Runner>(runners.values());
	}

}
