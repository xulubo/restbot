package com.quickplay.restbot.runner;

import static com.quickplay.restbot.caze.core.CaseStatus.error;
import static com.quickplay.restbot.caze.core.CaseStatus.failed;
import static com.quickplay.restbot.caze.core.CaseStatus.passed;
import static com.quickplay.restbot.caze.core.CaseStatus.starting;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.runner.notification.RunNotifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quickplay.restbot.caze.core.BasicCaseAllowFilter;
import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.core.CaseAllowFilter;
import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.caze.core.TestContext;
import com.quickplay.restbot.caze.session.CaseSessionHolder;
import com.quickplay.restbot.expr.MethodRefFactory;

/*
 * Carry out the test plan
 */
public class PlanExecutor {
	private static Logger logger = LoggerFactory.getLogger(PlanExecutor.class);
	
	private final CaseRunnerFactory caseRunnerFactory;
	private final RunNotifier notifier = new RunNotifier();
	private final TestContext context;
	private EventListener eventListener;
	private int caseExecutionCount = 0;
	private int totalCasesInPlan = 0;
	private CaseAllowFilter filter;

	public PlanExecutor(Plan plan) {
		this.context = new TestContext(plan);
		this.caseRunnerFactory = new DefaultCaseRunnerFactory(this.context);
		String libDir = plan.getProperty("libDir");
		String basePkgName = plan.getProperty("baseLibPackageName");
		
		if (StringUtils.isNotBlank(libDir) && StringUtils.isNotBlank(basePkgName)) {
			loadLibInDir(new File(libDir), basePkgName);			
		}
		totalCasesInPlan = count(context.getPlan());
	}

	//start to test all cases (filter applied if defined)
	public void start() {
		try {
			start(this.context.getPlan());
			this.context.getReport().dump();
		} catch(Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void execute(String casePath) {
		CaseAllowFilter oldFilter = filter;
		filter = new BasicCaseAllowFilter(Arrays.asList(casePath));
		Case c = this.getPlan().find(casePath);
		run(c);
		filter = oldFilter;
	}
	
	public void execute(Case c) {
		run(c);
	}
	
	//count total cases to be executed
	private int count(Case c) {
		int total = 0;
		if (c.isLeaf()) {
			total = 1;
		}
		
		for(Case a : c.children().values()) {
			total += count(a);
		}		
		
		return total;
	}
	
	private void start(Case c) {
		if (c.isLeaf()) {
			if (this.filter!=null && this.filter.isCaseAllowed(c.getPath())) {
				run(c);						
			}
			return;
		}
		
		for(Case child : c.children().values()) {
			start(child);
		}		
	}

	private void run(Case caze) {
		
		if (caseExecutionCount >= totalCasesInPlan) {
			throw new RuntimeException(
					String.format("There are total {} cases in plan, but executed {}, potiential algothem error", 
							totalCasesInPlan, caseExecutionCount));
		}
		
		Map<String, Case> deps = caze.findDependency();
		for(Case c : deps.values()) {
			if (!c.result().isExecuted()) {
				run(c);
			}
		}

		caseExecutionCount++;
		try {
			CaseSessionHolder.currentSession().setCase(caze);
			CaseRunner runner = caseRunnerFactory.newCaseRunner(context, caze);
			if (this.eventListener != null) {
				this.eventListener.onEvent(new CaseEvent(caze.getPath(), starting));
			}
			
			runner.run(notifier);
			
			if (this.eventListener != null) {
				this.eventListener.onEvent(new CaseEvent(caze.getPath(), caze.result().isPassed()? passed:error));
			}
		}
		catch(Throwable e) {	//AssertionError extends Error
			logger.error("Unexpected error", e);
			caze.result().addError(e.getMessage(), e);
			this.eventListener.onEvent(new CaseEvent(caze.getPath(), failed));								
		} finally {
			CaseSessionHolder.currentSession().setCase(null);
		}
	}
	
	//search the current and lib directory if it exists
	private void loadLibInDir(File dir, String basePackage) {
		if (basePackage == null) {
			logger.debug("basePackage is not provided, ignore lib discovering");
			return;
		}

		try {
			MethodRefFactory.instance().lookupMethodInPath(dir.getAbsolutePath(), basePackage);
		} catch (Exception e) {
			throw new RuntimeException("failed to load libs with in base package " + basePackage + " " + e.getMessage());
		}

		
		//String basePackage = "com.quickplay.openvideo.license_proxy";
		File lib = new File(dir,"lib");
		if (lib.canRead() && lib.isDirectory()) {
			loadLibInDir(lib, basePackage);
		} else {
			logger.debug("lib doesn't exist in {}", dir.getAbsoluteFile());
		}		
	}
	
	public TestContext getContext() {
		return this.context;
	}
	
	public Plan getPlan() {
		return this.context.getPlan();
	}
	

	public void setEventListener(EventListener l) {
		this.eventListener = l;
	}	
	
	public static interface EventListener {
		void onEvent(CaseEvent e);
	}

	public void setCaseFilter(CaseAllowFilter filter) {
		this.filter = filter;
	}
}
