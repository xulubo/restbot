package com.quickplay.restbot.runner;

import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.core.TestContext;
import com.quickplay.restbot.caze.filter.CaseFilterChain;
import com.quickplay.restbot.caze.filter.DefaultCaseFilterChainFactory;

/**
 * Create CaseRunner according to the type of test case
 * @author robertx
 *
 */
public class DefaultCaseRunnerFactory implements CaseRunnerFactory {

	private DefaultCaseFilterChainFactory filterChainFactory;
	
	public DefaultCaseRunnerFactory(TestContext ctx) {
		filterChainFactory = new DefaultCaseFilterChainFactory(ctx);
	}
	
	@Override
	public CaseRunner newCaseRunner(TestContext context, Case c) {
		CaseFilterChain chain = filterChainFactory.newChain();
		
		CaseRunner r = c.isLeaf() ? 
				new SingleCaseRunner(context, c, chain):
				new CollectionCaseRunner(context, c, chain);
		return r;
	}

	public DefaultCaseFilterChainFactory getFilterChainFactory() {
		return this.filterChainFactory;
	}
}
