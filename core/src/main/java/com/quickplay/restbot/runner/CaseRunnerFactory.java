package com.quickplay.restbot.runner;

import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.core.TestContext;

public interface CaseRunnerFactory {

	public CaseRunner newCaseRunner(TestContext context, Case c);

}
