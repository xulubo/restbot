package com.quickplay.restbot.runner;

import com.quickplay.restbot.caze.core.CaseStatus;

public class CaseEvent {
	private String casePath;
	private CaseStatus status;
	
	public CaseEvent(String casePath, CaseStatus status) {
		this.casePath = casePath;
		this.status = status;
	}

	public String getCasePath() {
		return casePath;
	}

	public CaseStatus getStatus() {
		return status;
	}
}