package com.quickplay.restbot.runner;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import javax.sql.DataSource;

import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.manipulation.Filter;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.ParentRunner;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.RunnerScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.UrlResource;

import com.quickplay.restbot.AppContext;
import com.quickplay.restbot.annotations.Config;
import com.quickplay.restbot.annotations.EmbeddedDataSource;
import com.quickplay.restbot.annotations.Filters;
import com.quickplay.restbot.annotations.Host;
import com.quickplay.restbot.annotations.ResourceFiles;
import com.quickplay.restbot.caze.core.BasicCaseAllowFilter;
import com.quickplay.restbot.caze.core.CaseAllowFilter;
import com.quickplay.restbot.caze.core.Plan;
import com.quickplay.restbot.caze.core.TestContext;
import com.quickplay.restbot.persist.reader.PlanReaderFactory;
import com.quickplay.restbot.persist.reader.xml.PlanParamName;

public abstract class AbstractSuiteRunner extends ParentRunner<Runner> implements RunnerScheduler{
	private static final Logger logger = LoggerFactory.getLogger(AbstractSuiteRunner.class);
	
	private final DefaultCaseRunnerFactory caseRunnerFactory;
	private final OrderedRunnerPreparer preparer;
	private final TestContext context;

	public AbstractSuiteRunner(Class<?> testClass) throws InitializationError, MalformedURLException, IOException, InstantiationException, IllegalAccessException {
		super(testClass);
		preparer = new OrderedRunnerPreparer();
		Config cfg = testClass.getAnnotation(Config.class);
		
		AppContext.addConfig(cfg);
		AppContext.autowireBean(this);
		
		
		ResourceFiles r = testClass.getAnnotation(ResourceFiles.class);
		Plan plan = loadResourceFiles(r);
		CaseAllowFilter filter = loadCaseFilter(r);
		EmbeddedDataSource dsAnnotation = testClass.getAnnotation(EmbeddedDataSource.class);
		DataSource ds = null;
		if (dsAnnotation != null) {
			ds = dsAnnotation.dsClass().newInstance();
		}
		
		context = new TestContext(plan, filter, ds);
		caseRunnerFactory = new DefaultCaseRunnerFactory(context);
		caseRunnerFactory.getFilterChainFactory().inject(testClass.getAnnotation(Filters.class));
		Host h = testClass.getAnnotation(Host.class);
		if (h != null) {
			this.context.setParam(PlanParamName.host.name(), h.host());
			this.context.setParam(PlanParamName.port.name(), String.valueOf(h.port()));
		}
 
		this.setScheduler(this);
		preparer.prepare(context, caseRunnerFactory);
	}

	private Plan loadResourceFiles(ResourceFiles r) throws InitializationError, MalformedURLException, IOException {
		Plan plan = new Plan();
		if (r == null || r.value().length==0) {
			throw new InitializationError("no resource file is defined");
		}
		else {
			for(String f : r.value()) {
				Plan tmp = PlanReaderFactory.INSTANCE.read(new UrlResource(f).getFile());
				if (tmp != null) {
					//plan.merge(tmp);
				}
			}
		}		
		return plan;
	}
	
	private CaseAllowFilter loadCaseFilter(ResourceFiles r) throws InitializationError {
		BasicCaseAllowFilter filter = new BasicCaseAllowFilter();
		if (r == null || r.value().length==0) {
			throw new InitializationError("no resource file is defined");
		}
		else {
			for(String p : r.allows()) {
				filter.addIDPattern(p);
			}		
			return filter;
		}
	}
		
	@Override
    public void filter(Filter filter)  {

    }


	@Override
	protected List<Runner> getChildren() {
		return this.preparer.getRunners();
	}

	@Override
	protected Description describeChild(Runner child) {
		return child.getDescription();
	}

	@Override
	protected void runChild(Runner child, RunNotifier notifier) {
		child.run(notifier);
	}
    
	@Override
    public void schedule(Runnable childStatement) {
        childStatement.run();
    }

	@Override
    public void finished() {
		try {
			this.context.getReport().dump();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
