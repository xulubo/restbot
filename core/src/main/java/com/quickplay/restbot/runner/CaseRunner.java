package com.quickplay.restbot.runner;

import org.junit.runner.Runner;

import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.filter.CaseFilterChain;

public abstract class CaseRunner extends Runner {

	final protected Case mCase;
	private CaseFilterChain filterChain;
	
	public CaseRunner(Case c, CaseFilterChain filterChain) {
		this.mCase = c;
		this.filterChain = filterChain;
	}
	
	public Case getCase() {
		return this.mCase;
	}

	public CaseFilterChain getFilterChain() {
		return filterChain;
	}

}
