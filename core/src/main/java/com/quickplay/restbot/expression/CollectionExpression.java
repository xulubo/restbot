package com.quickplay.restbot.expression;

import java.util.Collection;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.quickplay.restbot.caze.core.Expression;
import com.quickplay.restbot.caze.core.ExpressionEvaluator;

/**

 * @author robertx
 *
 */
public class CollectionExpression extends Expression {

	public final static String PATTERN = "^\\$?\\[([ a-zA-Z0-9\\.,-]+)\\]$";	

	private static Pattern pattern = Pattern.compile(PATTERN);
	
	public CollectionExpression(String name, String value) {
		super(name, value);
	}
	
	public Collection<String> values() {
		Matcher m = pattern.matcher(getValue());
		
		if (!m.matches() || m.groupCount()!=1) {
			return null;
		}
		
		String valList = m.group(1);
		LinkedList<String> vals = new LinkedList<String>();
		if (Pattern.matches("[0-9]+-[0-9]+", valList)) {
			String[] pair = valList.split("-");
			int low = Integer.valueOf(pair[0]);
			int high = Integer.valueOf(pair[1]);
			for(int i=low; i<=high; i++) {
				vals.add(String.valueOf(i));
			}
		}
		else {
			String[] strs = valList.split(",");
			for(String v : strs) {
				vals.add(v.trim());
			}
		}
		
		return vals;
	}
	
	public static boolean matches(String paramValue) {
		return pattern.matcher(paramValue).matches();
	}
	
	/*
	 * CollectionExpression shouldn't be evaluated
	 * @see com.quickplay.restbot.caze.core.Expression#eval(com.quickplay.restbot.caze.core.ExpressionEvaluator)
	 */
	@Override
	public Object eval(ExpressionEvaluator ee) {
		return getValue();
	}
}
