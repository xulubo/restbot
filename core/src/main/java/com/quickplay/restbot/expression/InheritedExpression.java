package com.quickplay.restbot.expression;

import com.quickplay.restbot.caze.core.Expression;
import com.quickplay.restbot.caze.core.ExpressionEvaluator;

public class InheritedExpression extends Expression {

	private Expression expression;

	public InheritedExpression(Expression e) {
		this.expression = e;
	}
	
	public Integer getId() {
		return expression.getId();
	}

	public void setId(Integer id) {
		expression.setId(id);
	}

	public Object getResult() {
		return expression.getResult();
	}

	public void setResult(Object result) {
		expression.setResult(result);
	}

	public String getName() {
		return expression.getName();
	}

	public void setName(String name) {
		expression.setName(name);
	}

	public String getValue() {
		return expression.getValue();
	}

	public void setValue(String value) {
		expression.setValue(value);
	}

	public Object eval(ExpressionEvaluator ee) {
		return expression.eval(ee);
	}

	public String toString() {
		return expression.toString();
	}

	public int hashCode() {
		return expression.hashCode();
	}

	public boolean equals(Object obj) {
		return expression.equals(obj);
	}
	
	public boolean isInherited() {
		return true;
	}
}
