package com.quickplay.restbot.expression.resolver;


public interface ExpressionResolver {

	String resolve(String value, ExpressionResolverChain chain);
}
