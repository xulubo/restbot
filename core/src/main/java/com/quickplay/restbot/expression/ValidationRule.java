package com.quickplay.restbot.expression;

import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import com.google.gson.annotations.Expose;


@XmlAccessorType(XmlAccessType.FIELD)
public class ValidationRule {

	@Expose
	private Integer id;

	@Expose
	private String dataPath;
	
	@Expose
	private String expr;
	
	@XmlTransient
	private String op;

	@XmlTransient
	private String value;	//the right operand
	
	public ValidationRule() {
		
	}
	
	public ValidationRule(String dataPath, String value) {
		this.dataPath = dataPath;
		this.expr = value;
	}
	
	public ValidationRule(String dataPath, String op, String right) {
		this.dataPath = dataPath;
		this.op = op;
		this.value = right;
		composeExpression();
	}
	
	public ValidationRule(Map<String, String> map) {
		this(map.get("dataPath"), map.get("op"), map.get("right"));
	}
	
	//It is used for exposing JSON to client which needs this property
	//This is for Jackson,  Gson uses the above annotation @Expose
	public final String getName() {
		return this.getDataPath();
	}
	
	public final void setName(String name) {
		setDataPath(name);
	}
	
	public String getDataPath() {
		return this.dataPath;
	}

	public void setDataPath(String dataPath) {
		this.dataPath = dataPath;
	}

	public String getOp() {
		if (this.op == null) {
			parseExpression();
		}
		return this.op;
	}
	
	public void setOp(String op) {
		try {
			this.op = Operator.valueOf(op).name();
		} catch(Exception e){
			this.op = op;
		}
	}
	
	public String getValue() {
		if (this.value == null) {
			parseExpression();
		}
		
		return this.value;
	}

	public void setValue(String right) {
		this.value = right;
	}

	public String getExpr() {
		if (this.expr == null) {
			composeExpression();
		}
		
		return this.expr == null ? "" : this.expr;
	}
	
	public void setExpr(String value) {
		this.expr = value;
		parseExpression();
	}

	private void composeExpression() {
		String expression;
		try {
			if (this.op != null) {
				expression = Operator.valueOf(this.op).format(this.value);
			} else {
				expression = this.value;
			}
		} catch(Exception e) {
			if (this.op != null) {
				expression = this.op + this.value;
			} else {
				expression = this.value;
			}
		}	
		
		setExpr(expression);
	}
	
	private void parseExpression() {
		String expr = getExpr();
		if (expr != null) {
			Operator op = Operator.match(expr);
			if (op != null) {
				this.op = op.name();
				this.value = op.extractValue(expr);
				return;
			}

			char c0 = expr.charAt(0), c1 = expr.charAt(expr.length()-1);
			if (c0 == '/' && c1 == '/') {
				this.op = Operator.mt.name();
				this.value = expr.substring(1, expr.length()-1);
			} else if (c0 == '"' && c1=='"') {
				this.op = Operator.eq.name();
				this.value = expr.substring(1, expr.length()-1);
			} else {
				this.op = "?=";
				this.value = expr;
			}
		}
	}

	public Integer getId() {
		return this.id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
}
