package com.quickplay.restbot.expression;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum Operator {

	le("<=", "(.+)", 		"<=%s"), 
	ge(">=", "(.+)", 		">=%s"), 
	ne("!=", "(.+)", 		"!=%s"), 
	eq("==", "(.+)", 		"==%s"), 
	mt(""  , "/(.+)/", 		"/%s/"), 
	lt("<" , "([^=]+.*)", 	"<%s"), 
	gt(">" , "([^=]+.*)", 	">%s"), 
	;
	
	//#=  array number equals
	//#<
	//#>
	//#>=
	//#<=
	final String symbol;
	final Pattern pattern;
	final String format;
	Operator(String symbol, String pattern, String format) {
		this.symbol = symbol;
		this.pattern = Pattern.compile(symbol + pattern);
		this.format = format;
	}
	
	public static Operator match(String expression) {

		for(Operator op : Operator.values()) {
			if (op.pattern.matcher(expression).matches()) {
				return op;
			}
		}
		
		return null;
	}
	
	public String format(String value) {
		return String.format(this.format, value);
	}
	
	public String extractValue(String expression) {
		Matcher m = pattern.matcher(expression);
		if (m.matches()) {
			return m.group(1);
		}
		
		//return whole string if failed to match
		return expression;
	}
}
