package com.quickplay.restbot.expression;

import com.quickplay.restbot.caze.core.Expression;



/**
 * Parameter value is not changeable
 * @author robertx
 *
 */
public class ConstantExpression extends Expression {
	
	public ConstantExpression(String name, String value) {
		super(name, value);
	}
}
