package com.quickplay.restbot.expression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quickplay.restbot.caze.core.Expression;

public class ExpressionParser {
	private static final Logger logger = LoggerFactory.getLogger(ExpressionParser.class);

	public static Expression create(String name, String expr) {		
		logger.trace("parsing expression {}", expr);
		if (expr.length() == 0) {
			return new ConstantExpression(name, expr);
		} else if (CollectionExpression.matches(expr)) {
			return new CollectionExpression(name, expr);	
		} else {
			return new ExtendableString(name, expr);
		}
	}
}
