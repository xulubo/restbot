package com.quickplay.restbot.expression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quickplay.restbot.caze.core.Case;
import com.quickplay.restbot.caze.core.ExpressionEvaluator;
import com.quickplay.restbot.exceptions.CaseNotFoundException;
import com.quickplay.restbot.expr.ExprEvaluator;
import com.quickplay.restbot.expr.ExprParseListener;

public class ExpressionEvaluatorImpl implements ExpressionEvaluator {
	
	private static final Logger logger = LoggerFactory.getLogger(ExpressionEvaluatorImpl.class);
	
	final Case owner;
	
	public ExpressionEvaluatorImpl(Case owner) {
		this.owner = owner;
	}

	public Object eval(String expression) {
		logger.trace("eval {}", expression);
		ExprEvaluator ee = ExprEvaluator.INSTANCE;
		Object val = ee.evaluate(expression, new ExprParseListener(){

			@Override
			public Object getCaseVarVal(String varName) throws CaseNotFoundException {
				return owner.readValue(varName);
			}
			
		});
		
		return val;
	}
		
}
