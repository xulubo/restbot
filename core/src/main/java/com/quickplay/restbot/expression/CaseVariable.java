package com.quickplay.restbot.expression;

import javax.xml.bind.annotation.XmlAttribute;

import org.apache.commons.lang.StringUtils;

public class CaseVariable {

	private String casePath;
	private String valName;
	
	public CaseVariable() {
		
	}

	public CaseVariable(String valName) {
		this(null, valName);
	}

	public CaseVariable(String casePath, String valName) {
		this.valName = valName;
		this.casePath = casePath;
	}

	@XmlAttribute
	public String getCasePath() {
		return casePath;
	}

	public void setCasePath(String casePath) {
		this.casePath = casePath;
	}

	@XmlAttribute
	public String getValName() {
		return valName;
	}

	public void setVarName(String varName) {
		this.valName = varName;
	}
	
	public String getValue() {
		if (StringUtils.isBlank(this.casePath)) {
			return String.format("${%s}", this.valName);			
		}
		return String.format("${%s:%s}", this.casePath, this.valName);
	}

	@Override
	public String toString() {
		return getValue();
	}
	
	public static CaseVariable parse(String expr) {
		int n = expr.lastIndexOf(":");
		if (n == -1) {
			return new CaseVariable(expr);
		}
		
		return new CaseVariable(expr.substring(0,n), expr.substring(n+1));
	}
}
