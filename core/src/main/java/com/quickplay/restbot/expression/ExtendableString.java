package com.quickplay.restbot.expression;

import com.quickplay.restbot.caze.core.Expression;



/**
 * Parameter value is not changeable
 * @author robertx
 *
 */
public class ExtendableString extends Expression {
	public static final String TYPE = "extendable";
	
	public ExtendableString(String name, String expression) {
		super(name, expression);
	}
	
	@Override
	public String toString() {
		return getValue();
	}
}
