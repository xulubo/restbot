package com.quickplay.restbot.expression;

public class InheritedValidationRule extends ValidationRule {

	private ValidationRule validationRule;

	public InheritedValidationRule(ValidationRule r) {
		this.validationRule = r;
	}
	
	public String getDataPath() {
		return validationRule.getDataPath();
	}

	public void setDataPath(String dataPath) {
		validationRule.setDataPath(dataPath);
	}

	public String getOp() {
		return validationRule.getOp();
	}

	public void setOp(String op) {
		validationRule.setOp(op);
	}

	public String getValue() {
		return validationRule.getValue();
	}

	public void setValue(String right) {
		validationRule.setValue(right);
	}

	public int hashCode() {
		return validationRule.hashCode();
	}

	public String getExpr() {
		return validationRule.getExpr();
	}

	public void setExpr(String value) {
		validationRule.setExpr(value);
	}

	public Integer getId() {
		return validationRule.getId();
	}

	public void setId(Integer id) {
		validationRule.setId(id);
	}

	public boolean equals(Object obj) {
		return validationRule.equals(obj);
	}

	public String toString() {
		return validationRule.toString();
	}
	
	public boolean isInherited() {
		return true;
	}
}
