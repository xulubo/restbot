package com.quickplay.restbot.expression.resolver;


public interface ExpressionResolverChain {

	public String resolve(String value);
}
