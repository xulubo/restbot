package com.quickplay.restbot.expression.resolver;

import org.springframework.stereotype.Component;

import com.quickplay.restbot.exceptions.ExprUnresolvableException;
import com.quickplay.restbot.exceptions.VariableUnresolvableException;
import com.quickplay.restbot.expr.ExprEvaluator;

/**
 * Check if the value is an resolvable expression, evaluate it if it does
 * @author robertx
 *
 */
@Component
public class ScriptResolver implements ExpressionResolver {

	ExprEvaluator exprEvaluator = ExprEvaluator.INSTANCE;

	
	@Override
	public String resolve(String value, ExpressionResolverChain chain) {
		if (value.startsWith("=")) {
			String expr = value.substring(1);
			try {
				Object exprValue = exprEvaluator.evaluate(expr);
				if (exprValue == null) {
					throw new VariableUnresolvableException("couldn't resolve expression " + expr + " null was returned");
				}
				expr = exprValue.toString();
				value = expr;
			} catch(ExprUnresolvableException e) {
				throw new VariableUnresolvableException("couldn't resolve expression " + expr, e);
			} catch(Exception e) {
				throw new VariableUnresolvableException("couldn't resolve expression " + expr, e);
			}
		}
		
		return chain.resolve(value);
	}

}
